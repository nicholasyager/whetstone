.PHONY: restart
.PHONY: upgrade_db
.PHONY: start
.PHONY: stop
.PHONY: build

start:
	DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose --env-file docker/.env.local --file docker/docker-compose.local.yml up --build --force-recreate --no-deps

stop:
	docker-compose --env-file docker/.env.local --file docker/docker-compose.local.yml down

build:
	DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose --env-file docker/.env.local --file docker/docker-compose.local.yml build --no-cache $(APP)

restart:
	DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose --env-file docker/.env.local --file docker/docker-compose.local.yml build $(APP)
	docker-compose --env-file docker/.env.local --file docker/docker-compose.local.yml up  --force-recreate --no-deps -d $(APP)

upgrade_db:
	docker exec -it $(APP) echo "export PATH=/opt/venv:$$PATH && cd /app && alembic upgrade head" | /bin/bash
