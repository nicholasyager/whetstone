import json

from whetstone_engine import DbtManifest
from whetstone_engine.models.dbt.manifests import CompiledModelNode, ResourceType


class TestDeserialization:
    def test_standard_manifest(self):
        """Test a standard manifest for deserialization"""

        manifest_dict = json.load(open("tests/manifest.json"))
        manifest = DbtManifest(**manifest_dict)

        assert manifest is not None
        for unique_id, node in manifest.nodes.items():
            if node.resource_type != ResourceType.model:
                continue

            assert isinstance(node, CompiledModelNode)
