from whetstone_api.schemas.result_groups import ResultGroupCreate
from whetstone_api.schemas.results import ResultCreate, ResultSeverity

from whetstone_engine.utilities.result_groups.content_generator import (
    DummyContentGenerator,
)
from whetstone_engine.utilities.result_groups.result_group_aggregator import (
    ResultGroupConstructor,
)


def test_constructor_adds_results():
    results = []
    for model in range(2):
        for test in range(3):
            result = ResultCreate(
                model=f"model.testing.example{model}",
                summary=f"test {test}",
                severity=ResultSeverity.low,
                rule_set="testing rule",
                content={"foo": "bar"},
                nodes={f"model.testing.example{model}"},
            )
            results.append((result, {f"model.testing.example{model}"}, {}))

    result_group_constructor = ResultGroupConstructor(
        content_generator=DummyContentGenerator(), result_group_type=ResultGroupCreate
    )
    result_group_constructor.add_results(results)

    result_groups = result_group_constructor.result_groups()
    assert len(result_groups) == 2
    assert len(result_groups[0].results) == 3
