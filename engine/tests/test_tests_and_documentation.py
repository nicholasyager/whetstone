import datetime

import pytest
from networkx import DiGraph

from whetstone_engine import DbtManifest, GraphRepository, create_graph
from whetstone_engine.models.dbt.manifests import (
    ManifestMetadata,
    CompiledGenericTestNode,
    TestMetadata,
    FileHash,
    CompiledModelNode,
    NodeConfig,
    TestConfig,
)
from whetstone_engine.rulesets.tests_and_documentation import TestAndDocsRuleSet


class TestTestAndDocumentationRuleSet:
    def test_test_mapping(self):
        """Test that a valid manifest results in proper test mappings."""

        tests = ["not_null", "unique"]
        target_model = "model.example.example_model"

        manifest = DbtManifest(
            metadata=ManifestMetadata(),
            selectors={},
            nodes={
                "test.example."
                + test: CompiledGenericTestNode(
                    raw_sql="fake",
                    test_metadata=TestMetadata(name=test),
                    compiled=True,
                    schema="example",
                    fqn=["test", "example", test],
                    unique_id="test.example." + test,
                    package_name="testing",
                    root_path="",
                    path="",
                    original_file_path="",
                    name="example " + test,
                    alias="example " + test,
                    checksum=FileHash(name="example.sql", checksum="foobar"),
                    depends_on={"nodes": [target_model], "macros": []},
                    config=TestConfig(),
                    created_at=datetime.datetime.timestamp(datetime.datetime.utcnow()),
                    resource_type="test",
                )
                for test in tests
            },
            sources={},
            macros={},
            docs={},
            exposures={},
        )

        ruleset = TestAndDocsRuleSet()
        mapping = ruleset.generate_test_mapping(manifest)
        assert target_model in mapping
        assert mapping[target_model] == {"not_null", "unique"}

    @pytest.mark.parametrize(
        "tests,result_count",
        [(["not_null", "unique"], 0), (["unique"], 1), (["not_null"], 1), ([], 1)],
    )
    def test_detect_missing_primary_keys(self, tests, result_count):
        """For a manifest with a given node missing any primary key tests, return a result."""

        target_model = "model.example.example_model"

        test_nodes = {
            "test.example."
            + test: CompiledGenericTestNode(
                raw_sql="fake",
                test_metadata=TestMetadata(name=test),
                compiled=True,
                schema="example",
                fqn=["test", "example", test],
                unique_id="test.example." + test,
                package_name="testing",
                root_path="",
                path="",
                original_file_path="",
                resource_type="test",
                name="example " + test,
                alias="example " + test,
                checksum=FileHash(name="example.sql", checksum="foobar"),
                depends_on={"nodes": [target_model], "macros": []},
                config=TestConfig(),
                created_at=datetime.datetime.timestamp(datetime.datetime.utcnow()),
            )
            for test in tests
        }

        manifest = DbtManifest(
            metadata=ManifestMetadata(),
            selectors={},
            nodes={
                **{
                    target_model: CompiledModelNode(
                        raw_sql="fake",
                        compiled=True,
                        schema="example",
                        description="Totally real description",
                        fqn=target_model.split("."),
                        unique_id=target_model,
                        package_name="testing",
                        root_path="",
                        path="",
                        original_file_path="",
                        name="example",
                        alias="example",
                        checksum=FileHash(name="example.sql", checksum="foobar"),
                        depends_on={"nodes": [], "macros": []},
                        config=NodeConfig(),
                        created_at=datetime.datetime.timestamp(
                            datetime.datetime.utcnow()
                        ),
                        resource_type="model",
                    )
                },
                **test_nodes,
            },
            sources={},
            macros={},
            docs={},
            exposures={},
            parent_map={target_model: []},
            child_map={target_model: []},
        )

        ruleset = TestAndDocsRuleSet()
        graph: DiGraph = create_graph(manifest)
        graph_repository = GraphRepository(graph)
        output = ruleset(
            manifest=manifest, graph_repository=graph_repository, run_results=None
        )
        print(tests)
        print(output)
        assert len(output) == result_count
