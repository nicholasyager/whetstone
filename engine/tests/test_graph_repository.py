import unittest

import networkx

from whetstone_engine import GraphRepository


class TestGraphRepository(unittest.TestCase):
    def test_get_salient_graph_trivial_case(self):
        """Test that get_salient_subgraph returns a basic subgraph when one node is provided."""

        graph = networkx.DiGraph()
        graph.add_edges_from(
            [("0", "1"), ("3", "1"), ("1", "4"), ("4", "5"), ("4", "6"), ("6", "7")]
        )
        graph_repository = GraphRepository(graph)
        salient_graph = graph_repository.get_salient_subgraph({"7"}, set(), depth=3)
        assert sorted(salient_graph.nodes) == ["6", "7"]

    def test_long_paths_preserved(self):
        """Test that get_salient_subgraph returns long paths between two salient nodes."""

        graph = networkx.DiGraph()
        graph.add_edges_from(
            [
                ("0", "1"),
                ("1", "2"),
                ("2", "3"),
                ("3", "8"),
                ("3", "4"),
                ("3", "5"),
                ("5", "6"),
                ("4", "6"),
                ("6", "7"),
            ]
        )
        graph_repository = GraphRepository(graph)
        salient_graph = graph_repository.get_salient_subgraph(
            {"2", "6"}, set(), depth=1
        )
        print(salient_graph)
        assert sorted(salient_graph.nodes) == ["1", "2", "3", "4", "5", "6", "7"]


if __name__ == "__main__":
    unittest.main()
