import os
from dotenv import load_dotenv

load_dotenv()

REDIS_HOST = os.environ.get("REDIS_HOST")
REDIS_PORT = os.environ.get("REDIS_PORT", 6379)

STORAGE_URL = os.environ.get("STORAGE_URL")
STORAGE_ACCESS_KEY = os.environ.get("STORAGE_ACCESS_KEY")
STORAGE_SECRET_KEY = os.environ.get("STORAGE_SECRET_KEY")
STORAGE_BUCKET = os.environ.get("STORAGE_BUCKET")
STORAGE_USE_SSL = os.environ.get("STORAGE_USE_SSL", "false").lower() == "true"

API_URL = os.environ.get("API_URL", "http://127.0.0.1:8000")
