import logging

import networkx as nx
from networkx.readwrite import json_graph

logger = logging.getLogger(__name__)


def graph_to_json_dict(graph, layout=True) -> dict:
    """Create JSON payload of graph data"""
    if layout:
        try:
            positions = nx.nx_agraph.graphviz_layout(
                graph, prog="dot", args='-Grankdir="LR"'
            )
            nx.set_node_attributes(graph, positions, "position")
        except Exception as e:
            logger.error("Failed to generate layout for graph! Falling back")
            logger.error(graph)
            logger.error(e)

    return json_graph.node_link_data(
        graph,
        attrs=dict(
            source="source",
            target="target",
            name="id",
            key="otherKey",
            link="links",
            position="position",
        ),
    )
