import datetime
import logging
import os
from typing import TypeVar, Type, Optional, List, Union

import requests
from pydantic import UUID4, BaseModel
from whetstone_api.schemas.demo.demo_result_groups import (
    DemoResultGroup,
    DemoResultGroupCreate,
    DemoResultGroupUpdate,
)
from whetstone_api.schemas.demo.results import DemoResultCreate
from whetstone_api.schemas.demo.runs import DemoRun
from whetstone_api.schemas.process_request import ProcessRequest
from whetstone_api.schemas.result_groups import (
    ResultGroup,
    ResultGroupCreate,
    ResultGroupUpdate,
)
from whetstone_api.schemas.results import ResultCreate
from whetstone_api.schemas.run_statistics import RunStatisticCreate, RunStatistic
from whetstone_api.schemas.runs import Run, RunUpdate, RunStatus, RunCreate

Resource = TypeVar("Resource", Run, ResultGroup, DemoRun, DemoResultGroup, RunStatistic)
ResultGroupTypes = TypeVar("ResultGroupTypes", ResultGroup, DemoResultGroup)
RunTypes = TypeVar("RunTypes", Run, DemoRun)
CreateResource = TypeVar(
    "CreateResource",
    RunCreate,
    ResultGroupCreate,
    DemoResultGroupCreate,
    RunStatisticCreate,
)
UpdateResource = TypeVar(
    "UpdateResource", RunUpdate, ResultGroupUpdate, DemoResultGroupUpdate
)

logger = logging.getLogger(__name__)


class DemoRunStatistic(RunStatistic):
    """A demo-specific RunStatistic type for managing URLs"""


class DemoResultGroupSet(BaseModel):
    """Model encapsulating a list of DemoResultCreate"""

    __root__: List[DemoResultGroupCreate]


class RunStatisticSet(BaseModel):
    """Model encapsulating a list of RunStatisticCreate"""

    __root__: List[RunStatisticCreate]


class ResultGroupSet(BaseModel):
    """Model encapsulating a list of ResultCreate"""

    __root__: List[ResultGroupCreate]


class WhetstoneAPI:
    """Client class for interfacing with the Whetstone API."""

    routes = {
        Run: "/accounts/{account_id}/runs",
        ResultGroup: "/accounts/{account_id}/runs/{run_id}/results",
        DemoRun: "/demo/runs",
        DemoResultGroup: "/demo/runs/{run_id}/results",
        RunStatistic: "/statistics/runs?account_id={account_id}",
        DemoRunStatistic: "/statistics/demo_runs",
    }

    def __init__(self, base_url: str):
        self.base_url = base_url
        self.headers = {
            "Content-Type": "application/json",
            "X-API-KEY": os.getenv("API_KEY"),
        }

    def get(
        self, resource_type: Type[Resource], resource_id: int | UUID4, **kwargs
    ) -> Optional[Resource]:
        response = requests.get(
            f"{self.base_url}{self.routes[resource_type].format(**kwargs)}/{resource_id}",
            headers=self.headers,
        )

        response_data = response.json()

        if response_data is None and response.status_code == 200:
            logger.error(
                f"Unable to find {resource_type.__name__} "
                f"{resource_id} in with the provided URL. Are you "
                f"sure that this {resource_type.__name__} exists?"
            )
            return None

        return resource_type(**(response_data["data"]))

    def update(
        self,
        resource_type: Type[Resource],
        resource_id: int,
        payload: UpdateResource,
        **kwargs,
    ) -> Optional[Resource]:
        response = requests.put(
            f"{self.base_url}{self.routes[resource_type].format(**kwargs)}/{resource_id}",
            data=payload.json(),
            headers=self.headers,
        )

        response_data = response.json()
        if response_data is None and response.status_code == 200:
            logger.error(
                f"Unable to find {resource_type.__name__} "
                f"{resource_id} in with the provided URL. Are you "
                f"sure that this {resource_type.__name__} exists?"
            )
            return None

        if response.status_code >= 400:
            raise Exception(f"Failed to post Resource! {response.text}")

        return resource_type(**response_data["data"])

    def partial_update(
        self,
        resource_type: Type[Resource],
        resource_id: Union[int, UUID4],
        payload: UpdateResource,
        **kwargs,
    ) -> Optional[Resource]:
        response = requests.patch(
            f"{self.base_url}{self.routes[resource_type].format(**kwargs)}/{resource_id}",
            data=payload.json(exclude_none=True, exclude_unset=True),
            headers=self.headers,
        )

        response_data = response.json()
        if response_data is None and response.status_code == 200:
            logger.error(
                f"Unable to find {resource_type.__name__} "
                f"{resource_id} in with the provided URL. Are you "
                f"sure that this {resource_type.__name__} exists?"
            )
            return None

        if response.status_code >= 400:
            raise Exception(f"Failed to post Resource! {response.text}")

        return resource_type(**response_data["data"])

    def invalidate_results(self, resource_type: Type[ResultGroupTypes], **kwargs):
        """Invalidate a set of results for a given Run."""

        response = requests.post(
            f"{self.base_url}{self.routes[resource_type].format(**kwargs)}/invalidate",
            headers=self.headers,
            data={},
        )
        if response.status_code >= 400:
            raise Exception(f"Failed to invalidate Results! {response.text}")

    def create_many(
        self, resource_type: Type[Resource], payload: List[CreateResource], **kwargs
    ) -> [Resource]:
        """Create resources in the Whetstone server"""

        logger.info(f"Serializing {resource_type.__name__}")
        if resource_type.__name__ == "DemoResultGroup":
            data = DemoResultGroupSet.parse_obj(payload).json()
        elif resource_type.__name__ in ("RunStatistic", "DemoRunStatistic"):
            data = RunStatisticSet.parse_obj(payload).json()
        else:
            data = ResultGroupSet.parse_obj(payload).json()
        logger.info(f"Completed {resource_type.__name__}")

        logger.info(f"Posting {resource_type.__name__}")
        response = requests.post(
            f"{self.base_url}{self.routes[resource_type].format(**kwargs)}",
            data=data,
            headers=self.headers,
        )
        logger.info(f"Completed post of {resource_type.__name__}")

        response_data = response.json()
        if response.status_code >= 400:
            raise Exception(f"Failed to post Resource! {response.text}")

        return [resource_type(**item) for item in response_data["data"]]

    def start_run(
        self,
        resource_type: Type[RunTypes],
        run_id: int,
        started_at: datetime.datetime,
        **kwargs,
    ):
        """Set a run as starting."""
        self.partial_update(
            resource_type=resource_type,
            resource_id=run_id,
            payload=RunUpdate(status=RunStatus.running, started_at=started_at),
            **kwargs,
        )


class RequestParameters(BaseModel):
    group_type: Type[DemoResultGroup | ResultGroup]
    run_type: Type[DemoRun | Run]
    run_update_type: Type[RunUpdate]
    result_group_type: Type[DemoResultGroup | ResultGroup]
    result_create_type: Type[ResultCreate | DemoResultCreate]
    result_group_create_type: Type[ResultGroupCreate | DemoResultGroupCreate]
    run_statistic_type: Type[RunStatistic | DemoRunStatistic]
    id_parameters: dict
    correlation_parameters: dict
    run_id: str
    result_args: dict


def generate_request_parameters(process_request: ProcessRequest) -> RequestParameters:
    """Generate parameters for a run_request using a ProcessRequest."""

    if process_request.correlation_id is not None:
        return RequestParameters(
            group_type=DemoResultGroup,
            run_type=DemoRun,
            result_group_type=DemoResultGroup,
            run_update_type=RunUpdate,
            result_create_type=DemoResultCreate,
            result_group_create_type=DemoResultGroupCreate,
            run_statistic_type=DemoRunStatistic,
            id_parameters={"resource_id": UUID4(process_request.run_id)},
            correlation_parameters={},
            run_id=process_request.run_id,
            result_args={"correlation_id": process_request.correlation_id},
        )

    else:
        return RequestParameters(
            group_type=ResultGroup,
            run_type=Run,
            result_group_type=ResultGroup,
            run_update_type=RunUpdate,
            result_create_type=ResultCreate,
            result_group_create_type=ResultGroupCreate,
            run_statistic_type=RunStatistic,
            id_parameters={
                "resource_id": int(process_request.run_id),
                "account_id": process_request.account_id,
            },
            correlation_parameters={"account_id": process_request.account_id},
            run_id=process_request.run_id,
            result_args={},
        )
