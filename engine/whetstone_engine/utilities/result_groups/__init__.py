from typing import NewType, Union

from whetstone_api.schemas.demo.demo_result_groups import DemoResultGroupCreate
from whetstone_api.schemas.result_groups import ResultGroupCreate

GenericResultGroupCreate = NewType(
    "GenericResultGroupCreate", Union[ResultGroupCreate, DemoResultGroupCreate]
)
