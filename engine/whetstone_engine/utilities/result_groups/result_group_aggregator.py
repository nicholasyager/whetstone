import logging
from typing import Dict, List, Set, Type, Tuple

from whetstone_api.schemas.demo.results import DemoResultCreate
from whetstone_api.schemas.result_groups import (
    ModelResultGroupContent,
)
from whetstone_api.schemas.results import ResultCreate

from whetstone_engine.utilities.api import CreateResource
from whetstone_engine.utilities.result_groups import GenericResultGroupCreate
from whetstone_engine.utilities.result_groups.content_generator import (
    ContentGenerator,
    DummyContentGenerator,
)

logger = logging.getLogger()


class ResultGroupConstructor:
    """
    Aggregate Results into ResultGroups based on entity. Once identified, each ResultGroup
    will have its content created, including the salient subgraph and query.
    """

    def __init__(
        self,
        result_group_type: Type[CreateResource],
        content_generator: ContentGenerator | DummyContentGenerator,
    ):
        self._result_group_type = result_group_type
        self._result_groups: Dict[str, GenericResultGroupCreate] = {}
        self._content_generator = content_generator
        self._stale_result_groups: Set[str] = set()
        self._salient_nodes: Dict[str, Set[str]] = {}
        self._secondary_nodes: Dict[str, Set[str]] = {}

        self.filter_cache: Dict[str, Set[Tuple[str, str]]] = {}

    def count(self) -> int:
        """Return count of ResultGroups created."""
        return len(self._result_groups)

    def add_results(
        self,
        results: List[Tuple[ResultCreate | DemoResultCreate, Set[str], Set[str]]],
        result_group_args: dict = None,
    ):
        """Add a list or other iterator of Results to the ResultGroupConstructor"""

        for result, salient_nodes, secondary_nodes in results:
            nodes = self._salient_nodes.get(result.model, set())
            self._salient_nodes[result.model] = nodes.union(salient_nodes)

            nodes = self._secondary_nodes.get(result.model, set())
            self._secondary_nodes[result.model] = nodes.union(secondary_nodes)

            if result.model not in self._result_groups:
                new_group = self._result_group_type(
                    entity=result.model,
                    entity_type="model",
                    entity_id=result.model,
                    content=ModelResultGroupContent(query="", subgraph={}),
                    results=[result],
                    **(result_group_args if result_group_args else {}),
                    filters=[{"rule_set": result.rule_set, "severity": result.severity}]
                )

                self.filter_cache[new_group.entity_id] = {
                    (result.rule_set, result.severity)
                }

                self._result_groups[new_group.entity_id] = new_group
                self._stale_result_groups.add(new_group.entity_id)

            else:
                self._result_groups[result.model].results.append(result)

                # Process filters
                new_filter: Set[Tuple[str, str]] = {(result.rule_set, result.severity)}
                if new_filter not in self.filter_cache[result.model]:
                    self._result_groups[result.model].filters.append(
                        {"rule_set": result.rule_set, "severity": result.severity}
                    )
                    self.filter_cache[result.model].update(new_filter)

    def generate_content(self):
        """Generate content for the constructed ResultGroups"""

        for entity_id in [x for x in self._stale_result_groups]:
            self._result_groups[entity_id].content = self._content_generator.generate(
                self._result_groups[entity_id],
                self._salient_nodes[entity_id],
                self._secondary_nodes[entity_id],
            )
            self._stale_result_groups.remove(entity_id)

    def result_groups(self) -> List[GenericResultGroupCreate]:
        """Return a list of GenericResultGroups generated based on the provided results."""

        if len(self._stale_result_groups) > 0:
            self.generate_content()

        return list(self._result_groups.values())
