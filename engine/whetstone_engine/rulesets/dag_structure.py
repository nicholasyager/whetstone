import logging
from typing import Optional, Any, Set, List

import networkx
from networkx import DiGraph
from whetstone_api.schemas.results import ResultSeverity

from whetstone_engine import RuleSet, DbtManifest
from whetstone_engine.models.dbt.manifests import (
    Node,
    ParsedSourceDefinition,
)
from whetstone_engine.models.dbt.run_results import DbtRunResults
from whetstone_engine.rulesets import (
    EngineResult,
    metadata,
    GraphRepository,
    EngineStatistic,
)

import sqlglot
from sqlglot.optimizer.scope import build_scope

logger = logging.getLogger(__name__)


def find_all_tables(query: str) -> List[str]:
    """Find all tables referenced in a query."""

    ast = sqlglot.parse_one(query, error_level=sqlglot.ErrorLevel.WARN)

    root = build_scope(ast)
    return [
        str(source)
        # Traverse the Scope tree, not the AST
        for scope in root.traverse()
        # `selected_sources` contains sources that have been selected in this scope, e.g. in a FROM or JOIN clause.
        # `alias` is the name of this source in this particular scope.
        # `node` is the AST node instance
        # if the selected source is a subquery (including common table expressions),
        #     then `source` will be the Scope instance for that subquery.
        # if the selected source is a table,
        #     then `source` will be a Table instance.
        for alias, (node, source) in scope.selected_sources.items()
        if isinstance(source, sqlglot.exp.Table)
    ]


class DAGStructureRuleSet(RuleSet):
    """
    The DAGStructureRuleSet raises rule violations based on the structure of
    the project's DAG.
    """

    name: str = "Structure"
    description: str = "Rules relating to the structure of a project's DAG."

    def __init__(self, **data: Any):
        super().__init__(**data)
        self.graph: Optional[DiGraph] = None

        # RULES!
        self.model_rules = [
            self.root_model_rule,
            self.multiple_joined_sources_rule,
            # 2024-03-26: This seems to add a lot of noise. Disabling for now/
            # self.model_fanout_rule,
            self.rejoining_upstream_concepts_rule,
            self.joining_sources_and_refs_rule,
        ]
        self.source_rules = [self.source_fanout_rule]

        # Statistics
        self._statistics = [self.meshedness_coefficient]

    def meshedness_coefficient(self) -> Optional[EngineStatistic]:
        """
        For graph complexity, we're going to use the Meshedness Coefficient, specifically
        using the Circuit Rank in the numerator to account for different numbers of connected
        components. In this measure, a 0 describes a perfect tree, whereas a 1 is a maximally
        connected graph. We're shooting for a low value!
        """
        if not self.graph:
            return None

        components = networkx.number_connected_components(self.graph.to_undirected())
        nodes = self.graph.number_of_nodes()
        edges = self.graph.number_of_edges()

        return EngineStatistic(
            statistic="meshedness_coefficient",
            label="Complexity",
            value=(edges - nodes + components) / ((2 * nodes) - 5),
            type="percentage",
        )

    def root_model_rule(
        self, model: Node, graph_repository: GraphRepository, **kwargs
    ) -> Optional[EngineResult]:
        """
        Check if a model is a root model, and return a result if it is. A root
        model is a model that does not have any node dependencies.
        """
        if (
            model.depends_on is not None
            and model.depends_on.nodes is not None
            and len(model.depends_on.nodes) > 0
        ):
            return None

        # Check if the FROM is values definition or a relation.
        try:
            if model.compiled_code:
                referenced_tables = find_all_tables(model.compiled_code)
                if len(referenced_tables) == 0:
                    return None
        except Exception:
            logger.warning("Unable to parse this query! Returning None for now.")
            return None

        return EngineResult(
            severity=ResultSeverity.high,
            model=model.unique_id,
            rule_set=self.name,
            content={"name": "Unexpected root model"},
            summary=(
                f"The {model.unique_id} model does not have any any models or "
                "sources as dependencies. This indicates that it is likely "
                "that this model contains a raw table references. Please "
                "confirm that this model uses `{{ ref() }}` and  "
                "`{{ source() }}` macros where appropriate."
            ),
            nodes={model.unique_id},
        )

    def multiple_joined_sources_rule(
        self, model: Node, graph_repository: GraphRepository, **kwargs
    ) -> Optional[EngineResult]:
        """
        Check if a model is referencing multiple sources, and return a result
        if it is.This is important, since joining multiple sources directly can
        be more brittle than joining staging or base models.
        """

        if (
            len({node for node in model.depends_on.nodes if node.startswith("source.")})
            < 2
        ):
            return None

        return EngineResult(
            severity=ResultSeverity.medium,
            model=model.unique_id,
            rule_set=self.name,
            content={
                "name": "Multiple sources joined directly",
            },
            summary=(
                f"The {model.unique_id} model references and joins two source."
                "models in a single model. While not dire, this pattern is "
                "generally more difficult to maintain long-term than the "
                "joining for multiple base models. Consider adding models as "
                "downstream dependencies of the affected sources, and update "
                "this model to reference the sources."
            ),
            nodes={model.unique_id},
        )

    def joining_sources_and_refs_rule(
        self, model: Node, graph_repository: GraphRepository, **kwargs
    ):
        """
        Check if a model is referencing both sources and refs, and return a result
        if it is. This is important, since joining multiple source and refs indicates
        that a source may not be staged appropriately
        """

        sources = {
            node for node in model.depends_on.nodes if node.startswith("source.")
        }
        models = {node for node in model.depends_on.nodes if node.startswith("model.")}

        if len(sources) == 0 or len(models) == 0:
            return None

        return EngineResult(
            severity=ResultSeverity.medium,
            model=model.unique_id,
            rule_set=self.name,
            content={
                "name": "Models and sources joined directly",
            },
            summary=(
                f"The {model.unique_id} model references both sources and upstream."
                f"models. This indicates that your sources ({', '.join(sources)}) may not be staged, "
                f"or that your staging models are not being referenced consistently."
                "This will be more difficult to maintain long-term than the "
                "appropriately staging your sources. Consider adding staging models "
                "for the references sources, and update "
                "this model to reference the new staging models.."
            ),
            nodes={model.unique_id},
        )

    def model_fanout_rule(
        self, model: Node, graph_repository: GraphRepository, **kwargs
    ) -> Optional[EngineResult]:
        """
        Check if a model has 4+ downstream dependencies. This may indicate that
        the model is fanning out in ways that may prove to be unsustainable.
        Consider changing the grain for the downstream dependencies so that the
        downstream dependencies are consolidated in one model.
        """

        subgraph = graph_repository.get_subgraph(model.unique_id)
        node_degree = subgraph.out_degree(model.unique_id)

        if node_degree < 5:
            return None

        salient_nodes = {model.unique_id}
        # salient_nodes.update(
        #     {child for child in networkx.neighbors(self.graph, model.unique_id)}
        # )

        return EngineResult(
            severity=ResultSeverity.medium,
            model=model.unique_id,
            rule_set=self.name,
            content={
                "name": "Model fanout",
            },
            summary=(
                f"The {model.unique_id} model is used by {node_degree} downstream models. "
                f"This may indicate model fanout, which is usually a dbt DAG "
                f"anti-pattern. Consider finding ways to consolidate the models "
                f"into fewer models."
            ),
            nodes=salient_nodes,
        )

    def source_fanout_rule(
        self,
        source: ParsedSourceDefinition,
        graph_repository: GraphRepository,
        **kwargs,
    ) -> Optional[EngineResult]:
        """
        Check if a source has 2+ downstream dependencies. This indicates that
        the source is fanning out in ways that may prove to be unsustainable.
        Consider adding a single canonical staging model to prevent concept
        drift.
        """

        subgraph = graph_repository.get_subgraph(source.unique_id)
        node_degree = subgraph.out_degree(source.unique_id)

        if node_degree <= 1:
            return None

        # Find out the actual successors. This is slightly more expensive, but
        # is necessary to know if we actually have multiple unique models referencing
        # the source.
        successors = {
            successor
            for successor in subgraph.successors(source.unique_id)
            if successor.startswith("model.")
        }

        if len(successors) <= 1:
            return None

        salient_nodes = {source.unique_id}

        return EngineResult(
            severity=ResultSeverity.medium,
            model=source.unique_id,
            rule_set=self.name,
            content={
                "name": "Source fanout",
            },
            summary=(
                f"The {source.unique_id} source is used by {node_degree} downstream models. "
                f"This indicates that there are multiple models with their own definition "
                f"of how to handle the data in {source.unique_id}, which will introduce "
                f"surface area for the definition of {source.unique_id} to change over time. "
                "It is high recommended that a single staging model be created as the interface "
                f"for {source.unique_id}, and that the downstream models reference this new "
                f"staging model."
            ),
            nodes=salient_nodes,
        )

    @staticmethod
    def detect_rejoining(graph: DiGraph, node: str) -> Optional[List]:
        """Identify the rejoining of nodes"""

        results = []

        predecessors = set(graph.predecessors(node))

        for predecessor in predecessors:
            predecessor_ancestors = set(networkx.ancestors(graph, predecessor))
            common_ancestors = predecessors.intersection(predecessor_ancestors)
            if len(predecessor_ancestors) == 0:
                continue

            results.extend(
                [((predecessor, node), ancestor) for ancestor in common_ancestors]
            )

        # for predecessor in graph.predecessors(node):
        #     predecessor_ancestors = networkx.ancestors(graph, predecessor)
        #     nodes[predecessor] = set(predecessor_ancestors)
        #     nodes[predecessor].add(predecessor)
        #
        # for combination in itertools.combinations(nodes.keys(), 2):
        #     lca = networkx.lowest_common_ancestor(graph, combination[0], combination[1])
        #     if lca is not None and lca not in combination:
        #
        #         # Rejoining detected. Determine depth. We have a hardcoded limit of 3 edges away. This is important for
        #         # preventing distant ancestor raising a rejoin error.
        #         length = networkx.shortest_path_length(graph, source=lca, target=node)
        #         if length >= 3:
        #             logger.debug(
        #                 f"Rejoined concept found between {lca} and {node}, but it is too far away from {node} "
        #                 f"to be reported."
        #             )
        #             continue
        #
        #         results.append((combination, lca))

        return results

    @staticmethod
    def format_items(items) -> List[str]:
        output = []

        for item in items:
            output.append(
                f" - `{'`, `'.join(item[0])}` both reference `{item[1]}`"  # noqa: W604
            )

        return output

    def rejoining_upstream_concepts_rule(
        self, model: Node, graph_repository: GraphRepository, **kwargs
    ) -> Optional[EngineResult]:
        """
        Check if a model has upstream dependencies that are shared across its other
        intermediate dependencies. This may indicate that the intermediate
        dependencies can be represented as a unified concept.
        """

        subgraph = graph_repository.get_subgraph(model.unique_id)
        node_degree = subgraph.in_degree(model.unique_id)

        if node_degree < 2:
            return None

        # Check if the node's ancestors have a shared dependency
        shared_nodes = self.detect_rejoining(subgraph, model.unique_id)

        if len(shared_nodes) == 0:
            return None

        salient_nodes: Set[str] = {model.unique_id}
        secondary_nodes: Set[str] = {lca for _, lca in shared_nodes}

        new_line = "\n"
        return EngineResult(
            severity=ResultSeverity.medium,
            model=model.unique_id,
            rule_set=self.name,
            content={
                "name": "Rejoining of an upstream concept",
            },
            summary=(
                f"The {model.unique_id} model contains multiple dependencies "
                "that each rejoin the same upstream concept. Analysis has identified "
                "the following shared dependencies:\n"
                f"{new_line.join(self.format_items(shared_nodes))}"
                "\n\nConsider adjusting your project so that these shared concepts are "
                "encompassed by the same model."
            ),
            nodes=salient_nodes,
            secondary_nodes=secondary_nodes,
        )

    @metadata()
    def __call__(
        self,
        *,
        manifest: DbtManifest,
        graph_repository: GraphRepository,
        run_results: Optional[DbtRunResults] = None,
    ) -> List[EngineResult]:
        logger.info(f"Starting rule set: {self.__class__.__name__}.")
        self.graph = graph_repository.graph

        results = []

        # Check for Root models
        for node_id, node in manifest.project_nodes.items():
            if node.resource_type != "model":
                continue

            for rule in self.model_rules:
                result = rule(
                    model=node, graph_repository=graph_repository, manifest=manifest
                )

                if result is None:
                    continue

                results.append(result)

        for source_id, source in manifest.project_sources.items():
            for rule in self.source_rules:
                result = rule(
                    source=source, graph_repository=graph_repository, manifest=manifest
                )

                if result is None:
                    continue

                results.append(result)

        logger.info(f"Completed rule set: {self.__class__.__name__}.")

        return results
