import logging
from typing import Optional, Any, Dict, Set, List

from whetstone_api.schemas.results import ResultSeverity

from whetstone_engine import RuleSet, DbtManifest
from whetstone_engine.models.dbt.manifests import CompiledModelNode
from whetstone_engine.models.dbt.run_results import DbtRunResults
from whetstone_engine.rulesets import (
    EngineResult,
    metadata,
    GraphRepository,
    EngineStatistic,
)

logger = logging.getLogger(__name__)


class TestAndDocsRuleSet(RuleSet):
    """
    The TestAndDocsRuleSet raises rule violations based on the absence of model
    tests and model documentation
    """

    name: str = "Tests and Docs"
    description: str = "Rules relating to a project's tests and documentation."

    def __init__(self, **data: Any):

        super().__init__(**data)
        self.rules = [self.missing_primary_key_tests, self.missing_model_description]

        self._statistics = [self.statistic_test_coverage]

        self.test_to_model_mapping = {}
        self.models_with_tests = 0
        self.models = 0

    def statistic_test_coverage(self) -> Optional[EngineStatistic]:
        if self.models == 0:
            return None

        return EngineStatistic(
            statistic="test_coverage",
            label="Test Coverage",
            type="percentage",
            value=self.models_with_tests / self.models,
        )

    def missing_primary_key_tests(
        self, model: CompiledModelNode, **kwargs
    ) -> Optional[EngineResult]:
        """Check if a model is missing primary key tests."""

        if "test_mapping" not in kwargs:
            raise Exception(
                "A test mapping is required to run the missing_primary_key_tests Rule"
            )

        self.models += 1

        test_mapping: Dict[str, Set[str]]
        test_mapping = kwargs["test_mapping"]
        tests = test_mapping.get(model.unique_id, {})

        if "unique" in tests and "not_null" in tests:
            self.models_with_tests += 1
            return None

        return EngineResult(
            severity=ResultSeverity.low,
            model=model.unique_id,
            rule_set=self.name,
            content={"name": "Missing Primary Key Tests"},
            summary=(
                f"The {model.unique_id} model is currently missing both a "
                "`unique` and a `not_null` tests. These tests are valuable for "
                "verifying standard primary key constraints. If your model does"
                " not have a [primary key](https://docs.getdbt.com/terms/primary-key), "
                "then please consider creating a "
                "surrogate key that can be verified using the `surrogate_key` "
                "macro from the `dbt_utils` package."
            ),
            nodes={model.unique_id},
        )

    def missing_model_description(self, model: CompiledModelNode, **kwargs):
        """Check if a model has been provided with a description. If not, return a Result."""

        if model.description is not None and model.description != "":
            return None

        return EngineResult(
            severity=ResultSeverity.low,
            model=model.unique_id,
            rule_set=self.name,
            content={
                "name": "Missing Model Description",
            },
            summary=(
                f"The {model.unique_id} model is currently missing a valid "
                f"description. Model descriptions are vital for long-term "
                f"maintainability and developer ergonomics. Please consider "
                f"adding model documentation if possible."
            ),
            nodes={model.unique_id},
        )

    @staticmethod
    def format_items(items) -> List[str]:
        output = []

        for item in items:
            output.append(f" - `{'`, `'.join(item[0])}` both reference `{item[1]}`")

        return output

    @staticmethod
    def generate_test_mapping(manifest) -> Dict[str, Set[str]]:
        # Identify all tests assigned to each model

        test_to_model_mapping = {}

        for node in manifest.nodes.values():

            if node.resource_type != "test":
                continue

            if node.test_metadata is None:
                logger.warning(f"The test {node.unique_id} is missing test metadata!")
                continue

            dependency: str
            for dependency in node.depends_on.nodes:
                if dependency not in test_to_model_mapping:
                    test_to_model_mapping[dependency] = set()
                test_to_model_mapping[dependency].add(node.test_metadata.name)

        return test_to_model_mapping

    @metadata()
    def __call__(
        self,
        *,
        manifest: DbtManifest,
        graph_repository: GraphRepository,
        run_results: Optional[DbtRunResults] = None,
    ) -> [EngineResult]:

        results = []
        self.test_to_model_mapping = self.generate_test_mapping(manifest)

        # Iterate through each model, and run the RuleSet
        for node_id, node in manifest.project_nodes.items():
            if not isinstance(node, CompiledModelNode):
                continue

            for rule in self.rules:
                logger.debug("Starting rule {{rule}}.")
                result = rule(
                    model=node,
                    graph_repository=graph_repository,
                    test_mapping=self.test_to_model_mapping,
                )

                if result is None:
                    continue

                results.append(result)

        return results
