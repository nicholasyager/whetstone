import logging
from typing import Optional, Set, Any

import networkx

from whetstone_api.schemas.results import ResultSeverity

from whetstone_engine import RuleSet, DbtManifest
from whetstone_engine.models.dbt.manifests import Node, ParsedExposure
from whetstone_engine.models.dbt.run_results import DbtRunResults
from whetstone_engine.rulesets import EngineResult, metadata, GraphRepository

logger = logging.getLogger(__name__)


class ExecutionRuleSet(RuleSet):
    """
    The ExecutionRuleSet raises rule violations if models have negative or
    lackluster execution characteristics. This includes execution time issues
    or other oddities that may need to be addressed.
    """

    name: str = "Execution"
    description: str = "Rules relating to the execution and performance of models."

    def __init__(self, **data: Any):

        super().__init__(**data)

        self.result_rules = [self.rule_execution_time_for_tables]

        self.model_rules = [
            self.chained_view_rule,
        ]

        self.exposure_rules = [self.rule_exposure_parent_materialization]

    def chained_view_rule(
        self,
        model: Node,
        graph_repository: GraphRepository,
        manifest: DbtManifest,
        **kwargs,
    ) -> Optional[EngineResult]:
        """Determine if a given model is part of a chain of view-materialized models."""

        subgraph = graph_repository.get_subgraph(model.unique_id)

        # Filter subgraph to only include view-materialized models.
        view_node_ids = [
            node_id
            for node_id in subgraph.nodes
            if node_id in manifest.nodes
            and manifest.nodes[node_id].resource_type == "model"
            and manifest.nodes[node_id].config.materialized == "view"
        ]

        view_subgraph = subgraph.subgraph(view_node_ids)
        generations = list(networkx.topological_generations(view_subgraph))

        for generation, generation_list in enumerate(generations):
            if model.unique_id in generation_list and generation >= 3:
                return EngineResult(
                    severity=ResultSeverity.medium,
                    model=model.unique_id,
                    rule_set=self.name,
                    content={
                        "name": "Chained view references",
                    },
                    summary=(
                        f"The {model.unique_id} model references a chain of {generation} models that "
                        f"are all materialized as `view`s. Some database technologies are "
                        "greedy when query planning nested views, which may translate into "
                        "very slow query compilation times during execution by the database "
                        "engine. Consider materializing this model or its immediate ancestors "
                        "as tables to reduce this query planning/compilation overhead."
                    ),
                    nodes={model.unique_id},
                )

        return None

    def rule_exposure_parent_materialization(
        self,
        exposure: ParsedExposure,
        graph_repository: GraphRepository,
        manifest: DbtManifest,
        **kwargs,
    ) -> Optional[EngineResult]:
        """
        Check the materializations used by the parent nodes of all exposures. We recommend that all
        exposures references models materialized as tables and incremental objects.
        """

        unstable_nodes: Set[str] = set()

        for node_id in exposure.depends_on.nodes:
            if not node_id.startswith("model."):
                unstable_nodes.add(node_id)
                continue

            node: Node = manifest.nodes[node_id]

            if node.config.materialized not in ("table", "incremental"):
                unstable_nodes.add(node.unique_id)

        if len(unstable_nodes) == 0:
            return None

        return EngineResult(
            severity=ResultSeverity.low,
            model=exposure.unique_id,
            rule_set=self.name,
            content={
                "name": "Exposure parent materialization",
            },
            summary=(
                f"The {exposure.unique_id} exposure depends on models that are not materialized into a table. "
                f"This may have negative performance implications, since your exposure may need to re-compute "
                f"its dependent data multiple times. Affected models: {', '.join(unstable_nodes)}."
            ),
            nodes={exposure.unique_id},
        )

    def rule_execution_time_for_tables(
        self,
        model: Node,
        graph_repository: GraphRepository,
        manifest: DbtManifest,
        **kwargs,
    ) -> Optional[EngineResult]:
        """Check if a slow-running materialization is occurring on a model materialized as a table."""

        run_result = kwargs["run_result"]
        threshold = 120

        # Skip over tests. They are valuable even if slow.
        if run_result.unique_id.startswith("test"):
            return None

        if run_result.unique_id not in manifest.project_nodes:
            return None

        if model.config.materialized == "incremental":
            return None

        if run_result.execution_time < threshold:
            return None

        return EngineResult(
            severity=ResultSeverity.low,
            model=run_result.unique_id,
            rule_set=self.name,
            content={
                "name": "Model execution time over threshold",
            },
            summary=(
                f"Executing the {run_result.unique_id} model took "
                f"{round(run_result.execution_time, ndigits=1)} seconds, "
                f"which is greater than the {threshold} second threshold. "
                f"This model is currently materialized as `{model.config.materialized}`, "
                f"and there may be a benefit to materializing this model incrementally instead."
            ),
            nodes={run_result.unique_id},
        )

    @metadata()
    def __call__(
        self,
        *,
        manifest: DbtManifest,
        graph_repository: GraphRepository,
        run_results: Optional[DbtRunResults] = None,
    ) -> [EngineResult]:

        results = []

        if run_results is None:
            logger.warning("A DbtRunResults object was not provided. Returning.")
            return results

        for run_result in run_results.results:

            if run_result.unique_id not in manifest.project_nodes:
                continue

            for rule in self.result_rules:
                result = rule(
                    model=manifest.nodes[run_result.unique_id],
                    graph_repository=graph_repository,
                    manifest=manifest,
                    run_result=run_result,
                )

                if result is None:
                    continue

                results.append(result)

        for node_id, node in manifest.project_nodes.items():

            if node_id.startswith("test."):
                continue

            for rule in self.model_rules:
                result = rule(
                    model=node,
                    graph_repository=graph_repository,
                    manifest=manifest,
                )

                if result is None:
                    continue

                results.append(result)

        for exposure_id, exposure in manifest.project_exposures.items():

            for rule in self.exposure_rules:
                result = rule(
                    exposure=exposure,
                    graph_repository=graph_repository,
                    manifest=manifest,
                )

                if result is None:
                    continue

                results.append(result)

        return results
