from __future__ import annotations

from datetime import datetime
from enum import Enum
from typing import Any, Dict, List, Optional, Union, Annotated, Literal, NewType

from pydantic import BaseModel, Extra, Field, constr


class ParsedRef(BaseModel):
    name: str
    package: Optional[str]
    version: Optional[str]


RefList = NewType("RefList", List[Union[ParsedRef, List[str]]])


class ManifestMetadata(BaseModel):
    class Config:
        extra = Extra.allow

    dbt_schema_version: Optional[
        str
    ] = "https://schemas.getdbt.com/dbt/manifest/v4.json"
    dbt_version: Optional[str] = "1.0.0rc2"
    generated_at: Optional[datetime] = "2021-11-30T01:35:47.307789Z"
    invocation_id: Optional[Optional[str]] = "66dd78f0-c79a-4b06-81b1-99794345df16"
    env: Optional[Dict[str, str]] = {}
    project_id: Optional[Optional[str]] = Field(
        None, description="A unique identifier for the project"
    )
    user_id: Optional[
        Optional[
            constr(
                regex=r"[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"  # noqa: F722
            )
        ]
    ] = Field(None, description="A unique identifier for the user")
    send_anonymous_usage_stats: Optional[Optional[bool]] = Field(
        None,
        description="Whether dbt is configured to send anonymous usage statistics",  # noqa: F722
    )
    adapter_type: Optional[Optional[str]] = Field(
        None, description="The type name of the adapter"
    )


class ResourceType(Enum):
    analysis = "analysis"
    test = "test"
    model = "model"
    operation = "operation"
    rpc = "rpc"
    sql = "sql"
    seed = "seed"
    snapshot = "snapshot"
    source = "source"
    exposure = "exposure"
    macro = "macro"
    doc = "doc"


class FileHash(BaseModel):
    class Config:
        extra = Extra.allow

    name: str
    checksum: str


class Hook(BaseModel):
    class Config:
        extra = Extra.allow

    sql: str
    transaction: Optional[bool] = True
    index: Optional[Optional[int]] = None


class DependsOn(BaseModel):
    class Config:
        extra = Extra.allow

    macros: Optional[List[str]] = []
    nodes: Optional[List[str]] = []


class ColumnInfo(BaseModel):
    class Config:
        extra = Extra.allow

    name: str
    description: Optional[str] = ""
    meta: Optional[Dict[str, Any]] = {}
    data_type: Optional[Optional[str]] = None
    quote: Optional[Optional[bool]] = None
    tags: Optional[List[str]] = []


class Docs(BaseModel):
    class Config:
        extra = Extra.allow

    show: Optional[bool] = True
    node_color: Optional[str] = True


class InjectedCTE(BaseModel):
    class Config:
        extra = Extra.allow

    id: str
    sql: str


class TestConfig(BaseModel):
    class Config:
        extra = Extra.allow

    enabled: Optional[bool] = True
    alias: Optional[Optional[str]] = None
    schema_: Optional[Optional[str]] = Field("dbt_test__audit", alias="schema")
    database: Optional[Optional[str]] = None
    tags: Optional[Union[List[str], str]] = []
    meta: Optional[Dict[str, Any]] = {}
    materialized: Optional[str] = "test"
    severity: Optional[
        constr(regex=r"^([Ww][Aa][Rr][Nn]|[Ee][Rr][Rr][Oo][Rr])$")  # noqa: F722
    ] = "ERROR"
    store_failures: Optional[Optional[bool]] = None
    where: Optional[Optional[str]] = None
    limit: Optional[Optional[int]] = None
    fail_calc: Optional[str] = "count(*)"
    warn_if: Optional[str] = "!= 0"
    error_if: Optional[str] = "!= 0"


class TestMetadata(BaseModel):
    class Config:
        extra = Extra.allow

    name: str
    kwargs: Optional[Dict[str, Any]] = {}
    namespace: Optional[Optional[str]] = None


class SeedConfig(BaseModel):
    class Config:
        extra = Extra.allow

    enabled: Optional[bool] = True
    alias: Optional[Optional[str]] = None
    schema_: Optional[Optional[str]] = Field(None, alias="schema")
    database: Optional[Optional[str]] = None
    tags: Optional[Union[List[str], str]] = []
    meta: Optional[Dict[str, Any]] = {}
    materialized: Optional[str] = "seed"
    persist_docs: Optional[Dict[str, Any]] = {}
    post_hook: Optional[List[Hook]] = Field([], alias="post-hook")
    pre_hook: Optional[List[Hook]] = Field([], alias="pre-hook")
    quoting: Optional[Dict[str, Any]] = {}
    column_types: Optional[Dict[str, Any]] = {}
    full_refresh: Optional[Optional[bool]] = None
    on_schema_change: Optional[Optional[str]] = "ignore"
    quote_columns: Optional[Optional[bool]] = None


class ParsedSingularTestNode(BaseModel):
    class Config:
        extra = Extra.allow

    raw_sql: str
    database: Optional[Optional[str]] = None
    schema_: str = Field(..., alias="schema")
    fqn: List[str]
    unique_id: str
    package_name: str
    root_path: str
    path: str
    original_file_path: str
    name: str
    resource_type: ResourceType = "test"
    alias: str
    checksum: FileHash
    config: Optional[TestConfig] = {
        "enabled": True,
        "alias": None,
        "schema": "dbt_test__audit",
        "database": None,
        "tags": [],
        "meta": {},
        "materialized": "test",
        "severity": "ERROR",
        "store_failures": None,
        "where": None,
        "limit": None,
        "fail_calc": "count(*)",
        "warn_if": "!= 0",
        "error_if": "!= 0",
    }
    tags: Optional[List[str]] = []
    refs: Optional[RefList] = []
    sources: Optional[List[List[str]]] = []
    depends_on: Optional[DependsOn] = {"macros": [], "nodes": []}
    description: Optional[str] = ""
    columns: Optional[Dict[str, ColumnInfo]] = {}
    meta: Optional[Dict[str, Any]] = {}
    docs: Optional[Docs] = {"show": True}
    patch_path: Optional[Optional[str]] = None
    compiled_path: Optional[Optional[str]] = None
    build_path: Optional[Optional[str]] = None
    deferred: Optional[bool] = False
    unrendered_config: Optional[Dict[str, Any]] = {}
    created_at: Optional[float] = 1638236147.3306549
    config_call_dict: Optional[Dict[str, Any]] = {}


class ParsedGenericTestNode(BaseModel):
    class Config:
        extra = Extra.allow

    raw_sql: str
    test_metadata: TestMetadata
    database: Optional[Optional[str]] = None
    schema_: str = Field(..., alias="schema")
    fqn: List[str]
    unique_id: str
    package_name: str
    root_path: str
    path: str
    original_file_path: str
    name: str
    resource_type: ResourceType = "test"
    alias: str
    checksum: FileHash
    config: Optional[TestConfig] = {
        "enabled": True,
        "alias": None,
        "schema": "dbt_test__audit",
        "database": None,
        "tags": [],
        "meta": {},
        "materialized": "test",
        "severity": "ERROR",
        "store_failures": None,
        "where": None,
        "limit": None,
        "fail_calc": "count(*)",
        "warn_if": "!= 0",
        "error_if": "!= 0",
    }
    tags: Optional[List[str]] = []
    refs: Optional[RefList] = []
    sources: Optional[List[List[str]]] = []
    depends_on: Optional[DependsOn] = {"macros": [], "nodes": []}
    description: Optional[str] = ""
    columns: Optional[Dict[str, ColumnInfo]] = {}
    meta: Optional[Dict[str, Any]] = {}
    docs: Optional[Docs] = {"show": True}
    patch_path: Optional[Optional[str]] = None
    compiled_path: Optional[Optional[str]] = None
    build_path: Optional[Optional[str]] = None
    deferred: Optional[bool] = False
    unrendered_config: Optional[Dict[str, Any]] = {}
    created_at: Optional[float] = 1638236147.3383949
    config_call_dict: Optional[Dict[str, Any]] = {}
    column_name: Optional[Optional[str]] = None
    file_key_name: Optional[Optional[str]] = None


class ParsedSeedNode(BaseModel):
    class Config:
        extra = Extra.allow

    raw_sql: str
    database: Optional[Optional[str]] = None
    schema_: str = Field(..., alias="schema")
    fqn: List[str]
    unique_id: str
    package_name: str
    root_path: str
    path: str
    original_file_path: str
    name: str
    resource_type: ResourceType = "seed"
    alias: str
    checksum: FileHash
    config: Optional[SeedConfig] = {
        "enabled": True,
        "alias": None,
        "schema": None,
        "database": None,
        "tags": [],
        "meta": {},
        "materialized": "seed",
        "persist_docs": {},
        "quoting": {},
        "column_types": {},
        "full_refresh": None,
        "on_schema_change": "ignore",
        "quote_columns": None,
        "post-hook": [],
        "pre-hook": [],
    }
    tags: Optional[List[str]] = []
    refs: Optional[RefList] = []
    sources: Optional[List[List[str]]] = []
    depends_on: Optional[DependsOn] = {"macros": [], "nodes": []}
    description: Optional[str] = ""
    columns: Optional[Dict[str, ColumnInfo]] = {}
    meta: Optional[Dict[str, Any]] = {}
    docs: Optional[Docs] = {"show": True}
    patch_path: Optional[Optional[str]] = None
    compiled_path: Optional[Optional[str]] = None
    build_path: Optional[Optional[str]] = None
    deferred: Optional[bool] = False
    unrendered_config: Optional[Dict[str, Any]] = {}
    created_at: Optional[float] = 1638236147.339838
    config_call_dict: Optional[Dict[str, Any]] = {}


class ResourceType17(Enum):
    snapshot = "snapshot"


class SnapshotConfig(BaseModel):
    class Config:
        extra = Extra.allow

    enabled: Optional[bool] = True
    alias: Optional[Optional[str]] = None
    schema_: Optional[Optional[str]] = Field(None, alias="schema")
    database: Optional[Optional[str]] = None
    tags: Optional[Union[List[str], str]] = []
    meta: Optional[Dict[str, Any]] = {}
    materialized: Optional[str] = "snapshot"
    persist_docs: Optional[Dict[str, Any]] = {}
    post_hook: Optional[List[Hook]] = Field([], alias="post-hook")
    pre_hook: Optional[List[Hook]] = Field([], alias="pre-hook")
    quoting: Optional[Dict[str, Any]] = {}
    column_types: Optional[Dict[str, Any]] = {}
    full_refresh: Optional[Optional[bool]] = None
    on_schema_change: Optional[Optional[str]] = "ignore"
    strategy: Optional[Optional[str]] = None
    unique_key: Optional[Optional[str]] = None
    target_schema: Optional[Optional[str]] = None
    target_database: Optional[Optional[str]] = None
    updated_at: Optional[Optional[str]] = None
    check_cols: Optional[Optional[Union[str, List[str]]]] = None


class ResourceType18(Enum):
    source = "source"


class Quoting(BaseModel):
    class Config:
        extra = Extra.allow

    database: Optional[Optional[bool]] = None
    schema_: Optional[Optional[bool]] = Field(None, alias="schema")
    identifier: Optional[Optional[bool]] = None
    column: Optional[Optional[bool]] = None


class FreshnessMetadata(BaseModel):
    class Config:
        extra = Extra.allow

    dbt_schema_version: Optional[str] = "https://schemas.getdbt.com/dbt/sources/v3.json"
    dbt_version: Optional[str] = "1.0.0rc2"
    generated_at: Optional[datetime] = "2021-11-30T01:35:47.301745Z"
    invocation_id: Optional[Optional[str]] = "66dd78f0-c79a-4b06-81b1-99794345df16"
    env: Optional[Dict[str, str]] = {}


class Status(Enum):
    runtime_error = "runtime error"


class SourceFreshnessRuntimeError(BaseModel):
    class Config:
        extra = Extra.allow

    unique_id: str
    error: Optional[Optional[Union[str, int]]] = None
    status: Status


class Status1(Enum):
    pass_ = "pass"
    warn = "warn"
    error = "error"
    runtime_error = "runtime error"


class PeriodEnum(Enum):
    minute = "minute"
    hour = "hour"
    day = "day"


class Time(BaseModel):
    class Config:
        extra = Extra.allow

    count: Optional[Optional[int]] = None
    period: Optional[Optional[PeriodEnum]] = None


class TimingInfo(BaseModel):
    class Config:
        extra = Extra.allow

    name: str
    started_at: Optional[Optional[datetime]] = None
    completed_at: Optional[Optional[datetime]] = None


class ExternalPartition(BaseModel):
    class Config:
        extra = Extra.allow

    name: Optional[str] = ""
    description: Optional[str] = ""
    data_type: Optional[str] = ""
    meta: Optional[Dict[str, Any]] = {}


class SourceConfig(BaseModel):
    class Config:
        extra = Extra.allow

    enabled: Optional[bool] = True


class ResourceType19(Enum):
    macro = "macro"


class MacroDependsOn(BaseModel):
    class Config:
        extra = Extra.allow

    macros: Optional[List[str]] = []


class MacroArgument(BaseModel):
    class Config:
        extra = Extra.allow

    name: str
    type: Optional[Optional[str]] = None
    description: Optional[str] = ""


class ParsedDocumentation(BaseModel):
    class Config:
        extra = Extra.allow

    unique_id: str
    package_name: str
    path: str
    original_file_path: str
    name: str
    block_contents: str
    resource_type: ResourceType = "doc"
    root_path: Optional[str] = None


class Type(Enum):
    dashboard = "dashboard"
    notebook = "notebook"
    analysis = "analysis"
    ml = "ml"
    application = "application"


class ResourceType20(Enum):
    model = "model"
    analysis = "analysis"
    test = "test"
    snapshot = "snapshot"
    operation = "operation"
    seed = "seed"
    rpc = "rpc"
    sql = "sql"
    docs = "docs"
    source = "source"
    macro = "macro"
    exposure = "exposure"
    metric = "metric"


class MaturityEnum(Enum):
    low = "low"
    medium = "medium"
    high = "high"


class ExposureOwner(BaseModel):
    class Config:
        extra = Extra.allow

    email: Optional[str] = None
    name: Optional[Optional[str]] = None


class ResourceType21(Enum):
    model = "model"
    analysis = "analysis"
    test = "test"
    snapshot = "snapshot"
    operation = "operation"
    seed = "seed"
    rpc = "rpc"
    sql = "sql"
    docs = "docs"
    source = "source"
    macro = "macro"
    exposure = "exposure"
    metric = "metric"


class MetricFilter(BaseModel):
    class Config:
        extra = Extra.allow

    field: str
    operator: str
    value: str


class NodeConfig(BaseModel):
    class Config:
        extra = Extra.allow

    enabled: Optional[bool] = True
    alias: Optional[Optional[str]] = None
    schema_: Optional[Optional[str]] = Field(None, alias="schema")
    database: Optional[Optional[str]] = None
    tags: Optional[Union[List[str], str]] = []
    meta: Optional[Dict[str, Any]] = {}
    materialized: Optional[str] = "view"
    persist_docs: Optional[Dict[str, Any]] = {}
    post_hook: Optional[List[Hook]] = Field([], alias="post-hook")
    pre_hook: Optional[List[Hook]] = Field([], alias="pre-hook")
    quoting: Optional[Dict[str, Any]] = {}
    column_types: Optional[Dict[str, Any]] = {}
    full_refresh: Optional[Optional[bool]] = None
    on_schema_change: Optional[Optional[str]] = "ignore"


class HasCode(BaseModel):
    raw_sql: Optional[str] = None
    raw_code: Optional[str] = None
    # language: str

    def __init__(self, **data: Any):
        if "raw_code" in data and data["raw_code"]:
            data["raw_sql"] = data["raw_code"]

        super().__init__(**data)


class HasUniqueId(BaseModel):
    unique_id: str


class HasFqn(BaseModel):
    fqn: List[str]


class HasRelationMetadata(BaseModel):
    database: Optional[str]
    schema_: str = Field(..., alias="schema")


class UnparsedBaseNode(BaseModel):
    package_name: str
    path: str
    original_file_path: str
    root_path: Optional[str] = None


class UnparsedNode(UnparsedBaseNode, HasCode):
    name: str
    resource_type: ResourceType


class ParsedNodeMandatory(UnparsedNode, HasUniqueId, HasFqn, HasRelationMetadata):
    alias: str
    checksum: FileHash
    config: NodeConfig


class ParsedNodeMixins(BaseModel):
    resource_type: ResourceType
    depends_on: DependsOn
    config: NodeConfig


class ParsedNode(ParsedNodeMandatory, ParsedNodeMixins):
    tags: Optional[List[str]] = []
    refs: Optional[RefList] = []
    sources: Optional[List[List[str]]] = []
    metrics: Optional[List[List[str]]] = []
    depends_on: Optional[DependsOn] = {"macros": [], "nodes": []}
    description: Optional[str] = ""
    columns: Optional[Dict[str, ColumnInfo]] = {}
    meta: Optional[Dict[str, Any]] = {}
    docs: Optional[Docs] = {"show": True}
    patch_path: Optional[str] = None
    compiled_path: Optional[str] = None
    build_path: Optional[str] = None
    deferred: bool = False
    unrendered_config: Optional[Dict[str, Any]] = {}
    created_at: Optional[float] = None
    config_call_dict: Optional[Dict[str, Any]] = {}
    language: Optional[str] = None


class CompiledNodeMixin(BaseModel):
    compiled: bool = True


class CompiledNode(ParsedNode, CompiledNodeMixin):
    compiled_sql: Optional[Optional[str]] = None
    compiled_code: Optional[Optional[str]] = None
    extra_ctes_injected: Optional[bool] = False
    extra_ctes: Optional[List[InjectedCTE]] = []
    relation_name: Optional[Optional[str]] = None


class CompiledModelNode(CompiledNode):
    resource_type: Literal["model"]


class CompiledHookNode(CompiledNode):
    class Config:
        extra = Extra.allow

    resource_type: Literal["operation"]
    index: Optional[Optional[int]] = None


class CompiledRPCNode(CompiledNode):
    class Config:
        extra = Extra.allow

    resource_type: Literal["rpc"]


class CompiledSqlNode(CompiledNode):
    class Config:
        extra = Extra.allow

    resource_type: Literal["sql"]


class HasTestMetadata(BaseModel):
    test_metadata: TestMetadata


class CompiledGenericTestNode(CompiledNode, HasTestMetadata):
    class Config:
        extra = Extra.allow

    resource_type: Literal["test"]
    column_name: Optional[str] = None
    file_key_name: Optional[str] = None
    config: TestConfig
    compiled: Optional[bool] = True
    test_metadata: Optional[TestMetadata]
    created_at: Optional[float]


class CompiledSingularTestNode(CompiledNode, HasTestMetadata):
    class Config:
        extra = Extra.allow

    resource_type: Literal["test"]
    compiled: Optional[bool] = True


class CompiledSeedNode(CompiledNode):
    class Config:
        extra = Extra.allow

    resource_type: Literal["seed"]
    config: SeedConfig
    root_path: Optional[str] = None
    compiled: Optional[bool] = True
    created_at: Optional[float] = None


class CompiledSnapshotNode(CompiledNode):
    class Config:
        extra = Extra.allow

    resource_type: Literal["snapshot"]
    compiled: Optional[bool] = True


class ParsedAnalysisNode(ParsedNode):
    resource_type: Literal["analysis"]


class ParsedHookNode(ParsedNode):
    resource_type: Literal["operation"]
    index: Optional[Optional[int]] = None


class ParsedModelNode(ParsedNode):
    class Config:
        extra = Extra.allow

    resource_type: Literal["model"]


class ParsedRPCNode(ParsedNode):
    class Config:
        extra = Extra.allow

    resource_type: Literal["rpc"]


class ParsedSqlNode(ParsedNode):
    class Config:
        extra = Extra.allow

    resource_type: Literal["sql"]


class ParsedSnapshotNode(ParsedNode):
    class Config:
        extra = Extra.allow

    resource_type: Literal["snapshot"]
    config: SnapshotConfig


class FreshnessThreshold(BaseModel):
    class Config:
        extra = Extra.allow

    warn_after: Optional[Optional[Time]] = {"count": None, "period": None}
    error_after: Optional[Optional[Time]] = {"count": None, "period": None}
    filter: Optional[Optional[str]] = None


class SourceFreshnessOutput(BaseModel):
    class Config:
        extra = Extra.allow

    unique_id: str
    max_loaded_at: datetime
    snapshotted_at: datetime
    max_loaded_at_time_ago_in_s: float
    status: Status1
    criteria: FreshnessThreshold
    adapter_response: Dict[str, Any]
    timing: List[TimingInfo]
    thread_id: str
    execution_time: float


class ExternalTable(BaseModel):
    class Config:
        extra = Extra.allow

    location: Optional[Optional[str]] = None
    file_format: Optional[Optional[str]] = None
    row_format: Optional[Optional[str]] = None
    tbl_properties: Optional[Optional[str]] = None
    partitions: Optional[Optional[List[ExternalPartition]]] = None


class ParsedMacro(UnparsedBaseNode, HasUniqueId):
    class Config:
        extra = Extra.allow

    name: str
    macro_sql: str
    resource_type: Literal["macro"]
    tags: Optional[List[str]] = []
    depends_on: Optional[MacroDependsOn] = {"macros": []}
    description: Optional[str] = ""
    meta: Optional[Dict[str, Any]] = {}
    docs: Optional[Docs] = {"show": True}
    patch_path: Optional[Optional[str]] = None
    arguments: Optional[List[MacroArgument]] = []
    created_at: Optional[float] = 1638236147.345993
    supported_languages: Optional[List[str]] = None


class ParsedExposure(UnparsedBaseNode, HasFqn, HasUniqueId):
    class Config:
        extra = Extra.allow

    name: str
    type: Type
    owner: ExposureOwner
    resource_type: Literal["exposure"]
    description: Optional[str] = ""
    maturity: Optional[Optional[MaturityEnum]] = None
    meta: Optional[Dict[str, Any]] = {}
    tags: Optional[List[str]] = []
    url: Optional[Optional[str]] = None
    depends_on: Optional[DependsOn] = {"macros": [], "nodes": []}
    refs: Optional[RefList] = []
    sources: Optional[List[List[str]]] = []
    created_at: Optional[float] = 1638236147.347234
    label: Optional[str] = None
    config: Optional[dict] = None
    unrendered_config: Optional[dict] = None


class Period(Enum):
    day = "day"
    week = "week"
    month = "month"
    year = "year"


class MetricTime(BaseModel):
    count: Optional[int]
    period: Optional[Period]


class ParsedMetric(UnparsedBaseNode, HasFqn, HasUniqueId):
    class Config:
        extra = Extra.allow

    name: str
    description: str
    label: str
    model: Optional[str] = None
    calculation_method: Optional[str] = None
    expression: Optional[str] = None
    metrics: List[List[str]] = []
    model_unique_id: Optional[str] = None
    window: Optional[MetricTime] = None
    resource_type: ResourceType = "metric"
    type: Optional[str] = None
    sql: Optional[Optional[str]] = None
    timestamp: Optional[Optional[str]] = None
    filters: List[MetricFilter] = []
    time_grains: List[str] = []
    dimensions: List[str] = []
    resource_type: Literal["metric"]
    meta: Optional[Dict[str, Any]] = {}
    tags: Optional[List[str]] = []
    sources: Optional[List[List[str]]] = []
    depends_on: Optional[DependsOn] = {"macros": [], "nodes": []}
    refs: Optional[RefList] = []
    created_at: Optional[float] = 1638236147.348404
    config: Optional[dict] = None
    unrendered_config: Optional[dict] = None


class CompiledAnalysisNode(CompiledNode):
    class Config:
        extra = Extra.allow

    resource_type: Literal["analysis"]
    compiled: Optional[bool] = True


class ParsedSourceMandatory(
    UnparsedBaseNode,
    HasUniqueId,
    HasRelationMetadata,
    HasFqn,
):
    name: str
    source_name: str
    source_description: str
    loader: str
    identifier: str
    resource_type: Literal["source"]


class ParsedSourceDefinition(ParsedSourceMandatory):
    class Config:
        extra = Extra.allow

    resource_type: ResourceType = ResourceType.source
    quoting: Optional[Quoting] = {
        "database": None,
        "schema": None,
        "identifier": None,
        "column": None,
    }
    loaded_at_field: Optional[Optional[str]] = None
    freshness: Optional[Optional[FreshnessThreshold]] = None
    external: Optional[Optional[ExternalTable]] = None
    description: Optional[str] = ""
    columns: Optional[Dict[str, ColumnInfo]] = {}
    meta: Optional[Dict[str, Any]] = {}
    source_meta: Optional[Dict[str, Any]] = {}
    tags: Optional[List[str]] = []
    config: Optional[SourceConfig] = {"enabled": True}
    patch_path: Optional[Optional[str]] = None
    unrendered_config: Optional[Dict[str, Any]] = {}
    relation_name: Optional[Optional[str]] = None
    created_at: Optional[float] = 1638236147.345053


Node = Annotated[
    Union[
        CompiledAnalysisNode,
        CompiledSingularTestNode,
        CompiledModelNode,
        CompiledHookNode,
        CompiledRPCNode,
        CompiledSqlNode,
        CompiledGenericTestNode,
        CompiledSeedNode,
        CompiledSnapshotNode,
        # ParsedAnalysisNode,
        # ParsedSingularTestNode,
        # ParsedHookNode,
        # ParsedModelNode,
        # ParsedRPCNode,
        # ParsedSqlNode,
        # ParsedGenericTestNode,
        # ParsedSeedNode,
        # ParsedSnapshotNode,
    ],
    Field(discriminator="resource_type"),
]


class DbtManifest(BaseModel):
    class Config:
        extra = Extra.allow

    metadata: ManifestMetadata = Field(..., description="Metadata about the manifest")
    nodes: Dict[str, Node] = Field(
        ..., description="The nodes defined in the dbt project and its dependencies"
    )
    sources: Dict[str, ParsedSourceDefinition] = Field(
        ..., description="The sources defined in the dbt project and its dependencies"
    )
    macros: Dict[str, ParsedMacro] = Field(
        ..., description="The macros defined in the dbt project and its dependencies"
    )
    docs: Dict[str, ParsedDocumentation] = Field(
        ..., description="The docs defined in the dbt project and its dependencies"
    )
    exposures: Dict[str, ParsedExposure] = Field(
        ..., description="The exposures defined in the dbt project and its dependencies"
    )
    metrics: Optional[Dict[str, ParsedMetric]] = Field(
        None, description="The metrics defined in the dbt project and its dependencies"
    )
    selectors: Dict[str, Any] = Field(
        ..., description="The selectors defined in selectors.yml"
    )
    disabled: Optional[
        Union[
            Dict[
                str,
                List[Any],
            ],
            List[Any],
        ]
    ] = Field(None, description="A mapping of the disabled nodes in the target")
    parent_map: Optional[Optional[Dict[str, List[str]]]] = Field(
        None, description="A mapping from\xa0child nodes to their dependencies"
    )
    child_map: Optional[Optional[Dict[str, List[str]]]] = Field(
        None, description="A mapping from parent nodes to their dependents"
    )

    _project_name: Optional[str] = None

    @property
    def project_name(self) -> Optional[str]:
        """Attempt to infer the project's name from nodes in the project."""
        if self._project_name:
            return self._project_name

        nodes = list(self.nodes.values())
        if len(nodes) == 0:
            self._project_name = None
        else:
            self._project_name = nodes[0].package_name

        return self._project_name

    @property
    def project_nodes(self) -> Dict[str, Node]:
        """Get a dict of all nodes that are owned by this project and not a package."""

        return {
            key: value
            for key, value in self.nodes.items()
            if value.package_name == self.project_name
        }

    @property
    def project_sources(self) -> Dict[str, ParsedSourceDefinition]:
        """Get a dict of all sources that are owned by this project and not a package."""

        return {
            key: value
            for key, value in self.sources.items()
            if value.package_name == self.project_name
        }

    @property
    def project_exposures(self) -> Dict[str, ParsedExposure]:
        """Get a dict of all exposures that are owned by this project and not a package."""

        return {
            key: value
            for key, value in self.exposures.items()
            if value.package_name == self.project_name
        }
