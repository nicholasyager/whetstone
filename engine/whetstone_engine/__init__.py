import logging
from typing import Optional, Set, List, Type, Tuple, TypeVar

from networkx import DiGraph
from whetstone_api.schemas.demo.results import DemoResultCreate
from whetstone_api.schemas.results import ResultCreate

from whetstone_engine.models.dbt.manifests import DbtManifest
from whetstone_engine.models.dbt.run_results import DbtRunResults
from whetstone_engine.rulesets import (
    RuleSet,
    GraphRepository,
    EngineResult,
    EngineStatistic,
)

logger = logging.getLogger(__name__)
ResultCreationType = TypeVar("ResultCreationType", DemoResultCreate, ResultCreate)


def create_graph(manifest: DbtManifest) -> DiGraph:
    """
    Generate a NetworkX graph object using a manifest as the source of the
    edge and vertex data.
    """

    graph = DiGraph()

    nodes = [
        node.unique_id
        for _, node in manifest.nodes.items()
        if node.resource_type != "test"
    ]
    graph.add_nodes_from(nodes)

    sources = [source.unique_id for _, source in manifest.sources.items()]
    graph.add_nodes_from(sources)

    child_edges = [
        (key, value)
        for key, values in manifest.child_map.items()
        for value in values
        if not key.startswith("test.") and not value.startswith("test.")
    ]
    graph.add_edges_from(child_edges)

    parent_edges = [
        (value, key)
        for key, values in manifest.parent_map.items()
        for value in values
        if not key.startswith("test.") and not value.startswith("test.")
    ]
    graph.add_edges_from(parent_edges)

    # Remove nodes from different packages.
    graph.remove_nodes_from(
        [node for node in graph.nodes if node.split(".")[1] != manifest.project_name]
    )

    return graph


class Engine:
    """
    The Engine class is the main container for Whetstone Engine usage. It is
    provided with Manifest and RunResult data, and it executes Rules against
    its internal representation of the DAG to generate a set of Whetstone
    Results.
    """

    def __init__(self):
        logger.debug("Initializing Engine.")
        self._rule_set_names: Set = set()
        self._rule_sets: List[RuleSet] = []

    def register_ruleset(self, generator: Type[RuleSet], **kwargs) -> None:
        """Register a RuleSet with the engine for use _ruleset_names the next run."""
        ruleset_name = generator().__class__.__name__
        if ruleset_name in self._rule_set_names:
            logger.info(f"Skipping registration of {ruleset_name}")
            return

        self._rule_sets.append(generator(**kwargs))
        self._rule_set_names.add(ruleset_name)
        logger.info(
            f"Registered {self._rule_sets[-1].__class__.__name__} RuleSet with "
            f"the Engine"
        )

    def process(
        self,
        result_type: Type[ResultCreationType],
        manifest: DbtManifest,
        run_results: Optional[DbtRunResults] = None,
        result_args: dict = None,
    ) -> Tuple[
        List[Tuple[ResultCreationType, Set[str], Set[str]]],
        List[EngineStatistic],
        DiGraph,
    ]:
        """
        Process a manifest and an optional run_result to generate a set of results.
        """

        results: List[Tuple[result_type, Set[str]]] = []
        statistics: List[EngineStatistic] = []

        logger.info(
            f"Processing manifest file {manifest.metadata.invocation_id}",
        )
        graph: DiGraph = create_graph(manifest)
        graph_repository = GraphRepository(graph)
        logger.info(
            f"Created a graph with {graph.number_of_nodes()} and "
            f"{graph.number_of_edges()} edges",
        )

        if len(self._rule_sets) == 0:
            logger.warning("No rule sets were registered with the Engine.")

            statistics.append(
                EngineStatistic(
                    label="Results",
                    statistic="results",
                    value=len(results),
                    type="number",
                )
            )
            return results, statistics, graph

        for rule_set in self._rule_sets:
            new_results: List[EngineResult] = rule_set(
                manifest=manifest,
                graph_repository=graph_repository,
                run_results=run_results,
            )

            statistics.extend(
                [
                    statistic
                    for statistic in rule_set.statistics()
                    if statistic is not None
                ]
            )
            results.extend(
                [
                    (
                        result_type(
                            **(result.dict()), **(result_args if result_args else {})
                        ),
                        result.nodes,
                        result.secondary_nodes,
                    )
                    for result in new_results
                ]
            )

        statistics.append(
            EngineStatistic(
                label="Results", statistic="results", value=len(results), type="number"
            )
        )

        return results, statistics, graph
