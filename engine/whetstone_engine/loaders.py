import hashlib
import io
import logging
import time
import json
from typing import TypeVar, Union, Type

from networkx import DiGraph

from .config import (
    STORAGE_URL,
    STORAGE_ACCESS_KEY,
    STORAGE_SECRET_KEY,
    STORAGE_BUCKET,
    STORAGE_USE_SSL,
)
from whetstone_engine.models.dbt.manifests import DbtManifest
from whetstone_engine.models.dbt.run_results import DbtRunResults

import click
from minio import Minio

logger = logging.getLogger(__name__)

Artifact = TypeVar("Artifact", bound=Union[DbtManifest, DbtRunResults, DiGraph])


def load_local_artifact(artifact_type: Type[Artifact], file: click.File) -> Artifact:
    """Load an Artifact from a provided File descriptor."""
    logger.info(f"Loading {artifact_type.__name__} from {file.name}")
    start_time = time.time()
    data = artifact_type(**json.loads(file.read()))
    execution_time = round((time.time() - start_time), 5)
    logger.info(f"{artifact_type.__name__} {file.name} loaded in {execution_time}s")
    return data


def load_artifact_s3(artifact_type: Type[Artifact], key: str) -> Artifact:
    """Load an Artifact from Storage and return the deserialized result."""

    client = Minio(
        STORAGE_URL,
        access_key=STORAGE_ACCESS_KEY,
        secret_key=str(STORAGE_SECRET_KEY),
        secure=STORAGE_USE_SSL,
    )

    logger.info(
        f"Getting {artifact_type.__name__} artifact from {STORAGE_BUCKET}/{key}."
    )
    start_time = time.time()
    response = None
    try:
        response = client.get_object(bucket_name=STORAGE_BUCKET, object_name=key)
        artifact: Artifact = artifact_type(**json.load(response))

        execution_time = round((time.time() - start_time), 5)
        logger.info(
            f"{artifact_type.__name__} artifact {key} loaded in "
            f"{execution_time} seconds."
        )
    finally:
        if response:
            response.close()
            response.release_conn()

    return artifact


def put_artifact_in_s3(artifact_type: Type[Artifact], artifact: dict) -> str:
    client = Minio(
        STORAGE_URL,
        access_key=STORAGE_ACCESS_KEY,
        secret_key=str(STORAGE_SECRET_KEY),
        secure=STORAGE_USE_SSL,
    )

    key = hashlib.sha1(json.dumps(artifact, sort_keys=True).encode()).hexdigest()
    logger.info(
        f"Putting {artifact_type.__name__} artifact into {STORAGE_BUCKET}/{key}."
    )
    start_time = time.time()

    data = io.BytesIO(json.dumps(artifact).encode())
    client.put_object(
        bucket_name=STORAGE_BUCKET,
        object_name=key,
        data=data,
        length=data.getbuffer().nbytes,
    )
    execution_time = round((time.time() - start_time), 5)
    logger.info(
        f"{artifact_type.__name__} artifact {key} uploaded in "
        f"{execution_time} seconds."
    )
    return key
