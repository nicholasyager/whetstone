import datetime
import logging
from typing import Optional, Tuple, List, Set

from celery import Celery
from celery.signals import task_failure, task_prerun, task_success
from networkx import DiGraph
from whetstone_api.schemas.process_request import ProcessRequest
from whetstone_api.schemas.run_statistics import RunStatisticCreate
from whetstone_api.schemas.runs import Run, RunUpdate, RunStatus

from whetstone_engine.utilities.result_groups.result_group_aggregator import (
    ResultGroupConstructor,
)
from . import Engine, DbtManifest, DbtRunResults
from .config import REDIS_HOST, REDIS_PORT, API_URL
from .loaders import load_artifact_s3, put_artifact_in_s3
from .rulesets import EngineStatistic
from .rulesets.dag_structure import DAGStructureRuleSet
from .rulesets.execution import ExecutionRuleSet
from .rulesets.tests_and_documentation import TestAndDocsRuleSet
from .utilities.api import WhetstoneAPI, generate_request_parameters
from .utilities.graphs import graph_to_json_dict
from .utilities.result_groups.content_generator import ContentGenerator

logger = logging.getLogger(__name__)

app = Celery("engine", broker=f"redis://{REDIS_HOST}:{REDIS_PORT}/0")
api = WhetstoneAPI(base_url=API_URL)

rulesets = [ExecutionRuleSet, DAGStructureRuleSet, TestAndDocsRuleSet]


class RunNotFoundException(Exception):
    """The requested run was not present in the Whetstone API."""


def parse_manifest_and_run_results(process_request_str: str):
    process_request = ProcessRequest.parse_raw(process_request_str)

    manifest: DbtManifest = load_artifact_s3(
        DbtManifest,
        process_request.manifest_location,
    )

    run_results: Optional[DbtRunResults] = None
    if process_request.run_results_location:
        run_results = load_artifact_s3(
            DbtRunResults, process_request.run_results_location
        )
    else:
        logger.warning("No run_result_location provided.")

    return process_request, manifest, run_results


@app.task(name="engine.process_run")
def process_run(process_request_str: str):
    process_request, manifest, run_results = parse_manifest_and_run_results(
        process_request_str
    )
    request_parameters = generate_request_parameters(process_request)

    engine = Engine()
    for ruleset in rulesets:
        engine.register_ruleset(ruleset)

    api.partial_update(
        resource_type=request_parameters.run_type,
        **request_parameters.id_parameters,
        payload=RunUpdate(status=RunStatus.running, started_at=datetime.datetime.now()),
    )

    results_tuple: Tuple[
        List[Tuple[request_parameters.result_create_type, Set[str], Set[str]]],
        List[EngineStatistic],
        DiGraph,
    ] = engine.process(
        result_type=request_parameters.result_create_type,
        manifest=manifest,
        run_results=run_results,
        result_args=request_parameters.result_args,
    )
    results, statistics, graph = results_tuple

    # Store the Graph
    serialized_graph = graph_to_json_dict(graph)

    graph_key = put_artifact_in_s3(DiGraph, serialized_graph)
    api.partial_update(
        resource_type=request_parameters.run_type,
        **request_parameters.id_parameters,
        payload=request_parameters.run_update_type(graph_location=graph_key),
    )

    if len(results) < 1:
        return process_request

    # Construct ResultGroups
    logger.info("Starting ResultGroup construction.")
    content_generator = ContentGenerator(manifest=manifest, graph=graph)
    result_group_constructor = ResultGroupConstructor(
        result_group_type=request_parameters.result_group_create_type,
        content_generator=content_generator,
    )
    result_group_constructor.add_results(
        results, result_group_args=request_parameters.result_args
    )
    result_group_constructor.generate_content()
    logger.info(
        f"ResultGroup construction complete. {result_group_constructor.count()} ResultGroups created."
    )

    logger.info("Creating ResultGroups")
    api.create_many(
        resource_type=request_parameters.result_group_type,
        run_id=process_request.run_id,
        **request_parameters.correlation_parameters,
        payload=result_group_constructor.result_groups(),
    )
    logger.info("Completed creation of ResultGroups")

    # Store Result Statistics

    logger.warning(statistics)
    statistic_list = [
        RunStatisticCreate(run_id=process_request.run_id, **(statistic.dict()))
        for statistic in statistics
    ]
    logger.warning(statistic_list[0].label)

    api.create_many(
        resource_type=request_parameters.run_statistic_type,
        **request_parameters.correlation_parameters,
        payload=[
            RunStatisticCreate(run_id=process_request.run_id, **(statistic.dict()))
            for statistic in statistics
        ],
    )

    return process_request


@app.task(name="engine.process_demo_run")
def process_demo_run(process_request_str: str):
    return process_run(process_request_str)


@task_prerun.connect(sender=process_run)
def task_prerun_handler(**kwargs):
    """Report Engine startup back to the API."""

    try:
        process_request = ProcessRequest.parse_raw(kwargs["args"][0])
        request_parameters = generate_request_parameters(process_request)

        # Get the Run
        run: Optional[Run] = api.get(
            resource_type=request_parameters.run_type,
            **request_parameters.id_parameters,
        )

        if run is None:
            raise RunNotFoundException()

        # Set the dequeue time and set status to starting.
        api.partial_update(
            resource_type=request_parameters.run_type,
            **request_parameters.id_parameters,
            payload=request_parameters.run_update_type(
                status=RunStatus.starting, dequeued_at=datetime.datetime.now()
            ),
        )

        # Invalidate any existing results for this run.
        api.invalidate_results(
            resource_type=request_parameters.group_type,
            run_id=process_request.run_id,
            **request_parameters.correlation_parameters,
        )

    except Exception as e:
        logger.exception(e)
        app.control.revoke(kwargs["task_id"], terminate=True)


@task_prerun.connect(sender=process_demo_run)
def task_prerun_demo_handler(**kwargs):
    return task_prerun_handler(**kwargs)


@task_failure.connect(sender=process_run)
def task_failure_handler(**kwargs):
    """Report Engine failures back to the API."""

    if isinstance(kwargs["exception"], RunNotFoundException):
        logger.error("The requested Run was not found. Returning.")
        return

    process_request = ProcessRequest.parse_raw(kwargs["args"][0])
    request_parameters = generate_request_parameters(process_request)

    api.partial_update(
        resource_type=request_parameters.run_type,
        **request_parameters.id_parameters,
        payload=request_parameters.run_update_type(
            status=RunStatus.error, finished_at=datetime.datetime.now()
        ),
    )


@task_failure.connect(sender=process_demo_run)
def task_failure_demo_handler(**kwargs):
    """Report Engine failures back to the API."""
    task_failure_handler(**kwargs)


@task_success.connect(sender=process_run)
def task_success_handler(**kwargs):
    """Report Engine success back to the API."""
    process_request: ProcessRequest = kwargs["result"]
    request_parameters = generate_request_parameters(process_request)
    api.partial_update(
        resource_type=request_parameters.run_type,
        **request_parameters.id_parameters,
        payload=request_parameters.run_update_type(
            status=RunStatus.success, finished_at=datetime.datetime.now()
        ),
    )


@task_success.connect(sender=process_demo_run)
def task_success_demo_handler(**kwargs):
    """Report Engine success back to the API."""
    task_success_handler(**kwargs)
