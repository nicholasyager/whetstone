#!/usr/bin/env python
import codecs
import os
import pathlib

from setuptools import setup

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / "README.md").read_text(encoding="utf-8")


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), "r") as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith("__version__"):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


setup(
    name="whetstone-engine",
    version=get_version("whetstone_engine/__version__.py"),
    description="Result engine for Whetstone",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Nicholas Yager",
    author_email="yager@nicholasyager.com",
    url="https://gitlab.com/nicholasyager/whetstone",
    packages=["whetstone_engine"],
    python_requires=">=3.6, <4",
)
