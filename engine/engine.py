import logging
from typing import Optional, Tuple, List, Set

import click
from networkx import DiGraph
from whetstone_api.schemas.result_groups import ResultGroupCreate
from whetstone_api.schemas.results import ResultCreate
from whetstone_api.schemas.runs import Run
from whetstone_api.schemas.process_request import ProcessRequest

from whetstone_engine import Engine, DbtManifest, DbtRunResults, EngineStatistic
from whetstone_engine.loaders import load_local_artifact
from whetstone_engine.rulesets.dag_structure import DAGStructureRuleSet
from whetstone_engine.rulesets.execution import ExecutionRuleSet
from whetstone_engine.utilities.api import WhetstoneAPI
from whetstone_engine.utilities.result_groups.content_generator import ContentGenerator
from whetstone_engine.utilities.result_groups.result_group_aggregator import (
    ResultGroupConstructor,
)

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


@click.group()
def cli():
    pass


@cli.command()
@click.option("--manifest", "manifest_file", required=True, type=click.File("r"))
@click.option("--run-results", "run_result_file", type=click.File("r"))
def process_run(
    manifest_file: click.File, run_result_file: Optional[click.File] = None
) -> None:
    """Manually process a run using the provided manifest and run_result files."""

    engine = Engine()
    manifest = load_local_artifact(DbtManifest, manifest_file)

    if run_result_file:
        run_results = load_local_artifact(DbtRunResults, run_result_file)
    else:
        logger.warning("No --run-result file provided.")
        run_results = None

    engine.register_ruleset(ExecutionRuleSet)
    engine.register_ruleset(DAGStructureRuleSet)

    output: Tuple[
        List[Tuple[ResultCreate, Set[str]]], List[EngineStatistic], DiGraph
    ] = engine.process(
        result_type=ResultCreate, manifest=manifest, run_results=run_results
    )
    results, statistics, graph = output

    logger.info("Starting ResultGroup Construction")

    content_generator = ContentGenerator(manifest=manifest, graph=graph)
    result_group_constructor = ResultGroupConstructor(
        result_group_type=ResultGroupCreate,
        content_generator=content_generator,
    )
    logger.info("Adding results to results group")
    result_group_constructor.add_results(results, result_group_args={})
    logger.info("Completed added results to results group.")
    logger.info("Generating content for results groups")
    result_group_constructor.generate_content()
    logger.info(
        f"ResultGroup construction complete. {result_group_constructor.count()} ResultGroups created."
    )

    return

    for result_group in result_group_constructor.result_groups():
        print(result_group.entity)
        for result in result_group.results:
            print("\t" + result.content["name"])


@cli.command()
@click.option("--run-id", required=True)
@click.option("--account-id", required=True)
@click.option("--manifest", "manifest_path", required=True)
@click.option("--run-results", "run_result_path", type=str)
def worker(
    run_id: int,
    account_id: int,
    manifest_path: str,
    run_result_path: Optional[str] = None,
):
    """
    Send details about the Run to process using a distributed Celery worker.
    """

    from whetstone_engine.worker import process_run

    process_run.delay(
        ProcessRequest(
            run_id=run_id,
            account_id=account_id,
            manifest_location=manifest_path,
            run_results_location=run_result_path,
        ).json()
    )


@cli.command()
@click.option("--account-id", required=True)
@click.option("--run-id", required=True)
def get_run(account_id: int, run_id: int):
    client = WhetstoneAPI("http://127.0.0.1:8000")
    run = client.get(resource_type=Run, resource_id=run_id, account_id=account_id)
    print(run)


if __name__ == "__main__":
    cli()
