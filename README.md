<div align="center">
<img width=500px src="app/whetstone/public/full_logo.png">
</div>

<div align="center">
<img src="https://img.shields.io/gitlab/pipeline-status/nicholasyager/whetstone?branch=main">
<img src="https://img.shields.io/gitlab/v/release/nicholasyager/whetstone">
<img src="https://img.shields.io/badge/license-BSL%201.1-blue">
</div>
<div align="center">

[Whetstone](https://whetstone.nicholasyager.com) |
[Demo](https://whetstone.nicholasyager.com/demo)

</div>

Whetstone is a service that analyzes dbt projects metadata and artifacts to make recommendations that sharpen your project's
structure, testing, documentation, and execution characteristics.

[[_TOC_]]

## Features

- Recommendation generation for Graph Structure, Execution, Testing, and Documentation.
- Graph views for you dbt project, and per-result subgraph visualization
- Graph complexity and test coverage reporting.
- Support for accounts with multiple projects.
- Support for using dbt Cloud webhooks to trigger Whetstone analysis runs.
- [Open API](https://whetstone.nicholasyager.com/api/redoc) for automatic Whetstone runs at the end of your dbt orchestration process.

### Whetstone Rule Sets

Whetstone currently supports the following rules, with many more on the way!

#### Graph Structure

- Root Models: Models that exist without referencing another model, source, or seed. This indicates that the model may be using direct table references.
- Model Fanout: A model that is being referenced by many other models. This structure typically happens when there is a filtering or type field in the model that ought to be pushed into filters in the BI layer.
- Rejoining of Upstream Concepts: The model has an upstream dependency that is also referenced by other upstream depenencies. This type of rejoining indicates that the model's upstream dependency is missing necessary fields, or is at an inappropriate grain.
- Joined Sources: The model joins together multiple sources. This is important, since all sources should have a corresponding staging model. If all sources have a corresponding staging model, then this model would be referencing and joining the staging models instead.
- Joined Refs and Sources: The model has both models and sources as upstream dependencies. This is important, since all sources should have a corresponding staging model.
- Chained Views: A model references a long (4+) chain of models materialzed as views. This can result in performance issues in Snowflake and other data warehouse technologies if the upstream model logic is difficult for the warehouse to compile.
- Source Fanout: A source that has been reference by multiple other models. This indicates that there is not a single staging model for this source, which is a strong indicator that there is repeated logic in the source's downstream consumers.

#### Execution

- Slow-running queries: The model execution time is prolonged, indicating that it may be a good candidiate for incremental materialization.

#### Testing and Documentation

- Missing Primary Key Tests: The model is missing a column with both `unique` and `not_null` tests. These are important, since being able to detect duplicate data is vital for the long-term health of your project.
- Missing Model Documentation: The model does not have a description. Documentation is important for the long-term maintanablity of your project, so we recommend all models have at least basic documentation describing the data contained and any business logic being used.

### Upcoming Rules

- Node selection simplification: Evaluate a run's [node selections](https://docs.getdbt.com/reference/node-selection/syntax) , and use the dbt project's structure to recommend a simplified selector.
- Contribution to Complexity: A model's individual contribution to the graph's overall complexity. Complexity is defined as the graph's [meshedness coefficient](https://en.wikipedia.org/wiki/Meshedness_coefficient).
- Thread optimization: Examination of the project's structure and run results to identify an optiminal number of threads to use during this dbt run.

# Contributing

Contributions are the life-blood of any successful project! To get started, please follow these steps:

1. File an issue to notify the maintainers about what you're working
   on.
2. Fork the repo, develop and test your code changes.
3. Make sure that your commit messages clearly describe the changes.
4. Send a pull request.

## Local Setup

You can get started using Whetstone locally using Docker Compose and Yarn. The API and other back-end services are started by using the provided `Makefile`.

```bash
make start
```

This will build and start all of the resources defined in the `docker/docker-compose.local.yml` Docker compose file. The API will then be avaiable at http://localhost:8000.

The front-end can be started using yarn.

```bash
cd app/whetstone
yarn start
```

This will build and host the React web app. The web app will be available at http://localhost:3000.

# License

The Business Source License 1.1 - Nicholas Yager. Please have a look at the [LICENSE](https://gitlab.com/nicholasyager/whetstone/-/blob/main/LICENSE) for more details.
