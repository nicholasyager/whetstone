from typing import Callable
from fastapi import FastAPI

from .dependencies.celery import create_celery_app
from .dependencies.database import connect_to_db, close_db_connection
from .dependencies.storage import create_client


def create_start_app_handler(app: FastAPI) -> Callable:
    async def start_app() -> None:
        await connect_to_db(app)
        await create_client(app)
        await create_celery_app(app)

    return start_app


def create_stop_app_handler(app: FastAPI) -> Callable:
    async def stop_app() -> None:
        await close_db_connection(app)

    return stop_app
