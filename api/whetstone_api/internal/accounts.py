from fastapi import APIRouter, Depends
from starlette.status import HTTP_201_CREATED

from whetstone_api.dependencies.database import get_repository
from whetstone_api.repositories.accounts import AccountRepository
from whetstone_api.schemas.accounts import Account, AccountCreate

router = APIRouter(prefix="/accounts")


@router.post(
    "",
    response_model=Account,
    status_code=HTTP_201_CREATED,
)
async def create_account(
    account: AccountCreate,
    repo: AccountRepository = Depends(get_repository(AccountRepository)),
) -> Account:
    """
    The Create Account endpoint is user to create a new Whetstone account
    with the desired name and plan type.
    """
    return await repo.create(new_account=account)
