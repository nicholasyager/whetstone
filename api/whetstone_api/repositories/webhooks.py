import json

from databases import Database
from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST
from typing import Optional, List

from pydantic.types import UUID

from whetstone_api.repositories import BaseRepository
from whetstone_api.schemas.webhooks import (
    WebhookInDB,
    SensitiveWebhookInDB,
    WebhookCreate,
    WebhookUpdate,
)
from whetstone_api.services import crypto_service


class WebhookRepository(BaseRepository):
    """Database actions relating to Webhooks."""

    _register_query = """
        insert into webhooks (id, account_id, project_id, "system", "authorization", credential, is_active)
        values (:id, :account_id, :project_id, :system, :authorization, :credential, true)
        returning *;
    """

    _list_by_account_and_project = """
        select
            id,
            account_id,
            project_id,
            "system",
            "authorization",
            is_active,
            created_at,
            updated_at
        from webhooks
        where
            account_id = :account_id
            and project_id = :project_id;
    """

    _update_webhook = """
        update webhooks
        set
            is_active = :is_active,
            credential = :credential,
            "authorization" = :authorization
        where
            id = :id
            and account_id = :account_id
            and project_id = :project_id
        returning id, account_id, project_id, "system", "authorization", is_active, created_at, updated_at;
    """

    _get_by_id_and_project = """
        select
            id,
            account_id,
            project_id,
            "system",
            "authorization",
            is_active,
            created_at,
            updated_at
        from webhooks
        where
            account_id = :account_id
            and project_id = :project_id
            and id = :id;
    """

    _get_sensitive_webhook_by_id_and_project = """
        select
            id,
            account_id,
            project_id,
            "system",
            "authorization",
            credential,
            is_active,
            created_at,
            updated_at
        from webhooks
        where
            account_id = :account_id
            and project_id = :project_id
            and id = :id;
    """

    _get_sensitive_webhook_by_id = """
        select
            id,
            account_id,
            project_id,
            "system",
            "authorization",
            credential,
            is_active,
            created_at,
            updated_at
        from webhooks
        where
            id = :id;
    """

    def __init__(self, db: Database):
        self.crypto = crypto_service
        super().__init__(db)

    async def register_webhook(
        self, account_id: int, project_id: int, new_webhook: WebhookCreate
    ):
        """Register a new webhook for a project."""

        credential = self.crypto.encrypt(
            plaintext=new_webhook.service_token, header=str(new_webhook.id)
        )

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            created_webhook = await connection.fetch_one(
                query=self._register_query,
                values={
                    "id": new_webhook.id,
                    "account_id": account_id,
                    "project_id": project_id,
                    "system": new_webhook.system,
                    "authorization": new_webhook.authorization,
                    "credential": credential.dict(),
                },
            )

            return WebhookInDB(**created_webhook)

    async def list_webhooks_for_project(
        self, account_id: int, project_id: int
    ) -> List[WebhookInDB]:
        """List the webhooks associated with a project."""

        webhooks = await self.db.fetch_all(
            self._list_by_account_and_project,
            values={
                "account_id": account_id,
                "project_id": project_id,
            },
        )
        return [WebhookInDB(**webhook) for webhook in webhooks]

    async def update_webhook(
        self,
        account_id: int,
        project_id: int,
        webhook_id: UUID,
        webhook_update: WebhookUpdate,
    ):
        """Update a specific webhook."""

        webhook: SensitiveWebhookInDB = await self.get_sensitive_webhook(
            webhook_id=webhook_id, account_id=account_id, project_id=project_id
        )

        if webhook is None:
            return None

        if webhook_update.is_active is not None:
            webhook.is_active = webhook_update.is_active

        if webhook_update.authorization is not None:
            webhook.authorization = webhook_update.authorization

        if webhook_update.service_token is not None:
            webhook.credential = self.crypto.encrypt(
                plaintext=webhook_update.service_token, header=str(webhook.id)
            )

        try:
            updated_webhook = await self.db.fetch_one(
                query=self._update_webhook,
                values={
                    "id": webhook.id,
                    "account_id": account_id,
                    "project_id": project_id,
                    "is_active": webhook.is_active,
                    "authorization": webhook.authorization,
                    "credential": webhook.credential.dict(),
                },
            )
            return WebhookInDB(**updated_webhook)

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update parameters for a Webhook.",
            )

    async def get_webhook(
        self, webhook_id: UUID, account_id: int, project_id: int
    ) -> Optional[WebhookInDB]:
        """Get a specific webhook by id."""

        webhook = await self.db.fetch_one(
            self._get_by_id_and_project,
            values={
                "id": webhook_id,
                "account_id": account_id,
                "project_id": project_id,
            },
        )

        if webhook is None:
            return None

        return WebhookInDB(**webhook)

    async def get_sensitive_webhook(
        self, webhook_id: UUID, account_id: int, project_id: int
    ) -> Optional[SensitiveWebhookInDB]:
        """Get a specific webhook by id, including the secret Service Token."""

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )
            webhook = await connection.fetch_one(
                self._get_sensitive_webhook_by_id_and_project,
                values={
                    "id": webhook_id,
                    "account_id": account_id,
                    "project_id": project_id,
                },
            )

            if webhook is None:
                return None

            return SensitiveWebhookInDB(**webhook)

    async def get_sensitive_webhook_by_id(
        self, webhook_id: UUID
    ) -> Optional[SensitiveWebhookInDB]:
        """Get a specific webhook by id, including the secret Service Token."""

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )
            webhook = await connection.fetch_one(
                self._get_sensitive_webhook_by_id,
                values={"id": webhook_id},
            )

            if webhook is None:
                return None

            return SensitiveWebhookInDB(**webhook)
