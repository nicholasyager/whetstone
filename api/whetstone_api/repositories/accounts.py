from typing import Optional

from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST

from . import BaseRepository
from whetstone_api.schemas.accounts import AccountCreate, AccountUpdate, AccountInDB


class AccountRepository(BaseRepository):
    """Database actions relating to the Accounts resource"""

    _create_query = """
        INSERT INTO accounts (name, status, plan)
        VALUES (:name, :status, :plan)
        RETURNING id, name, status, plan, created_at, updated_at;
    """

    _get_by_id_query = """
        select *
        from accounts
        where id = :id;
    """

    _list_query = """
        select *
        from accounts;
    """

    _update_query_by_id = """
        update accounts
        set
            name = :name
        where id = :id
        RETURNING id, name, status, plan, created_at, updated_at;
    """

    async def create(self, *, new_account: AccountCreate) -> AccountInDB:
        """Store an account within the repository."""

        query_values = new_account.dict()
        account = await self.db.fetch_one(query=self._create_query, values=query_values)
        return AccountInDB(**account)

    async def get(self, *, account_id: int) -> Optional[AccountInDB]:
        """Store an account within the repository."""

        account = await self.db.fetch_one(
            query=self._get_by_id_query, values={"id": account_id}
        )

        if account is None:
            return None

        return AccountInDB(**account)

    async def list(self) -> [AccountInDB]:
        """Store an account within the repository."""

        accounts = await self.db.fetch_all(query=self._list_query)
        return [AccountInDB(**account) for account in accounts]

    async def update(
        self, *, account_id: int, account_update: AccountUpdate
    ) -> Optional[AccountInDB]:
        """Update an account using an AccountUpdate object"""
        account = await self.get(account_id=account_id)

        if not account:
            return None

        account_update_params = account.copy(
            update=account_update.dict(exclude_unset=True),
        )

        try:
            updated_account = await self.db.fetch_one(
                query=self._update_query_by_id,
                values={
                    "id": account_update_params.id,
                    "name": account_update_params.name,
                },
            )
            return AccountInDB(**updated_account)

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update params.",
            )
