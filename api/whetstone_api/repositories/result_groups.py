import json
import logging
from typing import Optional, List, Set, Tuple, Dict

from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_422_UNPROCESSABLE_ENTITY

from . import BaseRepository
from .results import ResultRepository
from ..dependencies.filtering import ResultGroupFilters
from ..dependencies.pagination import OrderBy
from ..schemas.result_groups import (
    ResultGroupCreate,
    ResultGroupInDB,
    ResultGroupUpdate,
)
from ..schemas.results import ResultInDB

logger = logging.getLogger(__name__)


class ResultGroupsRepository(BaseRepository):
    """Database actions relating to the ResultGroup resource"""

    _check_run = """
           SELECT id
           from runs
           where
               id = :id and
               account_id = :account_id;
       """

    _create_query = """
        INSERT INTO result_groups (run_id, account_id, entity, entity_id, entity_type, content, is_stale, filters)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
        RETURNING *;
    """

    _get_by_id_query = """
        select *
        from result_groups
        where
            id = :id and
            run_id = :run_id and
            account_id = :account_id
        limit 1;
    """

    @staticmethod
    def _build_filter_clauses(
        result_filters: Optional[ResultGroupFilters],
    ) -> Optional[str]:
        if not result_filters:
            return None

        filters = []
        values = result_filters.sql_values
        if result_filters.severity:
            # This is icky, but it works for now. Run tests to confirm if injection is possible.
            filters.append(
                f"expanded_result_groups.value ->> 'severity' in ({values['severity']})"
            )

        if result_filters.rule_set:
            filters.append(
                f"expanded_result_groups.value ->> 'rule_set' in ({values['rule_set']})"
            )

        if result_filters.entity_id:
            filters.append("result_groups.entity_id like :entity_id")

        if result_filters.unique_ids:
            filters.append(f"result_groups.entity_id in ({values['unique_ids']})")

        if len(filters) == 0:
            return None

        return " and ".join(filters)

    def _list_by_account_and_run_query(
        self,
        include_stale: bool = False,
        limit=None,
        offset=None,
        result_filters: ResultGroupFilters = None,
    ) -> str:

        filter_string = self._build_filter_clauses(result_filters)

        return f"""

        with

        expanded_result_groups as (
            select
                id, value
            from result_groups, jsonb_array_elements(result_groups.filters)
            where
                account_id = :account_id and
                run_id = :run_id
                {'and is_stale is false' if not include_stale else ''}
        )

        select distinct
            result_groups.*
        from result_groups
        left join expanded_result_groups on
            result_groups.id = expanded_result_groups.id
        where
            account_id = :account_id and
            run_id = :run_id
            {'and is_stale is false' if not include_stale else ''}
            {'and ' + filter_string if filter_string else ''}
        order by severity_score desc, entity
        {'limit :limit' if limit else ''}
        {'offset :offset' if offset else ''};
    """

    def _count_by_account_and_run_query(
        self, include_stale: bool = False, result_filters: ResultGroupFilters = None
    ) -> str:

        filter_string = self._build_filter_clauses(result_filters)

        return f"""

        with

        expanded_result_groups as (
            select
                id, value
            from result_groups, jsonb_array_elements(result_groups.filters)
            where
                account_id = :account_id and
                run_id = :run_id
                {'and is_stale is false' if not include_stale else ''}
        )

        select
            count(distinct result_groups.id)
        from result_groups
        left join expanded_result_groups on
            result_groups.id = expanded_result_groups.id
        where
            account_id = :account_id and
            run_id = :run_id
            {'and is_stale is false' if not include_stale else ''}
            {'and ' + filter_string if filter_string else ''}
    """

    _invalidation_query = """
        update result_groups
        set
            is_stale = true
        where
            account_id = :account_id and
            run_id = :run_id and
            is_stale = false
        RETURNING *;
    """

    _update_query_by_id = """
        update result_groups
        set
            entity = :entity,
            entity_id = :entity_id,
            entity_type = :entity_type,
            content = :content,
            is_stale = :is_stale
        where
            id = :id and
            account_id = :account_id and
            run_id = :run_id
        RETURNING *;
    """

    _recalculate_scores_query = """
        with

        run_results as (
            select
                *,
                case
                    when severity = 'low' then 1
                    when severity = 'medium' then 10
                    when severity = 'high' then 100
                end as severity_score
            from results
            where result_group_id in ( :result_group_ids )
        ),

        sort_order as (
            select
                result_group_id,
                count(id) as result_count,
                sum(severity_score) as total_severity
            from run_results
            group by 1
        )


        update result_groups
        set
            severity_score = sort_order.total_severity,
            result_count  = sort_order.result_count
        from sort_order
        where result_groups.id = sort_order.result_group_id
        returning result_groups.*;
    """

    def preprocess_query(self, query: str, values: dict) -> Tuple[str, dict]:
        """Handle some early pre-processing for queries that need select in list."""

        for parameter_name in list(values.keys()):
            if (
                isinstance(values[parameter_name], (tuple, set, list))
                and f":{parameter_name}" in query
            ):
                query = query.replace(
                    f":{parameter_name}",
                    f"{','.join([str(x) for x in values[parameter_name]])}",
                )
                # then remove the values from the value list
                del values[parameter_name]

        return query, values

    async def recalculate_scores(
        self, *, result_group_ids: Set[int]
    ) -> List[ResultGroupInDB]:
        """Recalculate the result_count and severity_score for a set of ResultGroup. Return the updated ResultGroups."""
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            query, values = self.preprocess_query(
                self._recalculate_scores_query, {"result_group_ids": result_group_ids}
            )

            result_group_responses = await connection.fetch_all(
                query=query, values=values
            )

            return [
                ResultGroupInDB(**result_group)
                for result_group in result_group_responses
            ]

    async def create(
        self,
        *,
        account_id: int,
        run_id: int,
        new_result_groups: [ResultGroupCreate],
    ) -> List[Tuple[ResultGroupInDB, List[ResultInDB]]]:
        """Store ResultGroups within the repository."""

        # Verify that the run associated with the ResultGroup is part of the linked
        # account.
        run = await self.db.fetch_one(
            query=self._check_run,
            values={"id": run_id, "account_id": account_id},
        )

        if run is None:
            raise HTTPException(
                status_code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail="The supplied run_id is not part of the account that"
                " this ResultGroup is associated with.",
            )

        result_repository = ResultRepository(self.db)

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            # Create ResultGroups
            result_group_values = []
            for new_result_group in new_result_groups:
                query_values = {
                    "run_id": run_id,
                    "account_id": account_id,
                    **(new_result_group.dict()),
                }
                del query_values["results"]
                result_group_values.append(
                    tuple(
                        [
                            query_values[key]
                            for key in [
                                "run_id",
                                "account_id",
                                "entity",
                                "entity_id",
                                "entity_type",
                                "content",
                                "is_stale",
                                "filters",
                            ]
                        ]
                    )
                )

            await connection.raw_connection.executemany(
                self._create_query, result_group_values
            )

            result_groups: List[ResultGroupInDB] = await self.list_by_account_id(
                account_id=account_id, include_stale=False, run_id=run_id
            )

            print(result_groups)

            result_group_map: Dict[str, ResultGroupInDB] = {
                result_group.entity_id: result_group for result_group in result_groups
            }

            print(result_group_map)

            # Create Results
            result_inputs = [
                (
                    account_id,
                    run_id,
                    result_group_map[new_result_group.entity_id].id,
                    new_result_group.results,
                )
                for new_result_group in new_result_groups
            ]

            await result_repository.create_bulk(items=result_inputs)

            # Stitch Return Value
            result_groups: List[ResultGroupInDB] = await self.recalculate_scores(
                result_group_ids={result_group.id for result_group in result_groups}
            )

            return [
                (
                    result_group,
                    await result_repository.list_by_result_group(
                        result_group_id=result_group.id
                    ),
                )
                for result_group in result_groups
            ]

    async def get(
        self, *, account_id: int, run_id: int, result_group_id: int
    ) -> Optional[ResultGroupInDB]:
        """Get a ResultGroup using its ID and the ID of its parent objects."""

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            result_group = await connection.fetch_one(
                query=self._get_by_id_query,
                values={
                    "id": result_group_id,
                    "account_id": account_id,
                    "run_id": run_id,
                },
            )

            if result_group is None:
                return None

            return ResultGroupInDB(**result_group)

    async def count_by_account_id(
        self,
        *,
        account_id: int,
        include_stale: bool,
        run_id: int = None,
        filters: Optional[ResultGroupFilters] = None,
    ) -> int:
        """Count the number of ResultGroupss for a given account_id, run_id, and stale status."""
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            query = self._count_by_account_and_run_query(include_stale, filters)

            values = {"account_id": account_id, "run_id": run_id}
            if filters and filters.entity_id:
                values["entity_id"] = filters.entity_id

            result_groups = await connection.fetch_one(
                query=query,
                values=values,
            )

            return result_groups["count"]

    async def list_by_account_id(
        self,
        *,
        account_id: int,
        include_stale: bool,
        offset: Optional[int] = None,
        limit: Optional[int] = None,
        run_id: int = None,
        order_by: Optional[List[OrderBy]] = None,
        filters: Optional[ResultGroupFilters] = None,
    ) -> [ResultGroupInDB]:
        """
        Get all Runs within the repository with a particular account id and run_id.
        """

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            values = {"account_id": account_id, "run_id": run_id}

            if offset:
                values["offset"] = offset

            if limit:
                values["limit"] = limit

            if filters and filters.entity_id:
                values["entity_id"] = "%" + filters.entity_id + "%"

            query = self._list_by_account_and_run_query(
                include_stale, limit, offset, result_filters=filters
            )

            result_groups = await connection.fetch_all(
                query=query,
                values=values,
            )

        return [ResultGroupInDB(**result_group) for result_group in result_groups]

    async def invalidate_results(
        self,
        *,
        account_id: int,
        run_id: int = None,
    ) -> [ResultGroupInDB]:
        """
        Invalidate all the ResultGroups present for a given run. Also, invalidate all associated results.
        """
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            result_groups = await connection.fetch_all(
                query=self._invalidation_query,
                values={"account_id": account_id, "run_id": run_id},
            )

            # Invalidate the individual results, too!
            result_repo = ResultRepository(self.db)
            result_repo.invalidate_results(account_id=account_id, run_id=run_id)

            return [ResultGroupInDB(**result_group) for result_group in result_groups]

    async def update(
        self,
        *,
        account_id: int,
        run_id: int,
        result_group_id: int,
        result_group_update: ResultGroupUpdate,
    ) -> Optional[ResultGroupInDB]:
        """Update a ResultGroup using a ResultGroupUpdate object"""
        result_group = await self.get(
            run_id=run_id, account_id=account_id, result_group_id=result_group_id
        )

        if not result_group:
            return None

        result_group_update_params = result_group.copy(
            update=result_group_update.dict(exclude_unset=True),
        )

        try:

            async with self.db.connection() as connection:
                await connection.raw_connection.set_type_codec(
                    "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
                )

                updated_result_group = await connection.fetch_one(
                    query=self._update_query_by_id,
                    values={
                        "id": result_group_update_params.id,
                        "account_id": result_group_update_params.account_id,
                        "run_id": result_group_update_params.run_id,
                        **(
                            ResultGroupUpdate(
                                **(
                                    ResultGroupInDB(
                                        **result_group_update_params.dict()
                                    ).dict()
                                )
                            )
                        ).dict(),
                    },
                )
                return ResultGroupInDB(**updated_result_group)

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update params.",
            )
