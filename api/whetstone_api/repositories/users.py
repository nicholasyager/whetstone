from typing import Optional

from databases import Database
from fastapi import HTTPException
from pydantic import EmailStr
from pydantic.types import UUID, UUID4
from starlette.status import HTTP_400_BAD_REQUEST

from . import BaseRepository
from .. import config
from ..schemas.accounts import Account
from ..schemas.tokens import AccessToken
from ..schemas.users import UserInDB, UserCreate, UserInDBCreate, SensitiveUserInDB
from ..services import auth_service, AuthService


class UserRepository(BaseRepository):
    """Database actions relating to the USer resource"""

    _register_query = """
        INSERT INTO users (email, hashed_password)
        VALUES (:email, :hashed_password)
        RETURNING *;
    """

    _get_by_id_query = """
        select *
        from users
        where id = :id
        limit 1;
    """

    _get_by_email_query = """
        select *
        from users
        where email = :email
        limit 1;
    """

    _update_query_by_id = """
        update users
        set
            status = :status,
            is_verified = :is_verified
        where id = :id
        RETURNING *;
    """

    _get_user_accounts = """
        select
            accounts.*
        from user_accounts
        left join accounts on
            user_accounts.account_id = accounts.id
        where
            user_id = :user_id and
            is_active is true;
    """

    _add_user_to_account = """
        insert into user_accounts (account_id, user_id, is_active)
        values (:account_id, :user_id, true)
        on conflict on constraint unique_associations do update
        set is_active = true;
    """

    _remove_user_from_account = """
        update user_accounts
        set
            is_active = false
        where
            user_id =  :user_id and
            account_id = : account_id
    """

    def __init__(self, db: Database) -> None:
        super().__init__(db)
        self.auth_service: AuthService = auth_service

    async def register_new_user(self, *, new_user: UserCreate) -> UserInDB:
        """Register a new Whetstone User."""

        # Make sure email isn't already taken
        if await self.get_by_email(email=new_user.email):
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=(
                    "That email is already taken. Login with that email or "
                    "register with another one."
                ),
            )

        user_password_update = self.auth_service.create_hashed_password(
            plaintext_password=new_user.password
        )

        created_user = await self.db.fetch_one(
            query=self._register_query,
            values=UserInDBCreate(
                **{**new_user.dict(), **user_password_update.dict()}
            ).dict(),
        )

        access_token = AccessToken(
            access_token=auth_service.create_access_token_for_user(
                user=UserInDB(**created_user),
                secret_key=str(config.SECRET_KEY),
                audience=config.JWT_AUDIENCE,
                algorithms=[config.JWT_ALGORITHM],
            ),
            token_type="bearer",
        )

        return UserInDB(**created_user, access_token=access_token)

    async def get_user_accounts(self, *, user_id: UUID4) -> [Account]:
        """Return the accounts that a user is a member of."""
        accounts = await self.db.fetch_all(
            query=self._get_user_accounts, values={"user_id": user_id}
        )

        return [Account(**account) for account in accounts]

    async def get_by_email(self, *, email: str) -> Optional[UserInDB]:
        """Return a User based on the user's email address."""
        user = await self.db.fetch_one(
            query=self._get_by_email_query, values={"email": email}
        )

        if user is None:
            return None

        return UserInDB(
            **user, accounts=await self.get_user_accounts(user_id=user["id"])
        )

    async def get_by_email_sensitive(
        self, *, email: str
    ) -> Optional[SensitiveUserInDB]:
        """Return a User based on the user's email address."""
        user = await self.db.fetch_one(
            query=self._get_by_email_query, values={"email": email}
        )

        if user is None:
            return None

        return SensitiveUserInDB(
            **user, accounts=await self.get_user_accounts(user_id=user["id"])
        )

    async def authenticate_user(
        self, *, email: EmailStr, plaintext_password: str
    ) -> Optional[UserInDB]:
        """Verify that a user exists, and that their password is accurate."""
        # make user user exists in db
        user = await self.get_by_email_sensitive(email=email)

        if not user:
            return None

        # if submitted password doesn't match
        if not self.auth_service.verify_password(
            plaintext_password=plaintext_password, hashed_password=user.hashed_password
        ):
            return None

        return user

    async def add_user_to_account(
        self, *, user_id: UUID4, account_id: int
    ) -> Optional[UserInDB]:
        """Add a User to an Account by creating a UserAccount record."""
        await self.db.execute(
            query=self._add_user_to_account,
            values={"user_id": user_id, "account_id": account_id},
        )

        return await self.get(user_id=user_id)

    async def get(self, *, user_id: UUID) -> Optional[UserInDB]:
        """Get a User using its ID."""

        user = await self.db.fetch_one(
            query=self._get_by_id_query, values={"id": user_id}
        )

        if user is None:
            return None

        return UserInDB(
            **user, accounts=await self.get_user_accounts(user_id=user["id"])
        )
