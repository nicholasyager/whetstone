import json

import uuid
from typing import Optional, Dict, List

from fastapi import HTTPException
from pydantic import UUID4
from starlette.status import HTTP_400_BAD_REQUEST

from .. import BaseRepository
from ...dependencies.pagination import create_order_by_string, OrderBy
from ...schemas.demo.runs import DemoRunCreate, DemoRunInDB, DemoRunUpdate
from ...schemas.runs import RunStatus


class DemoRunRepository(BaseRepository):
    """Database actions relating to the DemoRun resource"""

    _create_query = """
        INSERT INTO demo_runs (id, correlation_id, status, manifest_location, run_results_location, metadata)
        VALUES (:id, :correlation_id, :status, :manifest_location, :run_results_location, :metadata)
        RETURNING *;
    """

    _get_by_id_query = """
        select *
        from demo_runs
        where  id = :id
        limit 1;
    """

    _count_by_correlation_id_query = """
        select count(id)
        from demo_runs
        where correlation_id = :correlation_id;
    """

    @staticmethod
    def _list_by_correlation_id_query(order_by: Optional[List[OrderBy]]) -> str:

        order_by_string = create_order_by_string(order_by)

        return f"""
        select *
        from demo_runs
        where correlation_id = :correlation_id
        {order_by_string if order_by_string is not None else ''}
        limit :limit
        offset :offset;
    """

    _update_query_by_id = """
        update demo_runs
        set
            status = :status,
            dequeued_at = :dequeued_at,
            started_at = :started_at,
            finished_at = :finished_at,
            graph_location = :graph_location
        where
            id = :id
        RETURNING *;
    """

    @staticmethod
    def generate_partial_update(fields: [str]) -> str:
        """Generate the query for a partial update"""
        template = """
        update demo_runs
        set {set_fields}
        where id = :id
        RETURNING *;
        """

        set_fields = ",".join([f"\n\t{field} = :{field}" for field in fields])
        return template.format(set_fields=set_fields)

    async def create(
        self,
        *,
        new_run: DemoRunCreate,
        manifest_location: str,
        run_results_location: Optional[str] = None,
        metadata: Optional[Dict] = None,
    ) -> DemoRunInDB:
        """
        Store a DemoRun within the repository.
        """

        # Create the Run Record
        query_values = {
            # Generate a UUID for the run. This has to be done in python, since
            # PostgreSQL requires some special shenanigans to get this done.
            "id": uuid.uuid4(),
            "correlation_id": new_run.correlation_id,
            "status": RunStatus.queued,
            "manifest_location": manifest_location,
            "run_results_location": run_results_location,
            "metadata": json.dumps(metadata),
        }

        run = await self.db.fetch_one(query=self._create_query, values=query_values)

        return DemoRunInDB(**run)

    async def retry(self, *, run_id: UUID4) -> Optional[DemoRunInDB]:
        """
        Retry an existing DemoRun. This will create a new DemoRun object using
        the same manifest and run results as an existing DemoRun.
        """

        existing_run = await self.get(run_id=run_id)
        if existing_run is None:
            return None

        # Create the Run Record
        # TODO: Deduplicate code by having this call self.create.
        query_values = {
            "id": uuid.uuid4(),
            "correlation_id": existing_run.correlation_id,
            "status": RunStatus.queued,
            "manifest_location": existing_run.manifest_location,
            "run_results_location": existing_run.run_results_location,
        }

        run = await self.db.fetch_one(query=self._create_query, values=query_values)

        return DemoRunInDB(**run)

    async def get(
        self,
        *,
        run_id: UUID4,
    ) -> Optional[DemoRunInDB]:
        """Get a Run using its ID."""

        run = await self.db.fetch_one(
            query=self._get_by_id_query,
            values={"id": run_id},
        )

        if run is None:
            return None

        return DemoRunInDB(**run)

    async def count_by_correlation_id(self, *, correlation_id: UUID4) -> int:
        query = self._count_by_correlation_id_query

        return await self.db.fetch_val(
            query=query,
            values={"correlation_id": correlation_id},
            column="count",
        )

    async def list_by_correlation_id(
        self,
        *,
        correlation_id: UUID4,
        offset: Optional[int] = None,
        limit: Optional[int] = None,
        order_by: Optional[List[OrderBy]] = None,
    ) -> [DemoRunInDB]:
        """
        Get all DemoRuns within the repository with a particular correlation_id.
        """
        values = {"correlation_id": correlation_id}

        if offset:
            values["offset"] = offset

        if limit:
            values["limit"] = limit

        query = self._list_by_correlation_id_query(order_by=order_by)

        runs = await self.db.fetch_all(query=query, values=values)

        return [DemoRunInDB(**run) for run in runs]

    async def update(
        self, *, run_id: UUID4, run_update: DemoRunUpdate
    ) -> Optional[DemoRunInDB]:
        """
        Update a DemoRun using an RunUpdate object. An update creates a transaction
        to ensure that any side effects do not occur if the query fails.
        """
        transaction = await self.db.transaction()
        try:
            run = await self.get(run_id=run_id)

            if not run:
                return None

            run_update_params = run.copy(
                update=run_update.dict(exclude_unset=True),
            )
            merged_run_update = DemoRunUpdate(**run_update_params.dict()).dict()

            updated_run = await self.db.fetch_one(
                query=self._update_query_by_id,
                values={
                    "id": run_update_params.id,
                    **merged_run_update,
                },
            )

        except Exception:
            await transaction.rollback()
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update params.",
            )
        else:
            await transaction.commit()
            return DemoRunInDB(**updated_run)

    async def partial_update(
        self, *, run_id: UUID4, run_update: DemoRunUpdate
    ) -> Optional[DemoRunInDB]:
        """Update a DemoRun using an DemoRunUpdate object"""
        run = await self.get(run_id=run_id)

        if not run:
            return None

        run_update_params = {
            "id": run_id,
            **run_update.dict(exclude_unset=True),
        }

        partial_update_query: str = self.generate_partial_update(run_update_params)

        try:
            updated_run = await self.db.fetch_one(
                query=partial_update_query,
                values=run_update_params,
            )
            return DemoRunInDB(**updated_run)

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update params.",
            )
