import json
import logging
import uuid
from typing import Optional, List, Tuple

from fastapi import HTTPException
from pydantic import UUID4
from starlette.status import HTTP_400_BAD_REQUEST

from .. import BaseRepository
from ...dependencies.filtering import ResultFilters
from ...schemas.demo.results import DemoResultCreate, DemoResultInDB, DemoResultUpdate

logger = logging.getLogger(__name__)


class DemoResultRepository(BaseRepository):
    """Database actions relating to the Result resource"""

    _check_run = """
        SELECT id
        from demo_runs
        where
            id = :id;
    """

    _create_query = """
        INSERT INTO demo_results (id, demo_run_id, correlation_id, demo_result_group_id, summary, severity, model, rule_set, content, is_stale, is_ignored)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
        RETURNING *;
    """

    _get_by_id_query = """
        select *
        from demo_results
        where
            id = :id
        limit 1;
    """

    @staticmethod
    def _list_by_demo_run_query(include_stale: bool = False) -> str:
        return f"""
        with

        demo_run_results as (
            select
                *,
                case
                    when severity = 'low' then 1
                    when severity = 'medium' then 10
                    when severity = 'high' then 100
                end as severity_score
            from demo_results
            where
                demo_run_id = :demo_run_id
            {'and is_stale is false' if not include_stale else ''}
            order by model, id asc
        ),

        sort_order as (
            select
                model,
                sum(severity_score) as total_severity
            from demo_run_results r
            group by model
            order by 2 desc
            limit :limit
            offset :offset
        )

        select demo_run_results.*
        from demo_run_results
        inner join sort_order on
            demo_run_results.model = sort_order.model
        order by
            sort_order.total_severity desc,
            sort_order.model,
            demo_run_results.severity_score desc
    """

    @staticmethod
    def _list_by_demo_result_group_query(
        include_stale: bool = False, filters: Optional[ResultFilters] = None
    ) -> str:

        values = {}
        if filters:
            values = filters.sql_values

        return f"""
          with

          demo_run_results as (
              select
                  *,
                  case
                      when severity = 'low' then 1
                      when severity = 'medium' then 10
                      when severity = 'high' then 100
                  end as severity_score
              from demo_results
              where
                  demo_result_group_id = :demo_result_group_id
              {'and is_stale is false' if not include_stale else ''}
              {'and rule_set in (' + values['rule_set'] +')' if filters and filters.rule_set else ''}
              {'and severity in (' + values['severity'] +')' if filters and filters.severity else ''}
              order by model, id asc
          ),

          sort_order as (
              select
                  model,
                  sum(severity_score) as total_severity
              from demo_run_results r
              group by model
              order by 2 desc
              limit :limit
              offset :offset
          )

          select demo_run_results.*
          from demo_run_results
          inner join sort_order on
              demo_run_results.model = sort_order.model
          order by
              sort_order.total_severity desc,
              sort_order.model,
              demo_run_results.severity_score desc
      """

    @staticmethod
    def _count_by_run_query(include_stale: bool = False) -> str:
        return f"""
           select count(*) as count
           from demo_results
           where
               demo_run_id = :demo_run_id
               {'and is_stale is false' if not include_stale else ''}
       """

    _invalidation_query = """
        update demo_results
        set
            is_stale = true
        where
            demo_run_id = :demo_run_id and
            is_stale = false
        RETURNING *;
    """

    _update_query_by_id = """
        update demo_results
        set
            summary = :summary,
            severity = :severity,
            rule_set = :rule_set,
            content = :content,
            is_stale = :is_stale,
            is_ignored = :is_ignored
        where
            id = :id
        RETURNING *;
    """

    async def create_bulk(
        self, *, items: List[Tuple[UUID4, UUID4, List[DemoResultCreate]]]
    ):

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            values = []
            for demo_run_id, demo_result_group_id, new_results in items:

                for new_result in new_results:
                    row_values = {
                        "id": uuid.uuid4(),
                        "demo_run_id": demo_run_id,
                        "demo_result_group_id": demo_result_group_id,
                        **new_result.dict(),
                    }
                    # row_values['content'] = json.dumps(row_values['content'])

                    fields_of_interest = [
                        "id",
                        "demo_run_id",
                        "correlation_id",
                        "demo_result_group_id",
                        "summary",
                        "severity",
                        "model",
                        "rule_set",
                        "content",
                        "is_stale",
                        "is_ignored",
                    ]

                    values.append(
                        tuple([row_values[key] for key in fields_of_interest])
                    )

            await connection.raw_connection.executemany(self._create_query, values)

            return

    async def create(
        self,
        *,
        demo_run_id: UUID4,
        demo_result_group_id: UUID4,
        new_results: [DemoResultCreate],
    ) -> List[DemoResultInDB]:
        """Store DemoResults within the repository."""

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            items = [(demo_run_id, demo_result_group_id, new_results)]

            await self.create_bulk(items=items)

            return await self.list_by_demo_result_group_id(
                demo_result_group_id=demo_result_group_id
            )

    async def get(self, *, demo_result_id: UUID4) -> Optional[DemoResultInDB]:
        """Get a DemoResult using its ID."""

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            result = await connection.fetch_one(
                query=self._get_by_id_query,
                values={"id": demo_result_id},
            )

            if result is None:
                return None

            return DemoResultInDB(**result)

    async def count_by_demo_run_id(
        self,
        *,
        demo_run_id: UUID4,
        include_stale: bool,
    ) -> int:
        """Count the number of DemoResults for a given demo_run_id, and stale status."""
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            results = await connection.fetch_one(
                query=self._count_by_run_query(include_stale),
                values={"demo_run_id": demo_run_id},
            )

            return results["count"]

    async def list_by_demo_result_group_id(
        self,
        *,
        include_stale: bool = False,
        demo_result_group_id: UUID4,
        filters: Optional[ResultFilters] = None,
    ) -> [DemoResultInDB]:
        """
        Get all DemoResults within the repository with a particular demo_run_id.
        """
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            results = await connection.fetch_all(
                query=self._list_by_demo_result_group_query(
                    include_stale, filters=filters
                ),
                values={"demo_result_group_id": demo_result_group_id},
            )

        return [DemoResultInDB(**result) for result in results]

    async def list_by_demo_run_id(
        self,
        *,
        include_stale: bool,
        demo_run_id: UUID4,
        offset: int,
        limit: int,
    ) -> [DemoResultInDB]:
        """
        Get all DemoResults within the repository with a particular demo_run_id.
        """
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            results = await connection.fetch_all(
                query=self._list_by_demo_run_query(include_stale),
                values={
                    "demo_run_id": demo_run_id,
                    "offset": offset,
                    "limit": limit,
                },
            )

        return [DemoResultInDB(**result) for result in results]

    async def invalidate_results(
        self,
        *,
        demo_run_id: UUID4,
    ) -> [DemoResultInDB]:
        """
        Invalidate all the DemoResults present for a given run.
        """
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            results = await connection.fetch_all(
                query=self._invalidation_query,
                values={"demo_run_id": demo_run_id},
            )

            return [DemoResultInDB(**result) for result in results]

    async def update(
        self,
        *,
        demo_result_id: UUID4,
        result_update: DemoResultUpdate,
    ) -> Optional[DemoResultInDB]:
        """Update a DemoResult using an ResultUpdate object"""
        result = await self.get(demo_result_id=demo_result_id)

        if not result:
            return None

        result_update_params = result.copy(
            update=result_update.dict(exclude_unset=True),
        )

        try:

            async with self.db.connection() as connection:
                await connection.raw_connection.set_type_codec(
                    "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
                )

                updated_run = await connection.fetch_one(
                    query=self._update_query_by_id,
                    values={
                        "id": result_update_params.id,
                        "demo_run_id": result_update_params.demo_run_id,
                        **(DemoResultUpdate(**result_update_params.dict()).dict()),
                    },
                )
                return DemoResultInDB(**updated_run)

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update params.",
            )
