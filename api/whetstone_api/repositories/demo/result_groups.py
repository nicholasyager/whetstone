import uuid

import json
import logging
from pydantic import UUID4
from typing import Optional, List, Set, Tuple, Dict

from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_422_UNPROCESSABLE_ENTITY

from .results import DemoResultRepository
from .. import BaseRepository
from ...dependencies.filtering import ResultGroupFilters
from ...dependencies.pagination import OrderBy
from ...schemas.demo.demo_result_groups import (
    DemoResultGroupInDB,
    DemoResultGroupCreate,
    DemoResultGroupUpdate,
)
from ...schemas.demo.results import DemoResultInDB

logger = logging.getLogger(__name__)


class DemoResultGroupsRepository(BaseRepository):
    """Database actions relating to the ResultGroup resource"""

    _check_run = """
           SELECT id
           from demo_runs
           where
               id = :id;
       """

    _create_query = """
        INSERT INTO demo_result_groups (id, demo_run_id, correlation_id, entity, entity_id, entity_type, content, is_stale, filters)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
        RETURNING *;
    """

    _get_by_id_query = """
        select *
        from demo_result_groups
        where
            id = :id
        limit 1;
    """

    @staticmethod
    def _build_filter_clauses(
        result_filters: Optional[ResultGroupFilters],
    ) -> Optional[str]:
        if not result_filters:
            return None

        filters = []
        values = result_filters.sql_values
        if result_filters.severity:
            # This is icky, but it works for now. Run tests to confirm if injection is possible.
            filters.append(
                f"expanded_result_groups.value ->> 'severity' in ({values['severity']})"
            )

        if result_filters.rule_set:
            filters.append(
                f"expanded_result_groups.value ->> 'rule_set' in ({values['rule_set']})"
            )

        if result_filters.entity_id:
            filters.append("demo_result_groups.entity_id like :entity_id")

        if len(filters) == 0:
            return None

        return " and ".join(filters)

    def _list_by_demo_run_query(
        self, include_stale: bool = False, result_filters: ResultGroupFilters = None
    ) -> str:
        filter_string = self._build_filter_clauses(result_filters)

        return f"""

        with

        expanded_result_groups as (
            select
                id, value
            from demo_result_groups, jsonb_array_elements(demo_result_groups.filters)
            where
                demo_run_id = :demo_run_id
                {'and is_stale is false' if not include_stale else ''}
        )

        select distinct
            demo_result_groups.*
        from demo_result_groups
        left join expanded_result_groups on
            demo_result_groups.id = expanded_result_groups.id
        where
            demo_run_id = :demo_run_id
            {'and is_stale is false' if not include_stale else ''}
            {'and ' + filter_string if filter_string else ''}
        order by severity_score desc, entity
        limit :limit
        offset :offset
    """

    def _count_by_account_and_run_query(
        self, include_stale: bool = False, result_filters: ResultGroupFilters = None
    ) -> str:
        filter_string = self._build_filter_clauses(result_filters)

        return f"""

        with

        expanded_result_groups as (
            select
                id, value
            from demo_result_groups, jsonb_array_elements(demo_result_groups.filters)
            where
                demo_run_id = :demo_run_id
                {'and is_stale is false' if not include_stale else ''}
        )

        select
            count(distinct demo_result_groups.id)
        from demo_result_groups
        left join expanded_result_groups on
            demo_result_groups.id = expanded_result_groups.id
        where
            demo_run_id = :demo_run_id
            {'and is_stale is false' if not include_stale else ''}
            {'and ' + filter_string if filter_string else ''}
    """

    _invalidation_query = """
        update demo_result_groups
        set
            is_stale = true
        where
            demo_run_id = :demo_run_id and
            is_stale = false
        RETURNING *;
    """

    _update_query_by_id = """
        update demo_result_groups
        set
            entity = :entity,
            entity_id = :entity_id,
            entity_type = :entity_type,
            content = :content,
            is_stale = :is_stale
        where
            id = :id and
            correlation_id = :correlation_id and
            demo_run_id = :demo_run_id
        RETURNING *;
    """

    _recalculate_scores_query = """
        with

        run_results as (
            select
                *,
                case
                    when severity = 'low' then 1
                    when severity = 'medium' then 10
                    when severity = 'high' then 100
                end as severity_score
            from demo_results
            where demo_result_group_id in ( :demo_result_group_ids )
        ),

        sort_order as (
            select
                demo_result_group_id,
                count(id) as result_count,
                sum(severity_score) as total_severity
            from run_results
            group by 1
        )


        update demo_result_groups
        set
            severity_score = sort_order.total_severity,
            result_count  = sort_order.result_count
        from sort_order
        where demo_result_groups.id = sort_order.demo_result_group_id
        returning demo_result_groups.*;
    """

    def preprocess_query(self, query: str, values: dict) -> Tuple[str, dict]:
        """Handle some early pre-processing for queries that need select in list."""

        for parameter_name in list(values.keys()):
            if (
                isinstance(values[parameter_name], (tuple, set, list))
                and f":{parameter_name}" in query
            ):
                replacement = ",".join(
                    [
                        str(x) if isinstance(x, (int, float)) else f"'{str(x)}'"
                        for x in values[parameter_name]
                    ]
                )
                query = query.replace(
                    f":{parameter_name}",
                    f"{replacement}",
                )
                # then remove the values from the value list
                del values[parameter_name]

        return query, values

    async def recalculate_scores(
        self, *, demo_result_group_ids: Set[UUID4]
    ) -> [DemoResultGroupInDB]:
        """Recalculate the result_count and severity_score for a set of ResultGroup. Return the updated ResultGroups."""
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            query, values = self.preprocess_query(
                self._recalculate_scores_query,
                {"demo_result_group_ids": demo_result_group_ids},
            )

            result_group_responses = await connection.fetch_all(
                query=query, values=values
            )

            return [
                DemoResultGroupInDB(**result_group)
                for result_group in result_group_responses
            ]

    async def create(
        self,
        *,
        demo_run_id: UUID4,
        new_result_groups: [DemoResultGroupCreate],
    ) -> List[Tuple[DemoResultGroupInDB, List[DemoResultInDB]]]:
        """Store ResultGroups within the repository."""

        # Verify that the run associated with the ResultGroup is part of the linked
        # account.
        run = await self.db.fetch_one(
            query=self._check_run,
            values={"id": demo_run_id},
        )

        if run is None:
            raise HTTPException(
                status_code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail="The supplied run_id is not part of the account that"
                " this ResultGroup is associated with.",
            )

        result_repository = DemoResultRepository(self.db)

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            # Create the ResultGroup
            result_group_values = []
            for new_result_group in new_result_groups:
                query_values = {
                    "id": uuid.uuid4(),
                    "demo_run_id": demo_run_id,
                    **(new_result_group.dict()),
                }
                del query_values["results"]
                result_group_values.append(
                    tuple(
                        [
                            query_values[key]
                            for key in [
                                "id",
                                "demo_run_id",
                                "correlation_id",
                                "entity",
                                "entity_id",
                                "entity_type",
                                "content",
                                "is_stale",
                                "filters",
                            ]
                        ]
                    )
                )

            await connection.raw_connection.executemany(
                self._create_query, result_group_values
            )

            result_groups: List[DemoResultGroupInDB] = await self.list_by_demo_run_id(
                include_stale=False, demo_run_id=demo_run_id
            )

            result_group_map: Dict[str, DemoResultGroupInDB] = {
                result_group.entity_id: result_group for result_group in result_groups
            }

            for new_result_group in new_result_groups:
                if new_result_group.entity_id not in result_group_map:
                    raise Exception(
                        f"ERROR! Unable to find {new_result_group.entity_id} in result_group_map. "
                        f"{', '.join(result_group_map.keys())}"
                    )

            # Create Results
            result_inputs = [
                (
                    demo_run_id,
                    result_group_map[new_result_group.entity_id].id,
                    new_result_group.results,
                )
                for new_result_group in new_result_groups
            ]

            await result_repository.create_bulk(items=result_inputs)

            result_groups: List[DemoResultGroupInDB] = await self.recalculate_scores(
                demo_result_group_ids={
                    result_group.id for result_group in result_groups
                }
            )

            return [
                (
                    result_group,
                    await result_repository.list_by_demo_result_group_id(
                        demo_result_group_id=result_group.id
                    ),
                )
                for result_group in result_groups
            ]

    async def get(
        self, *, demo_result_group_id: UUID4
    ) -> Optional[DemoResultGroupInDB]:
        """Get a ResultGroup using its ID and the ID of its parent objects."""

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            result_group = await connection.fetch_one(
                query=self._get_by_id_query,
                values={"id": demo_result_group_id},
            )

            if result_group is None:
                return None

            return DemoResultGroupInDB(**result_group)

    async def count_by_demo_run_id(
        self,
        *,
        demo_run_id: UUID4,
        include_stale: bool = False,
        filters: Optional[ResultGroupFilters] = None,
    ) -> int:
        """Count the number of ResultGroupss for a given account_id, run_id, and stale status."""
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            values = {"demo_run_id": demo_run_id}
            if filters and filters.entity_id:
                values["entity_id"] = filters.entity_id

            result_groups = await connection.fetch_one(
                query=self._count_by_account_and_run_query(include_stale, filters),
                values=values,
            )

            return result_groups["count"]

    async def list_by_demo_run_id(
        self,
        *,
        demo_run_id: UUID4,
        include_stale: bool,
        offset: Optional[int] = None,
        limit: Optional[int] = None,
        order_by: Optional[List[OrderBy]] = None,
        filters: Optional[ResultGroupFilters] = None,
    ) -> [DemoResultGroupInDB]:
        """
        Get all Runs within the repository with a particular account id and run_id.
        """

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            values = {
                "demo_run_id": demo_run_id,
            }

            if offset:
                values["offset"] = offset

            if limit:
                values["limit"] = limit

            if filters and filters.entity_id:
                values["entity_id"] = "%" + filters.entity_id + "%"

            result_groups = await connection.fetch_all(
                query=self._list_by_demo_run_query(
                    include_stale, result_filters=filters
                ),
                values=values,
            )

        return [DemoResultGroupInDB(**result_group) for result_group in result_groups]

    async def invalidate_results(self, *, demo_run_id: UUID4) -> [DemoResultGroupInDB]:
        """
        Invalidate all the ResultGroups present for a given run. Also, invalidate all associated results.
        """
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            result_groups = await connection.fetch_all(
                query=self._invalidation_query,
                values={"demo_run_id": demo_run_id},
            )

            # Invalidate the individual results, too!
            result_repo = DemoResultRepository(self.db)
            await result_repo.invalidate_results(demo_run_id=demo_run_id)

            return [
                DemoResultGroupInDB(**result_group) for result_group in result_groups
            ]

    async def update(
        self,
        *,
        demo_result_group_id: UUID4,
        result_group_update: DemoResultGroupUpdate,
    ) -> Optional[DemoResultGroupInDB]:
        """Update a ResultGroup using a ResultGroupUpdate object"""
        result_group = await self.get(demo_result_group_id=demo_result_group_id)

        if not result_group:
            return None

        result_group_update_params: DemoResultGroupInDB = result_group.copy(
            update=result_group_update.dict(exclude_unset=True),
        )

        try:
            async with self.db.connection() as connection:
                await connection.raw_connection.set_type_codec(
                    "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
                )

                updated_result_group = await connection.fetch_one(
                    query=self._update_query_by_id,
                    values={
                        "id": result_group_update_params.id,
                        "correlation_id": result_group_update_params.correlation_id,
                        "demo_run_id": result_group_update_params.demo_run_id,
                        **(
                            DemoResultGroupUpdate(
                                **(
                                    DemoResultGroupInDB(
                                        **result_group_update_params.dict()
                                    ).dict()
                                )
                            )
                        ).dict(),
                    },
                )
                return DemoResultGroupInDB(**updated_result_group)

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update params.",
            )
