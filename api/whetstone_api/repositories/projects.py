import json
from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST
from typing import Optional, Dict

from . import BaseRepository
from ..schemas.projects import ProjectCreate, ProjectInDB, ProjectUpdate


class ProjectRepository(BaseRepository):
    """Database actions relating to the Projects resource"""

    _create_query = """
        INSERT INTO projects (name, account_id, status)
        VALUES (:name, :account_id, :status)
        RETURNING id, name, account_id, status, created_at, updated_at;
    """

    _get_by_id_query = """
        select *
        from projects
        where
            id = :id and
            account_id = :account_id;
    """

    _list_by_account_query = """
        select *
        from projects
        where account_id = :account_id;
    """

    _update_query_by_id = """
        update projects
        set
            name = :name,
            status= :status
        where
            id = :id and
            account_id = :account_id
        RETURNING id, name, account_id, status, created_at, updated_at;
    """

    _get_project_statistics = """
    with

    runs as (
        select
            id,
            created_at
        from runs
        where
            project_id = :project_id
            and account_id = :account_id
        order by created_at desc
        limit 15
    ),

    statistic_history as (
        select
            run_statistics.statistic,
            array_agg(value order by runs.created_at  desc ) as results
        from runs
        left join run_statistics on
            runs.id::text = run_statistics.run_id
        where run_statistics.statistic is not null
        group by 1
        order by run_statistics.statistic
    )

    select
         json_object_agg(statistic, results) as statistics
    from statistic_history
    """

    async def create(
        self, *, account_id: int, new_project: ProjectCreate
    ) -> ProjectInDB:
        """Store a project within the repository."""

        query_values = new_project.dict()
        query_values["account_id"] = account_id

        project = await self.db.fetch_one(query=self._create_query, values=query_values)
        return ProjectInDB(**project)

    async def get(self, *, account_id: int, project_id: int) -> Optional[ProjectInDB]:
        """Get a project by the ID of the project."""

        project = await self.db.fetch_one(
            query=self._get_by_id_query,
            values={"id": project_id, "account_id": account_id},
        )

        if project is None:
            return None

        return ProjectInDB(**project)

    async def get_project_statistics(
        self, account_id: int, project_id: int
    ) -> Optional[Dict]:
        """Get a limited history of project run statistics."""

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            statistics = await connection.fetch_val(
                query=self._get_project_statistics,
                values={"project_id": project_id, "account_id": account_id},
            )

            if statistics is None:
                return None

            return json.loads(statistics)

    async def list_by_account_id(self, *, account_id: int) -> [ProjectInDB]:
        """
        Get all projects within the repository with a particular account id.
        """

        projects = await self.db.fetch_all(
            query=self._list_by_account_query, values={"account_id": account_id}
        )
        return [ProjectInDB(**project) for project in projects]

    async def update(
        self, *, account_id: int, project_id: int, project_update: ProjectUpdate
    ) -> Optional[ProjectInDB]:
        """Update a project using an ProjectUpdate object"""
        project = await self.get(project_id=project_id, account_id=account_id)

        if not project:
            return None

        project_update_params = project.copy(
            update=project_update.dict(exclude_unset=True),
        )

        try:
            updated_project = await self.db.fetch_one(
                query=self._update_query_by_id,
                values={
                    "id": project_update_params.id,
                    "name": project_update_params.name,
                    "status": project_update_params.status,
                    "account_id": project_update_params.account_id,
                },
            )
            return ProjectInDB(**updated_project)

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update params.",
            )
