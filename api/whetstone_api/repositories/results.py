import json
import logging
from typing import Optional, List, Tuple

from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_422_UNPROCESSABLE_ENTITY

from . import BaseRepository
from ..dependencies.filtering import ResultFilters
from ..dependencies.pagination import OrderBy
from ..schemas.results import ResultCreate, ResultInDB, ResultUpdate

logger = logging.getLogger(__name__)


class ResultRepository(BaseRepository):
    """Database actions relating to the Result resource"""

    _check_run = """
        SELECT id
        from runs
        where
            id = :id and
            account_id = :account_id;
    """

    create_query = """
        INSERT INTO results (run_id, account_id, result_group_id, summary, severity, model, rule_set, content, is_stale, is_ignored)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
        RETURNING *;
    """

    bulk_create_query = """
     INSERT INTO results (run_id, account_id, result_group_id, summary, severity, model, rule_set, content, is_stale, is_ignored)
        VALUES {values}
        RETURNING *;
    """

    bulk_create_values = """
    {run_id}, {account_id}, {result_group_id}, {summary}, {severity}, {model}, {rule_set}, {content}, {is_stale}, {is_ignored}
    """

    _get_by_id_query = """
        select *
        from results
        where
            id = :id and
            run_id = :run_id and
            account_id = :account_id
        limit 1;
    """

    @staticmethod
    def _list_by_result_group_query(
        include_stale: bool = False, filters: Optional[ResultFilters] = None
    ) -> str:

        values = {}
        if filters:
            values = filters.sql_values

        return f"""
        select *
        from results
        where
            result_group_id = :result_group_id
            {'and is_stale is false' if not include_stale else ''}
            {'and rule_set in (' + values['rule_set'] +')' if filters and filters.rule_set else ''}
            {'and severity in (' + values['severity'] +')' if filters and filters.severity else ''}
            ;
        """

    @staticmethod
    def _list_grouped_by_result_group_query(include_stale: bool = False) -> str:
        return f"""
        select
            result_group_id, array_agg(results.*)
        from results
        where
            result_group_id in :result_group_id
             {'and is_stale is false' if not include_stale else ''}
        group by result_group_id;
        """

    @staticmethod
    def _list_by_account_and_run_query(include_stale: bool = False) -> str:
        return f"""
        with

        run_results as (
            select
                *,
                case
                    when severity = 'low' then 1
                    when severity = 'medium' then 10
                    when severity = 'high' then 100
                end as severity_score
            from results
            where
                account_id = :account_id and
                run_id = :run_id
                {'and is_stale is false' if not include_stale else ''}
            order by model, id asc
        ),

        sort_order as (
            select
                model,
                sum(severity_score) as total_severity
            from run_results r
            group by model
            order by 2 desc
            limit :limit
            offset :offset
        )

        select run_results.*
        from run_results
        inner join sort_order on
            run_results.model = sort_order.model
        order by
            sort_order.total_severity desc,
            sort_order.model,
            run_results.severity_score desc;
    """

    @staticmethod
    def _count_by_account_and_run_query(include_stale: bool = False) -> str:
        return f"""
        select count(*) as count
        from results
        where
            account_id = :account_id and
            run_id = :run_id
            {'and is_stale is false' if not include_stale else ''}
    """

    _invalidation_query = """
        update results
        set
            is_stale = true
        where
            account_id = :account_id and
            run_id = :run_id and
            is_stale = false
        RETURNING *;
    """

    _update_query_by_id = """
        update results
        set
            summary = :summary,
            severity = :severity,
            rule_set = :rule_set,
            content = :content,
            is_stale = :is_stale,
            is_ignored = :is_ignored
        where
            id = :id and
            account_id = :account_id and
            run_id = :run_id
        RETURNING *;
    """

    async def create_bulk(
        self, *, items: List[Tuple[int, int, int, List[ResultCreate]]]
    ):

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            values = []
            for account_id, run_id, result_group_id, new_results in items:

                for new_result in new_results:
                    row_values = {
                        "run_id": run_id,
                        "account_id": account_id,
                        "result_group_id": result_group_id,
                        **new_result.dict(),
                    }
                    # row_values['content'] = json.dumps(row_values['content'])

                    fields_of_interest = [
                        "run_id",
                        "account_id",
                        "result_group_id",
                        "summary",
                        "severity",
                        "model",
                        "rule_set",
                        "content",
                        "is_stale",
                        "is_ignored",
                    ]

                    values.append(
                        tuple([row_values[key] for key in fields_of_interest])
                    )

            await connection.raw_connection.executemany(self.create_query, values)

            return

    async def create(
        self,
        *,
        account_id: int,
        run_id: int,
        result_group_id: int,
        new_results: [ResultCreate],
    ) -> List[ResultInDB]:
        """Store Results within the repository."""

        # Verify that the run associated with the result is part of the linked
        # account.
        run = await self.db.fetch_one(
            query=self._check_run,
            values={"id": run_id, "account_id": account_id},
        )

        if run is None:
            raise HTTPException(
                status_code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail="The supplied run_id is not part of the account that"
                " this Result is associated with.",
            )

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            # Create the Run Record
            values = []
            for new_result in new_results:
                row_values = {
                    "run_id": run_id,
                    "account_id": account_id,
                    "result_group_id": result_group_id,
                    **new_result.dict(),
                }
                # row_values['content'] = json.dumps(row_values['content'])

                fields_of_interest = [
                    "run_id",
                    "account_id",
                    "result_group_id",
                    "summary",
                    "severity",
                    "model",
                    "rule_set",
                    "content",
                    "is_stale",
                    "is_ignored",
                ]

                values.append(tuple([row_values[key] for key in fields_of_interest]))

            await connection.raw_connection.executemany(self.create_query, values)

            # TODO: Find a way to only return the salient results.
            return await self.list_by_result_group(result_group_id=result_group_id)

    async def get(
        self, *, account_id: int, run_id: int, result_id: int
    ) -> Optional[ResultInDB]:
        """Get a Result using its ID and the ID of its parent objects."""

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            result = await connection.fetch_one(
                query=self._get_by_id_query,
                values={"id": result_id, "account_id": account_id, "run_id": run_id},
            )

            if result is None:
                return None

            return ResultInDB(**result)

    async def count_by_account_id(
        self,
        *,
        account_id: int,
        include_stale: bool,
        run_id: int = None,
    ) -> int:
        """Count the number of Results for a given account_id, run_id, and stale status."""
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            results = await connection.fetch_one(
                query=self._count_by_account_and_run_query(include_stale),
                values={"account_id": account_id, "run_id": run_id},
            )

            return results["count"]

    async def list_by_account_id(
        self,
        *,
        account_id: int,
        include_stale: bool,
        offset: int,
        limit: int,
        run_id: int = None,
        order_by: Optional[List[OrderBy]] = None,
    ) -> [ResultInDB]:
        """
        Get all Runs within the repository with a particular account id and project_id.
        """
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            results = await connection.fetch_all(
                query=self._list_by_account_and_run_query(include_stale),
                values={
                    "account_id": account_id,
                    "run_id": run_id,
                    "offset": offset,
                    "limit": limit,
                },
            )

        return [ResultInDB(**result) for result in results]

    async def list_by_result_group(
        self,
        *,
        result_group_id: int,
        include_stale: bool = False,
        filters: Optional[ResultFilters] = None,
    ) -> [ResultInDB]:
        """
        Get all Runs within the repository with a particular account id and project_id.
        """
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            values = {
                "result_group_id": result_group_id,
            }

            results = await connection.fetch_all(
                query=self._list_by_result_group_query(include_stale, filters=filters),
                values=values,
            )

            result_list = [ResultInDB(**result) for result in results]

        return result_list

    async def invalidate_results(
        self,
        *,
        account_id: int,
        run_id: int = None,
    ) -> [ResultInDB]:
        """
        Invalidate all the Results present for a given run.
        """
        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            results = await connection.fetch_all(
                query=self._invalidation_query,
                values={"account_id": account_id, "run_id": run_id},
            )

            return [ResultInDB(**result) for result in results]

    async def update(
        self,
        *,
        account_id: int,
        run_id: int,
        result_id: int,
        result_update: ResultUpdate,
    ) -> Optional[ResultInDB]:
        """Update a Result using an ResultUpdate object"""
        result = await self.get(
            run_id=run_id, account_id=account_id, result_id=result_id
        )

        if not result:
            return None

        result_update_params = result.copy(
            update=result_update.dict(exclude_unset=True),
        )

        try:

            async with self.db.connection() as connection:
                await connection.raw_connection.set_type_codec(
                    "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
                )

                updated_run = await connection.fetch_one(
                    query=self._update_query_by_id,
                    values={
                        "id": result_update_params.id,
                        "account_id": result_update_params.account_id,
                        "run_id": result_update_params.run_id,
                        **(ResultUpdate(**result_update_params.dict()).dict()),
                    },
                )
                return ResultInDB(**updated_run)

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update params.",
            )
