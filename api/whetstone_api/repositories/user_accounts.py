from typing import List

from databases import Database
from pydantic.types import UUID4

from . import BaseRepository
from ..schemas.user_accounts import UserAccountInDB, UserAccountUpdate
from ..services import auth_service, AuthService


class UserAccountRepository(BaseRepository):
    """Database actions relating to UserAccount resource"""

    _get_account_membership = """
        select
            user_accounts.*
        from user_accounts
        left join accounts on
            user_accounts.account_id = accounts.id
        where
            account_id = :account_id and
            is_active is true;
    """

    _get_user_accounts = """
        select
            accounts.*
        from user_accounts
        left join accounts on
            user_accounts.account_id = accounts.id
        where
            user_id = :user_id and
            is_active is true;
    """

    _get_user_account_by_user_and_account = """
        select
            accounts.*
        from user_accounts
        left join accounts on
            user_accounts.account_id = accounts.id
        where
            user_id = :user_id and
            account_id = :account_id;
    """

    _get_user_account = """
        select
            accounts.*
        from user_accounts
        left join accounts on
            user_accounts.account_id = accounts.id
        where
            id = :id;
    """

    _add_user_to_account = """
        insert into user_accounts (account_id, user_id, is_active)
        values (:account_id, :user_id, true)
        on conflict on constraint unique_associations do update
        set is_active = true
        RETURNING *;
    """

    _update_user_for_account = """
        update user_accounts
        set
            is_active = :is_active
        where
            id =  :user_account_id
    """

    def __init__(self, db: Database) -> None:
        super().__init__(db)
        self.auth_service: AuthService = auth_service

    async def add_user_to_account(
        self, *, user_id: UUID4, account_id: int
    ) -> UserAccountInDB:
        """Add a User to an Account by creating a UserAccount record."""
        user_account = await self.db.fetch_one(
            query=self._add_user_to_account,
            values={"user_id": user_id, "account_id": account_id},
        )

        return UserAccountInDB(**user_account)

    async def get(self, *, user_account_id: int):
        """Get a specific UserAccount using its ID"""

        user_account = await self.db.fetch_one(
            query=self._get_user_accounts, values={"id": user_account_id}
        )

        return UserAccountInDB(**user_account)

    async def get_user_accounts_for_account(
        self, *, account_id: int
    ) -> List[UserAccountInDB]:
        """Get all User Accounts for an account using its ID."""

        user_accounts = await self.db.fetch_all(
            query=self._get_account_membership, values={"account_id": account_id}
        )

        return [UserAccountInDB(**user_account) for user_account in user_accounts]

    async def get_by_values(self, *, user_id: UUID4, account_id: int):
        """Get a specific UserAccount using its ID"""

        user_account = await self.db.fetch_one(
            query=self._get_user_account_by_user_and_account,
            values={"user_id": user_id, "account_id": account_id},
        )

        return UserAccountInDB(**user_account)

    async def update_user_account(
        self, *, user_account_id: int, update_user_account: UserAccountUpdate
    ) -> UserAccountInDB:
        """Update a UserAccount"""

        await self.db.execute(
            self._update_user_for_account,
            {
                "user_account_id": user_account_id,
                "is_active": update_user_account.is_active,
            },
        )

        return await self.get(user_account_id=user_account_id)
