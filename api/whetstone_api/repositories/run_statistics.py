import json
import logging
from pydantic import UUID4
from typing import List, Union

from . import BaseRepository
from ..schemas.run_statistics import RunStatisticCreate, RunStatisticInDB

logger = logging.getLogger(__name__)


class RunStatisticRepository(BaseRepository):
    """Database actions relating to the Run resource"""

    _create_query = """
        INSERT INTO run_statistics (run_id, label, statistic, value)
        VALUES (:run_id, :label, :statistic, :value)
        RETURNING *;
    """

    _get_by_run_id_query = """

    with

    correlation_ids as (
        select project_id::text as correlation_id
        from runs
        where id::text = :run_id

        union all

        select correlation_id::text
        from demo_runs
        where id::text = :run_id
    ),

    selected_runs as (
        select id::text as id
        from runs
        inner join correlation_ids on
            runs.project_id::text = correlation_ids.correlation_id

        union all

        select id::text as id
        from demo_runs
        inner join correlation_ids on
            demo_runs.correlation_id::text = correlation_ids.correlation_id
    )

    select
        distinct on (run_statistics.statistic)
        run_statistics.*,
        value
        - lag(value) over (
            partition by statistic
            order by run_statistics.created_at
        ) as delta
    from run_statistics
    inner join selected_runs on
        run_statistics.run_id  = selected_runs.id
    order by run_statistics.statistic, run_statistics.created_at desc;
    """

    async def create(
        self,
        *,
        new_run_statistics: List[RunStatisticCreate],
    ) -> List[RunStatisticInDB]:
        """
        Store a list of RunStatistics in the database
        """

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            # Create the Run Record
            run_statistics = []
            for new_result in new_run_statistics:
                values = new_result.dict()

                values["run_id"] = str(values["run_id"])

                run_statistic = await connection.fetch_one(
                    query=self._create_query, values=values
                )
                run_statistics.append(RunStatisticInDB(**run_statistic))

            return run_statistics

    async def get_statistics_by_run(self, *, run_id: Union[int, UUID4]):
        """Get a list of statistics for a given run_id."""

        async with self.db.connection() as connection:
            await connection.raw_connection.set_type_codec(
                "jsonb", encoder=json.dumps, decoder=json.loads, schema="pg_catalog"
            )

            run_statistics = await connection.fetch_all(
                query=self._get_by_run_id_query, values={"run_id": str(run_id)}
            )

        return [RunStatisticInDB(**run_statistic) for run_statistic in run_statistics]
