import json
from typing import Optional, List

from fastapi import HTTPException
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_422_UNPROCESSABLE_ENTITY
import logging

from . import BaseRepository
from ..dependencies.pagination import OrderBy, create_order_by_string
from ..schemas.runs import RunInDB, RunCreate, RunUpdate, RunStatus

logger = logging.getLogger(__name__)


class RunRepository(BaseRepository):
    """Database actions relating to the Run resource"""

    _check_project = """
        SELECT id
        from projects
        where
            id = :id and
            account_id = :account_id;
    """

    _create_query = """
        INSERT INTO runs (project_id, account_id, status, manifest_location, run_results_location, metadata)
        VALUES (:project_id, :account_id, :status, :manifest_location, :run_results_location, :metadata)
        RETURNING *;
    """

    _get_by_id_query = """
        select *
        from runs
        where
            id = :id and
            account_id = :account_id
        limit 1;
    """

    @staticmethod
    def _count_by_account_and_project_query(project_id: Optional[int]) -> str:
        return f"""
               select count(id)
               from runs
               where account_id = :account_id
               {"and project_id = :project_id" if project_id is not None else ''};
               """

    @staticmethod
    def _list_by_account_query(order_by: Optional[List[OrderBy]]) -> str:

        order_by_string = create_order_by_string(order_by)

        return f"""
        select *
        from runs
        where account_id = :account_id
        {order_by_string if order_by_string is not None else ''}
        limit :limit
        offset :offset;
        """

    @staticmethod
    def _list_by_account_and_project_query(order_by: Optional[List[OrderBy]]) -> str:
        order_by_string = create_order_by_string(order_by)
        return f"""
            select *
            from runs
            where
                account_id = :account_id and
                project_id = :project_id
            {order_by_string if order_by_string is not None else ''}
            limit :limit
            offset :offset;
        """

    _update_query_by_id = """
        update runs
        set
            status = :status,
            dequeued_at = :dequeued_at,
            started_at = :started_at,
            finished_at = :finished_at,
            graph_location = :graph_location
        where
            id = :id and
            account_id = :account_id
        RETURNING *;
    """

    @staticmethod
    def generate_partial_update(fields: [str]) -> str:
        """Generate the query for a partial update"""
        template = """
        update runs
        set {set_fields}
        where
            id = :id and
            account_id = :account_id
        RETURNING *;
        """

        set_fields = ",".join([f"\n\t{field} = :{field}" for field in fields])
        return template.format(set_fields=set_fields)

    async def create(
        self,
        *,
        account_id: int,
        new_run: RunCreate,
        manifest_location: str,
        run_results_location: Optional[str] = None,
        metadata: Optional[str] = None,
    ) -> RunInDB:
        """
        Store a Run within the repository. As a side effect, this method will
        also store the manifest and run result data in <UPDATE WITH LOCATION>
        and trigger Whetstone Engine task.
        """

        # Verify that the project associated with the run is part of the linked
        # account.
        project = await self.db.fetch_one(
            query=self._check_project,
            values={"id": new_run.project_id, "account_id": account_id},
        )

        if project is None:
            raise HTTPException(
                status_code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail="The supplied project_id is not part of the account that"
                " this Run is associated with.",
            )

        # Create the Run Record
        query_values = {
            "project_id": new_run.project_id,
            "account_id": account_id,
            "status": RunStatus.queued,
            "manifest_location": manifest_location,
            "run_results_location": run_results_location,
            "metadata": json.dumps(metadata),
        }

        run = await self.db.fetch_one(query=self._create_query, values=query_values)

        return RunInDB(**run)

    async def retry(self, *, account_id, run_id) -> Optional[RunInDB]:
        """
        Retry an existing Run. This will create a new Run object using the
        same manifest and run results as an existing Run.
        """

        existing_run = await self.get(account_id=account_id, run_id=run_id)
        if existing_run is None:
            return None

        # Create the Run Record
        query_values = {
            "project_id": existing_run.project_id,
            "account_id": account_id,
            "status": RunStatus.queued,
            "manifest_location": existing_run.manifest_location,
            "run_results_location": existing_run.run_results_location,
            "metadata": json.dumps(existing_run.metadata),
        }

        run = await self.db.fetch_one(query=self._create_query, values=query_values)

        return RunInDB(**run)

    async def get(
        self,
        *,
        account_id: int,
        run_id: int,
    ) -> Optional[RunInDB]:
        """Get a Run using its ID."""

        run = await self.db.fetch_one(
            query=self._get_by_id_query,
            values={"id": run_id, "account_id": account_id},
        )

        if run is None:
            return None

        return RunInDB(**run)

    async def count_by_account_id(
        self, *, account_id: int, project_id: Optional[int] = None
    ) -> int:
        query = self._count_by_account_and_project_query(project_id=project_id)

        values = {"account_id": account_id}

        if project_id:
            values["project_id"] = project_id

        return await self.db.fetch_val(
            query=query,
            values=values,
            column="count",
        )

    async def list_by_account_id(
        self,
        *,
        account_id: int,
        project_id: Optional[int] = None,
        offset: Optional[int] = None,
        limit: Optional[int] = None,
        order_by: Optional[List[OrderBy]] = None,
    ) -> [RunInDB]:
        """
        Get all Runs within the repository with a particular account id and project_id.
        """
        values = {"account_id": account_id}

        if project_id is None:
            query = self._list_by_account_query(order_by)
        else:
            query = self._list_by_account_and_project_query(order_by)
            values["project_id"] = project_id

        if offset:
            values["offset"] = offset

        if limit:
            values["limit"] = limit

        runs = await self.db.fetch_all(query=query, values=values)

        return [RunInDB(**run) for run in runs]

    async def update(
        self, *, account_id: int, run_id: int, run_update: RunUpdate
    ) -> Optional[RunInDB]:
        """
        Update a Run using an RunUpdate object. An update creates a transaction
        to ensure that any side effects do not occur if the query fails.
        """
        transaction = await self.db.transaction()
        try:
            run = await self.get(run_id=run_id, account_id=account_id)

            if not run:
                return None

            run_update_params = run.copy(
                update=run_update.dict(exclude_unset=True),
            )
            merged_run_update = RunUpdate(**run_update_params.dict()).dict()

            updated_run = await self.db.fetch_one(
                query=self._update_query_by_id,
                values={
                    "id": run_update_params.id,
                    "account_id": run_update_params.account_id,
                    **merged_run_update,
                },
            )

        except Exception:
            await transaction.rollback()
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update params.",
            )
        else:
            await transaction.commit()
            return RunInDB(**updated_run)

    async def partial_update(
        self, *, account_id: int, run_id: int, run_update: RunUpdate
    ) -> Optional[RunInDB]:
        """Update a Run using an RunUpdate object"""
        run = await self.get(run_id=run_id, account_id=account_id)

        if not run:
            return None

        run_update_params = {
            "id": run_id,
            "account_id": account_id,
            **run_update.dict(exclude_unset=True),
        }

        partial_update_query: str = self.generate_partial_update(run_update_params)

        try:
            updated_run = await self.db.fetch_one(
                query=partial_update_query,
                values=run_update_params,
            )
            return RunInDB(**updated_run)

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail="Invalid update params.",
            )
