from base64 import b64encode, b64decode

from Crypto.Cipher import AES

from whetstone_api.schemas import CoreModel


class EncryptedValue(CoreModel):
    """Internal representation of a credential."""

    nonce: str
    header: str
    ciphertext: str
    tag: str

    def decode(self):
        json_k = ["nonce", "header", "ciphertext", "tag"]
        return {k: b64decode(self.dict()[k]) for k in json_k}


class CryptographyService:
    """Service to manage encryption and decryption of values."""

    def __init__(self, key: bytes):
        self._key = key

    def encrypt(self, plaintext: str, header: str):
        """Encrypt a string using AEX-EAX"""

        cipher = AES.new(self._key, AES.MODE_EAX)
        cipher.update(header.encode())
        ciphertext, tag = cipher.encrypt_and_digest(plaintext.encode())

        json_k = ["nonce", "header", "ciphertext", "tag"]
        json_v = [
            b64encode(x).decode("utf-8")
            for x in (cipher.nonce, header.encode(), ciphertext, tag)
        ]
        return EncryptedValue(**(dict(zip(json_k, json_v))))

    def decrypt(self, encrypted_value: EncryptedValue) -> str:
        """Decrypt an encrypted value and return the plaintext."""

        decoded_values = encrypted_value.decode()
        cipher = AES.new(self._key, AES.MODE_EAX, nonce=decoded_values["nonce"])

        cipher.update(decoded_values["header"])

        plaintext = cipher.decrypt_and_verify(
            decoded_values["ciphertext"], decoded_values["tag"]
        )
        return plaintext.decode("utf-8")
