from .authentication import AuthService
from .cryptography import CryptographyService
from .. import config

auth_service = AuthService()
crypto_service = CryptographyService(key=str(config.SECRET_KEY).encode()[:16])
