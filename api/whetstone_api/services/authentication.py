import datetime
from typing import Optional, List

import jwt
from fastapi import HTTPException
from passlib.context import CryptContext
from pydantic import ValidationError
from starlette import status

from whetstone_api import config
from whetstone_api.schemas.tokens import JWTMeta, JWTCreds, JWTPayload
from whetstone_api.schemas.users import UserPasswordUpdate, UserInDB


class AuthException(BaseException):
    """
    Custom auth exception that can be modified later on.
    """

    pass


class AuthService:
    """Service to manage Authentication tasks."""

    def __init__(self):
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

    def create_hashed_password(self, *, plaintext_password: str) -> UserPasswordUpdate:
        hashed_password = self.hash_password(plaintext_password=plaintext_password)
        return UserPasswordUpdate(hashed_password=hashed_password)

    def verify_password(self, *, plaintext_password: str, hashed_password: str) -> bool:
        return self.pwd_context.verify(plaintext_password, hashed_password)

    def hash_password(self, *, plaintext_password: str) -> str:
        return self.pwd_context.hash(plaintext_password)

    @staticmethod
    def verify_api_key(*, api_key: str):
        """Verify that an API Key matches the expected API Key"""
        return api_key == str(config.API_KEY)

    @staticmethod
    def get_email_from_token(*, secret_key: str, token: str) -> Optional[str]:
        """Extract the email address from the provided JSON Web Token."""

        try:
            decoded_token = jwt.decode(
                token,
                secret_key,
                audience=config.JWT_AUDIENCE,
                algorithms=[config.JWT_ALGORITHM],
            )
            payload = JWTPayload(**decoded_token)

        except (jwt.PyJWTError, ValidationError):
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Could not validate token credentials.",
                headers={"WWW-Authenticate": "Bearer"},
            )

        return payload.email

    @staticmethod
    def create_access_token_for_user(
        *,
        user: UserInDB,
        secret_key: str,
        audience: str,
        algorithms: List[str],
        expires_in: int = config.ACCESS_TOKEN_EXPIRE_MINUTES,
    ) -> Optional[str]:

        if not user or not isinstance(user, UserInDB):
            return None

        jwt_meta = JWTMeta(
            aud=audience,
            iat=datetime.datetime.timestamp(datetime.datetime.utcnow()),
            exp=datetime.datetime.timestamp(
                datetime.datetime.utcnow() + datetime.timedelta(minutes=expires_in)
            ),
        )
        jwt_creds = JWTCreds(sub=str(user.id), email=user.email)

        token_payload = JWTPayload(
            **jwt_meta.dict(),
            **jwt_creds.dict(),
        )

        access_token = jwt.encode(
            token_payload.dict(), secret_key, algorithm=algorithms[0]
        )

        return access_token
