from databases import DatabaseURL
from starlette.config import Config
from starlette.datastructures import Secret

from .__version__ import __version__

config = Config(".env")

PROJECT_NAME = "Whetstone"
VERSION = __version__

API_PREFIX = "/api"
DOMAIN = "localhost"

SECRET_KEY = config("SECRET_KEY", cast=Secret)
API_KEY = config("API_KEY", cast=Secret)

ACCESS_TOKEN_EXPIRE_MINUTES = config(
    "ACCESS_TOKEN_EXPIRE_MINUTES", cast=int, default=7 * 24 * 60  # one week
)
JWT_ALGORITHM = config("JWT_ALGORITHM", cast=str, default="HS256")
JWT_AUDIENCE = config("JWT_AUDIENCE", cast=str, default="whetstone:auth")
JWT_TOKEN_PREFIX = config("JWT_TOKEN_PREFIX", cast=str, default="Bearer")


POSTGRES_USER = config("POSTGRES_USER", cast=str)
POSTGRES_PASSWORD = config("POSTGRES_PASSWORD", cast=Secret)
POSTGRES_SERVER = config("POSTGRES_SERVER", cast=str, default="db")
POSTGRES_PORT = config("POSTGRES_PORT", cast=str, default="5432")
POSTGRES_DB = config("POSTGRES_DB", cast=str)
DATABASE_URL = config(
    "DATABASE_URL",
    cast=DatabaseURL,
    default=f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@"
    f"{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB}",
)

STORAGE_URL = config("STORAGE_URL", cast=str)
STORAGE_ACCESS_KEY = config("STORAGE_ACCESS_KEY", cast=str)
STORAGE_SECRET_KEY = config("STORAGE_SECRET_KEY", cast=Secret)
STORAGE_BUCKET = config("STORAGE_BUCKET", cast=str)
STORAGE_USE_SSL = config("STORAGE_USE_SSL", cast=bool, default=False)

REDIS_HOST = config("REDIS_HOST", cast=str)
REDIS_PORT = config("REDIS_PORT", cast=str, default="6379")
