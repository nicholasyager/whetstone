from fastapi import FastAPI, Security
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.gzip import GZipMiddleware

from . import config, tasks
from .dependencies.auth import get_superuser
from .routers import (
    accounts,
    projects,
    runs,
    results,
    users,
    auth,
    user_accounts,
    run_statistics,
    webhooks,
)
from .routers.demo import runs as demo_runs
from .routers.demo import results as demo_results
from .internal import accounts as internal_accounts

tags_metadata = [
    {
        "name": "Authentication",
        "description": (
            "The Authentication endpoints are important for making sure that "
            "your API can access the Whetstone API."
        ),
    },
    {
        "name": "Accounts",
        "description": "Accounts are the top-level organizational structure in"
        "Whetstone. Users are members of an Account, and "
        "Accounts can have multiple Projects.",
    },
    {
        "name": "Projects",
        "description": "Projects represent a particular DAG that is analyzed."
        "Projects have Runs, which represent analyses that have"
        "been performed on the Project.",
    },
    {
        "name": "Runs",
        "description": "Runs are the specific executions of the Whetstone engine"
        " on a set of dbt artifacts. A run is associated with a "
        "particular Project, and can have multiple Results.",
    },
    {
        "name": "Results",
        "description": "Results are the output of a run of the Whetstone Engine."
        " A result can communicate potential issues with a DAG,"
        " including the model affected, a summary of the issue,"
        " the name of the Rule Set that generated the issue, and"
        " a long-form description of the potential issue.",
    },
    {"name": "Users", "description": "User registration and update endpoints."},
    {
        "name": "Statistics",
        "description": (
            "The Statistics endpoints are used to store statistics information about different resources."
        ),
    },
]


def get_application():
    app = FastAPI(
        title=config.PROJECT_NAME,
        version=config.VERSION,
        openapi_tags=tags_metadata,
    )
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    app.add_middleware(GZipMiddleware, minimum_size=1000)
    app.add_event_handler("startup", tasks.create_start_app_handler(app))
    app.add_event_handler("shutdown", tasks.create_stop_app_handler(app))

    routers = [
        accounts.router,
        projects.router,
        runs.router,
        results.router,
        users.router,
        user_accounts.router,
        auth.router,
        demo_runs.router,
        demo_results.router,
        run_statistics.router,
        webhooks.router,
    ]
    for router in routers:
        app.include_router(router)

    internal_routes = [
        internal_accounts.router,
    ]
    for router in internal_routes:
        app.include_router(
            router=router,
            prefix="/admin",
            tags=["Administration"],
            dependencies=[Security(get_superuser)],
        )

    return app


app = get_application()
