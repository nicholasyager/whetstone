import blosc
import io
import datetime

import pickle

from dbt.contracts.graph.manifest import Manifest
from dbt.graph import Graph
from fastapi import Depends
from hotdag import HotDAG
from hotdag.manifest import DictManifestLoader
from hotdag.renderer import Renderer
from minio import S3Error
from typing import Set, Callable, Tuple

from minio.commonconfig import GOVERNANCE
from minio.retention import Retention
from urllib3 import HTTPResponse

from whetstone_api.config import STORAGE_BUCKET
from whetstone_api.dependencies.storage import Storage, get_storage


class CacheExpirationException(BaseException):
    """Exception raised when a cached file has expired."""


class CachedHotDAG:
    """A HotDAG instance with a caching layer for quicker repeated loading of Manifest and dbt Graph objects."""

    def __init__(self, storage: Storage):
        self.storage: Storage = storage

    def _get_from_cache(self, key: str) -> Tuple[Manifest, Graph]:
        """Get a parsed Manifest and dbt Graph from the cache."""

        manifest_response: HTTPResponse = self.storage.client.get_object(
            bucket_name=STORAGE_BUCKET,
            object_name=key + ".manifest.pickle",
        )
        manifest: Manifest = pickle.loads(blosc.decompress(manifest_response.data))

        graph_response: HTTPResponse = self.storage.client.get_object(
            bucket_name=STORAGE_BUCKET,
            object_name=key + ".graph.pickle",
        )
        graph: Graph = pickle.loads(blosc.decompress(graph_response.data))

        return manifest, graph

    def _put_into_cache(self, key: str, manifest: Manifest, graph: Graph):
        """Put a parsed Manifest and dbt Graph into the cache as a pickle"""

        manifest_data = io.BytesIO(blosc.compress(pickle.dumps(manifest)))
        graph_data = io.BytesIO(blosc.compress(pickle.dumps(graph)))
        retention_config = Retention(
            GOVERNANCE, datetime.datetime.utcnow() + datetime.timedelta(days=7)
        )

        self.storage.client.put_object(
            bucket_name=STORAGE_BUCKET,
            object_name=key + ".manifest.pickle",
            data=manifest_data,
            length=manifest_data.getbuffer().nbytes,
            retention=retention_config,
        )
        self.storage.client.put_object(
            bucket_name=STORAGE_BUCKET,
            object_name=key + ".graph.pickle",
            data=graph_data,
            length=graph_data.getbuffer().nbytes,
            retention=retention_config,
        )

    def get_selection(self, key: str, select: str, exclude: str) -> Set[str]:
        """Get a selection for a specific manifest."""

        hotdag = HotDAG(
            manifest_loader=DictManifestLoader(), renderer=Renderer(content_type="")
        )

        # Check the cache for this key
        try:
            self.storage.client.stat_object(
                bucket_name=STORAGE_BUCKET,
                object_name=key,
            )

            manifest, graph = self._get_from_cache(key)
            hotdag.manifest = manifest
            hotdag.graph = graph

        except S3Error:
            artifact = self.storage.get_artifact(key=key)
            hotdag.load_manifest(dict=artifact)

            self._put_into_cache(key=key, manifest=hotdag.manifest, graph=hotdag.graph)

        return hotdag.get_selection(select, exclude)


def get_hotdag() -> Callable:
    def get_client(storage: Storage = Depends(get_storage())) -> CachedHotDAG:
        return CachedHotDAG(storage)

    return get_client
