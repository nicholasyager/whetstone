import datetime
import hashlib
import io
import json
import os
from typing import Callable, Union

from fastapi import FastAPI, Depends
from minio import Minio
from starlette.requests import Request
from urllib3 import HTTPResponse

from whetstone_api.config import (
    STORAGE_URL,
    STORAGE_ACCESS_KEY,
    STORAGE_SECRET_KEY,
    STORAGE_BUCKET,
    STORAGE_USE_SSL,
)


class DummyMinio:
    """Dummy class for mocking the Minio client."""

    def __init__(self):
        self.storage = {}

    def put_object(self, *args, **kwargs) -> None:
        self.storage[kwargs["object_name"]] = kwargs["data"]

    def get_presigned_url(self, *args, **kwargs) -> str:
        return "http://example.com/fake"


async def create_client(app: FastAPI) -> None:
    """Create a Storage Client and store it in the app's state."""

    if os.environ.get("TESTING"):
        client = DummyMinio()

    else:

        client = Minio(
            STORAGE_URL,
            access_key=STORAGE_ACCESS_KEY,
            secret_key=str(STORAGE_SECRET_KEY),
            secure=STORAGE_USE_SSL,
        )

        found = client.bucket_exists(STORAGE_BUCKET)
        if not found:
            client.make_bucket(STORAGE_BUCKET)

    app.state.storage = client


class Storage:
    """Class for storing data in a long-term storage location"""

    def __init__(self, client: Union[Minio, DummyMinio]):
        self.client = client

    def put_artifact(self, artifact: dict) -> str:
        """Put an artifact in the STORAGE_BUCKET, and return the key for the object."""
        key = hashlib.sha1(json.dumps(artifact, sort_keys=True).encode()).hexdigest()
        data = io.BytesIO(json.dumps(artifact).encode())
        self.client.put_object(
            bucket_name=STORAGE_BUCKET,
            object_name=key,
            data=data,
            length=data.getbuffer().nbytes,
        )
        return key

    def get_artifact(self, key: str) -> dict:
        """Get an artifact from the STORAGE_BUCKET and return the artifact as a Dict."""
        response: HTTPResponse = self.client.get_object(
            bucket_name=STORAGE_BUCKET, object_name=key
        )
        return json.loads(response.data.decode())

    def get_artifact_url(self, key: str) -> dict:
        """Get an artifact object and return it as s JSON blob."""

        return self.client.get_presigned_url(
            method="GET",
            bucket_name=STORAGE_BUCKET,
            object_name=key,
            expires=datetime.timedelta(hours=2),
            response_headers={"content-type": "application/json"},
        )


def get_storage_client(request: Request) -> Union[Minio, DummyMinio]:
    """Get a Storage client"""
    return request.app.state.storage


def get_storage() -> Callable:
    def get_client(
        client: Union[Minio, DummyMinio] = Depends(get_storage_client)
    ) -> Storage:
        return Storage(client)

    return get_client
