from typing import Optional, Union

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, APIKeyHeader

from whetstone_api import config
from whetstone_api.dependencies.database import get_repository
from whetstone_api.repositories.users import UserRepository
from whetstone_api.schemas.tokens import APIKeyAccess
from whetstone_api.schemas.users import UserInDB, UserStatus
from whetstone_api.services import auth_service

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/token", auto_error=False)
api_header_auth = APIKeyHeader(name="X-API-KEY", auto_error=False)


async def get_api_key(api_key_header: str = Depends(api_header_auth)):
    """Validate an API Key for authentication"""

    if not api_key_header:
        return None

    if auth_service.verify_api_key(api_key=api_key_header):
        return APIKeyAccess(is_superuser=True)

    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Authentication failed",
        headers={"WWW-Authenticate": "X-API-KEY"},
    )


async def get_superuser(api_key: Optional[APIKeyAccess] = Depends(get_api_key)):
    """Require that an API Key be present."""
    if api_key is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Authentication failed",
            headers={"WWW-Authenticate": "X-API-KEY"},
        )

    if api_key.is_superuser is False:
        HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=(
                "Action forbidden. Your API Key credentials do not have the "
                "permissions required to access this resource."
            ),
        )


async def get_user_from_token(
    *,
    token: str = Depends(oauth2_scheme),
    user_repo: UserRepository = Depends(get_repository(UserRepository)),
) -> Optional[UserInDB]:
    """Get the current User based on the provided token."""

    if not token:
        return None

    try:
        email = auth_service.get_email_from_token(
            token=token, secret_key=str(config.SECRET_KEY)
        )
        user = await user_repo.get_by_email(email=email)

    except Exception as e:
        raise e

    return user


def get_current_active_user(
    current_user: Optional[UserInDB] = Depends(get_user_from_token),
    api_key: Optional[APIKeyAccess] = Depends(get_api_key),
) -> Optional[Union[UserInDB, APIKeyAccess]]:
    """
    Return the current User based on the provided JWT, and process all
    validation code required to prevent auth if required.
    """

    if api_key:
        return api_key

    if not current_user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="No authenticated user.",
            headers={"WWW-Authenticate": "Bearer"},
        )

    if current_user.status != UserStatus.active:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not an active user.",
            headers={"WWW-Authenticate": "Bearer"},
        )

    return current_user


def get_current_user_account_membership(
    account_id: int,
    current_user: Union[UserInDB, APIKeyAccess] = Depends(get_current_active_user),
) -> bool:
    """Determine if the current user has access to the account resource requested."""

    if isinstance(current_user, APIKeyAccess):
        return current_user.is_superuser

    user_accounts = [account.id for account in current_user.accounts]
    if account_id not in user_accounts:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=(
                "Action forbidden. Your user does not have access to the "
                "requested resource."
            ),
        )

    return True
