from enum import Enum
from pydantic import BaseModel

from typing import Optional, List


async def pagination(offset: int = 0, limit: int = 20, order_by: Optional[str] = None):
    """Query string parameters allowed for pagination"""

    if order_by:
        order_by_clauses = parse_order_by(order_by)
    else:
        order_by_clauses = set()

    return {"offset": offset, "limit": limit, "order_by": order_by_clauses}


class SortDirection(Enum):
    ASC = "asc"
    DESC = "desc"


class OrderBy(BaseModel):
    column: str
    direction: SortDirection


def parse_order_by(order_by: str) -> List[OrderBy]:
    """Parse an order_by field into an OrderBy set"""

    output = []

    for clause in order_by.split(","):
        if clause[0] == "-":
            output.append(OrderBy(column=clause[1:], direction=SortDirection.DESC))
        elif clause[0] == "+":
            output.append(OrderBy(column=clause[1:], direction=SortDirection.ASC))
        else:
            output.append(OrderBy(column=clause, direction=SortDirection.ASC))

    return output


def create_order_by_string(order_by: List[OrderBy]) -> Optional[str]:
    if not order_by or len(order_by) == 0:
        order_by_string = None

    else:
        clauses = [
            clause.column + " " + str(clause.direction.value) for clause in order_by
        ]
        order_by_string = f'order by {", ".join(clauses)}'
    return order_by_string
