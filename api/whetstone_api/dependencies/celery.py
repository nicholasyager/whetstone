import os
from typing import Any, Union, Callable

from celery import Celery
from fastapi import FastAPI, Depends
from starlette.requests import Request

from whetstone_api.config import REDIS_HOST, REDIS_PORT


class DummyCelery:
    """Dummy class of celery for unit testing"""

    def send_task(self, *args, **kwargs) -> Any:
        pass


async def create_celery_app(app: FastAPI) -> None:
    """Create a Celery object and store it in the app's state"""
    if os.environ.get("TESTING"):
        celery = DummyCelery()

    else:
        celery = Celery("engine", broker=f"redis://{REDIS_HOST}:{REDIS_PORT}/0")

    app.state.celery = celery


def get_celery_object(request: Request) -> Union[Celery, DummyCelery]:
    """Get a celery object"""
    return request.app.state.celery


def get_celery() -> Callable:
    def get_client(
        client: Union[Celery, DummyCelery] = Depends(get_celery_object)
    ) -> Union[Celery, DummyCelery]:
        return client

    return get_client
