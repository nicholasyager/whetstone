import logging
import os
from typing import Callable, Type

from databases import Database
from fastapi import Depends
from fastapi import FastAPI
from starlette.requests import Request

from whetstone_api.config import DATABASE_URL
from whetstone_api.repositories import BaseRepository

logger = logging.getLogger(__name__)


async def connect_to_db(app: FastAPI) -> None:
    resolved_url = f"{DATABASE_URL}_test" if os.environ.get("TESTING") else DATABASE_URL
    database = Database(resolved_url, min_size=2, max_size=10)
    try:
        await database.connect()
        logger.info(database.url)
        app.state.db = database
    except Exception as e:
        logger.warning("--- DB CONNECTION ERROR ---")
        logger.warning(e)
        logger.warning("--- DB CONNECTION ERROR ---")


async def close_db_connection(app: FastAPI) -> None:
    try:
        await app.state.db.disconnect()

    except Exception as e:
        logger.warning("--- DB DISCONNECT ERROR ---")
        logger.warning(e)
        logger.warning("--- DB DISCONNECT ERROR ---")


def get_database(request: Request) -> Database:
    return request.app.state.db


def get_repository(repo_type: Type[BaseRepository]) -> Callable:
    def get_repo(db: Database = Depends(get_database)) -> Type[BaseRepository]:
        return repo_type(db)

    return get_repo
