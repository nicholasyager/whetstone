from fastapi import Query
from typing import List, Union, Dict, Optional, Set

from whetstone_api.schemas.results import ResultSeverity


class ResultGroupFilters:
    """Filter parameters used for narrowing down ResultGroups returned from GET /results"""

    def __init__(
        self,
        severity: Union[List[ResultSeverity], None] = Query(default=None),
        rule_set: Union[List[str], None] = Query(default=None),
        entity_id: Union[str, None] = Query(default=None),
        select: Union[str, None] = Query(default=None),
        exclude: Union[str, None] = Query(default=None),
    ):
        self.severity = severity
        self.rule_set = rule_set
        self.entity_id = entity_id
        self.select = select
        self.exclude = exclude
        self.unique_ids: Optional[Set[str]] = None

    @property
    def sql_values(self) -> Dict:
        """Get SQL-compatible values for SQL templating."""

        values = {}
        if self.rule_set:
            values["rule_set"] = "'" + "', '".join(self.rule_set) + "'"

        if self.severity:
            values["severity"] = "'" + "', '".join(self.severity) + "'"

        if self.entity_id:
            values["model"] = self.entity_id

        if self.unique_ids:
            values["unique_ids"] = "'" + "', '".join(self.unique_ids) + "'"

        return values


class ResultFilters:
    """Filter parameters used for narrowing down Results returned from GET /results"""

    def __init__(
        self,
        severity: Union[List[ResultSeverity], None] = Query(default=None),
        rule_set: Union[List[str], None] = Query(default=None),
        model: Union[str, None] = Query(default=None),
    ):
        self.severity = severity
        self.rule_set = rule_set
        self.model = model

    @property
    def sql_values(self) -> Dict:
        """Get SQL-compatible values for SQL templating."""

        values = {}
        if self.rule_set:
            values["rule_set"] = "'" + "', '".join(self.rule_set) + "'"

        if self.severity:
            values["severity"] = "'" + "', '".join(self.severity) + "'"

        if self.model:
            values["model"] = self.model

        return values
