from typing import Optional

from pydantic import BaseModel, UUID4


class ProcessRequest(BaseModel):
    """Payload to request the processing of a Project Run"""

    run_id: str
    manifest_location: str
    account_id: Optional[int]
    correlation_id: Optional[UUID4]
    run_results_location: Optional[str]
