import datetime
import json
from enum import Enum
from typing import Optional

from whetstone_api.schemas import (
    CoreModel,
    DateTimeModelMixin,
    IDModelMixin,
    ETagPropertyMixin,
    RunModelMixins,
)
from whetstone_api.schemas.run_statistics import RunStatisticDict


class RunStatus(str, Enum):
    """The status of a Project Run"""

    queued = "queued"
    starting = "starting"
    running = "running"
    success = "success"
    error = "error"
    cancelled = "cancelled"


class ArtifactType(str, Enum):
    """Types of artifact that Whetstone can return."""

    manifest = "manifest.json"
    run_results = "run_results.json"
    graph = "graph.json"


class Artifact(CoreModel):
    """A wrapper for a generic Artifact payload."""

    __root__: dict


class AbstractRun(CoreModel):
    """An abstract Run that can take multiple types."""

    status: RunStatus
    manifest_location: str
    run_results_location: Optional[str]
    graph_location: Optional[str]
    metadata: Optional[dict]

    def __init__(self, *args, **kwargs):

        if isinstance(kwargs.get("metadata"), str):
            kwargs["metadata"] = json.loads(kwargs["metadata"])

        super().__init__(*args, **kwargs)


class RunBase(AbstractRun):
    """An analysis Run triggered by a user."""

    # Run information
    account_id: int
    project_id: int


class RunInDB(ETagPropertyMixin, DateTimeModelMixin, RunBase, IDModelMixin):
    """The representation of a Run stored in the database."""

    dequeued_at: Optional[datetime.datetime]
    started_at: Optional[datetime.datetime]
    finished_at: Optional[datetime.datetime]


class RunCreate(CoreModel):
    """Model used to validate the creation of a new Run."""

    project_id: int
    manifest: dict
    run_results: Optional[dict]


class RunRetry(CoreModel):
    """Model used to retry an existing Run"""

    run_id: int


class RunUpdate(CoreModel):
    """Model used to validate the updating of a Run."""

    status: Optional[RunStatus]
    dequeued_at: Optional[datetime.datetime]
    started_at: Optional[datetime.datetime]
    finished_at: Optional[datetime.datetime]
    graph_location: Optional[str]


class Run(RunModelMixins, RunInDB):
    """
    A Run represents the specific invocation of the Whetstone Engine.
    """

    manifest_url: Optional[str]
    run_results_url: Optional[str]
    graph_url: Optional[str]

    statistics: Optional[RunStatisticDict]
