from pydantic import UUID4
from typing import Optional, Union, Dict

from whetstone_api.schemas import CoreModel, DateTimeModelMixin, IDModelMixin


class RunStatisticBase(CoreModel):
    """Arbitrary statistics relating to a run."""

    run_id: Union[int, UUID4]

    label: str
    statistic: str
    value: float


class RunStatisticInDB(DateTimeModelMixin, RunStatisticBase, IDModelMixin):
    """The in-database representation of a RunStatistic"""

    delta: Optional[float]


class RunStatistic(RunStatisticInDB):
    """The public versio of a RunStatistic."""


class RunStatisticCreate(RunStatisticBase):
    """Model used to validate the creation of run RunStatistics."""


class RunStatisticDict(CoreModel):
    """A dictionary aggregation of list of RunStatistics."""

    __root__: Dict[str, RunStatistic]
