import datetime
from typing import Optional

from pydantic import UUID4

from whetstone_api.schemas import (
    ETagPropertyMixin,
    DateTimeModelMixin,
    UUIDModelMixin,
    CoreModel,
    RunModelMixins,
)
from whetstone_api.schemas.run_statistics import RunStatisticDict
from whetstone_api.schemas.runs import AbstractRun, RunUpdate


class DemoRunBase(AbstractRun):
    """
    A DemoRunBase is the base representation of a DemoRun. A DemoRun is a Run
    that is not associated with a project or an account.
    """

    correlation_id: UUID4


class DemoRunInDB(ETagPropertyMixin, DateTimeModelMixin, DemoRunBase, UUIDModelMixin):
    """The in-database representation of a DemoRun."""

    dequeued_at: Optional[datetime.datetime]
    started_at: Optional[datetime.datetime]
    finished_at: Optional[datetime.datetime]


class DemoRunCreate(CoreModel):
    """Model used to validate the creation of a new DemoRun."""

    correlation_id: UUID4
    manifest: dict
    run_results: Optional[dict]


class DemoRunRetry(CoreModel):
    """Model used to retry an existing DemoRun"""

    run_id: UUID4


class DemoRunUpdate(RunUpdate):
    """Model used to validate the updating of a DemoRun."""


class DemoRun(RunModelMixins, DemoRunInDB):
    """
    A DemoRun represents the specific invocation of the Whetstone Engine.
    """

    manifest_url: Optional[str]
    run_results_url: Optional[str]
    graph_url: Optional[str]

    statistics: Optional[RunStatisticDict]
