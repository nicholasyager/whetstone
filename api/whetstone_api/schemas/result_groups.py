from typing import List, NewType, Optional, Union, Dict

from whetstone_api.schemas import CoreModel, DateTimeModelMixin, IDModelMixin
from whetstone_api.schemas.results import Result, ResultCreate


class ModelResultGroupContent(CoreModel):
    """Metadata relevant to a ResultGroup for a Model"""

    query: str
    subgraph: dict
    execution_time: Optional[float]


ResultGroupContent = NewType("ResultGroupContent", Union[ModelResultGroupContent])


class AbstractResultGroup(CoreModel):
    """The base-most representation of a ResultGroup that transcends production and demo data sets."""

    # An Entity is a data type that is being reported on. This is something like a Model or a Job.
    entity: str
    entity_type: str
    entity_id: str

    content: ResultGroupContent
    is_stale: bool = False

    filters: List[Dict] = []


class BaseResultGroup(AbstractResultGroup):
    """A base ResultGroup that is composed into different ResultGroup types."""

    # Information about the Run the created the ResultGroup
    account_id: int
    run_id: int


class ResultGroupInDB(DateTimeModelMixin, BaseResultGroup, IDModelMixin):
    """The in-database representation of a ResultGroup record."""

    result_count: int
    severity_score: int


class ResultGroup(ResultGroupInDB):
    """
    A record that indicates a collection or Results. A ResultGroup relates to a specific entity
    (like a model, or job), and contains metadata required to provide context about that entity.
    Additionally, the ResultGroup returns all Results associated with this ResultGroup to reduce
    the number of API/Database calls required.
    """

    results: List[Result]


class ResultGroupMutable(CoreModel):
    """Mutable aspects of a ResultGroup model."""

    entity: str
    entity_type: str
    entity_id: str
    content: ResultGroupContent
    is_stale: bool = False


class ResultGroupCreate(ResultGroupMutable):
    """A creation schema for creating a new ResultGroup."""

    results: List[ResultCreate]
    filters: List[Dict] = []


class ResultGroupUpdate(ResultGroupMutable):
    """Model used to validate the updating of a ResultGroup."""

    entity: Optional[str]
    entity_type: Optional[str]
    entity_id: Optional[str]
    content: Optional[ResultGroupContent]
    is_stale: bool = False
