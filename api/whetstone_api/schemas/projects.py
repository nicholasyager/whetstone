from enum import Enum
from typing import Optional, Dict

from whetstone_api.schemas import CoreModel, DateTimeModelMixin, IDModelMixin


class ProjectStatus(str, Enum):
    active = "active"
    inactive = "inactive"


class ProjectBase(CoreModel):
    """A Project encapsulates a DAG that is analyzed."""

    name: Optional[str]
    account_id: Optional[int]
    status: Optional[ProjectStatus]


class ProjectInDB(DateTimeModelMixin, ProjectBase, IDModelMixin):
    """The representation of a Project stored in the database."""


class ProjectCreate(CoreModel):
    """Model used to validate the creation of a new Project."""

    name: str
    status: ProjectStatus = ProjectStatus.active


class ProjectUpdate(CoreModel):
    """Model used to validate the updating of a Project."""

    name: str
    status: ProjectStatus


class Project(ProjectInDB):
    """
    A Project encapsulates a DAG that is may be analyzed. A project may
    have multiple runs associated with it.
    """

    statistics: Optional[Dict] = None
