import datetime
import hashlib
from typing import Optional
from uuid import uuid4

from pydantic import BaseModel, validator, UUID4, Field


class CoreModel(BaseModel):
    """Pydantic BaseModel with shared logic"""


class DateTimeModelMixin(BaseModel):
    """Add created and updated dates to a model."""

    created_at: Optional[datetime.datetime]
    updated_at: Optional[datetime.datetime]

    @validator("created_at", "updated_at", pre=True)
    def default_datetime(cls, value: datetime) -> datetime:
        return value or datetime.datetime.now()


class IDModelMixin(BaseModel):
    id: int


class UUIDModelMixin(BaseModel):
    """A mixin to set a model ID to a UUID4 upon instantiation."""

    id: UUID4 = Field(default_factory=uuid4)


class ETagPropertyMixin(BaseModel):
    """Add a content-based entity tag to the model's properties."""

    @property
    def etag(self) -> str:
        """The etag for the record. This is a content-based identifier"""
        return hashlib.sha1(self.json().encode()).hexdigest()


class RunModelMixins(BaseModel):
    """Date related fields and validators for Runs"""

    duration: Optional[float]
    queued_duration: Optional[float]
    run_duration: Optional[float]

    @validator("duration", always=True)
    def validate_duration(cls, _, values) -> Optional[float]:
        """The duration of the run once finished."""
        if values["finished_at"] is None or values["dequeued_at"] is None:
            return None

        return (values["finished_at"] - values["dequeued_at"]).total_seconds()

    @validator("queued_duration", always=True)
    def validate_queued_duration(cls, _, values) -> Optional[float]:
        """The duration that the run was queued."""
        if values["dequeued_at"] is None:
            return None

        return (values["dequeued_at"] - values["created_at"]).total_seconds()

    @validator("run_duration", always=True)
    def validate_run_duration(cls, _, values) -> Optional[float]:
        """The duration that the run executed for."""
        if values["finished_at"] is None or values["started_at"] is None:
            return None

        return (values["finished_at"] - values["started_at"]).total_seconds()
