from typing import Optional

import uuid

from enum import Enum
from pydantic.types import UUID

from whetstone_api.schemas import CoreModel, DateTimeModelMixin, UUIDModelMixin
from whetstone_api.services.cryptography import EncryptedValue


class WebhookSystem(str, Enum):
    """Specific systems we currently support webhooks from."""

    dbt_cloud = "dbt_cloud"


class WebhookBase(CoreModel):
    """
    A Webhook is the Whetstone side of a webhook handshake, and is responsible for
    creating a unique endpoint and storing authorization information."""

    system: WebhookSystem
    authorization: str


class WebhookInDB(DateTimeModelMixin, WebhookBase, UUIDModelMixin):
    """The representation of a Webhook stored in the database."""

    account_id: int
    project_id: int
    is_active: bool


class SensitiveWebhookInDB(WebhookInDB):
    """An internal representation of a Webhook that includes the Webhook's credentials."""

    credential: EncryptedValue


class Webhook(WebhookInDB):
    """A registered Webhook that Whetstone will track internally."""


class SensitiveWebhook(WebhookInDB):
    """A registered Webhook that Whetstone will track internally, including the secret Service Token."""

    service_token: str


class WebhookCreate(WebhookBase):
    """WebhookCreate defines how to create a webhook in Whetstone."""

    id: UUID = uuid.uuid4()
    service_token: str


class WebhookUpdate(CoreModel):
    """WebhookUpdate defines how to update a webhook in Whetstone."""

    is_active: Optional[bool] = None
    authorization: Optional[str] = None
    service_token: Optional[str] = None
