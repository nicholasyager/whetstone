from pydantic import UUID4

from whetstone_api.schemas import CoreModel, DateTimeModelMixin, IDModelMixin


class UserAccountBase(CoreModel):
    """Mapping between Users and Accounts"""

    user_id: UUID4
    account_id: int
    is_active: bool


class UserAccountInDB(DateTimeModelMixin, UserAccountBase, IDModelMixin):
    """The representation of a UserAccount stored in the database."""


class UserAccountCreate(CoreModel):
    """Model used to validate the creation of a new UserAccount."""

    user_id: UUID4
    account_id: int
    is_active: bool = True


class UserAccountUpdate(CoreModel):
    """Model used to validate the updating of a UserAccount."""

    is_active: bool


class UserAccount(UserAccountInDB):
    """
    A User represents the specific invocation of the Whetstone Engine.
    """
