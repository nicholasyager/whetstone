from typing import Dict, TypeVar, Generic, Optional, Any, Sized

from pydantic import Field
from pydantic.generics import GenericModel

from whetstone_api.schemas import CoreModel


class ResponsePagination(CoreModel):
    """Pagination data for a response."""

    count: int
    total_count: int


class ResponseMetadata(CoreModel):
    """
    Metadata for a given request, including the number of records in the complete
    result set, the number of records returned, and the limit and offset.
    """

    pagination: Optional[ResponsePagination]
    filters: Dict = Field(default={})
    order_by: Optional[str]


T = TypeVar("T")


class Response(GenericModel, Generic[T]):
    """
    An API Response data container, which returns metadata and the requested data.
    """

    metadata: ResponseMetadata
    data: T

    def __init__(self, **data: Any):
        if "metadata" not in data:
            data["metadata"] = ResponseMetadata()

        super().__init__(**data)


def generate_response(
    data: T,
    filters: Optional[Dict] = None,
    order_by: Optional[str] = None,
    total_count: Optional[int] = None,
) -> Response[T]:
    """Generate a valid Response object for a given payload."""

    if filters is None:
        filters = {}

    metadata = ResponseMetadata(filters=filters, order_by=order_by)

    if isinstance(data, Sized):
        metadata.pagination = ResponsePagination(
            count=len(data),
            total_count=total_count if total_count is not None else len(data),
        )

    return Response(data=data, metadata=metadata)
