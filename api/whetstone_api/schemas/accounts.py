from enum import Enum
from typing import Optional

from whetstone_api.schemas import IDModelMixin, DateTimeModelMixin, CoreModel


class AccountPlan(str, Enum):
    developer = "developer"
    team = "team"


class AccountStatus(str, Enum):
    pending = "pending"
    active = "active"
    inactive = "inactive"


class AccountBase(CoreModel):
    name: Optional[str]
    plan: Optional[AccountPlan]
    status: Optional[AccountStatus]


class AccountCreate(AccountBase):
    name: str
    plan: AccountPlan
    status: AccountStatus = AccountStatus.active


class AccountUpdate(CoreModel):
    name: str


class AccountInDB(DateTimeModelMixin, AccountBase, IDModelMixin):
    name: str
    plan: AccountPlan
    status: AccountStatus


class Account(AccountInDB):
    """The root customer class for Whetstone. Accounts have Users and Projects."""
