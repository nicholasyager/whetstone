import datetime

from pydantic import EmailStr

from whetstone_api import config
from whetstone_api.schemas import CoreModel


class JWTMeta(CoreModel):
    iss: str = config.DOMAIN
    aud: str = config.JWT_AUDIENCE
    iat: float = datetime.datetime.timestamp(datetime.datetime.utcnow())
    exp: float = datetime.datetime.timestamp(
        datetime.datetime.utcnow()
        + datetime.timedelta(minutes=config.ACCESS_TOKEN_EXPIRE_MINUTES)
    )


class JWTCreds(CoreModel):
    """How we'll identify users"""

    sub: str
    email: EmailStr


class JWTPayload(JWTMeta, JWTCreds):
    """
    JWT Payload right before it's encoded - combine meta and username
    """

    pass


class AccessToken(CoreModel):
    access_token: str
    token_type: str


class APIKeyAccess(CoreModel):
    is_superuser: bool
