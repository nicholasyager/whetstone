from enum import Enum
from typing import Optional, List

from pydantic import EmailStr, constr

from whetstone_api.schemas import (
    CoreModel,
    DateTimeModelMixin,
    UUIDModelMixin,
)
from whetstone_api.schemas.accounts import Account
from whetstone_api.schemas.tokens import AccessToken


class UserStatus(str, Enum):
    """The status of a User"""

    active = "active"
    inactive = "inactive"


class UserBase(CoreModel):
    """
    A User is a representation of a person or user-agent for Whetstone. Users
    participate in accounts, and can have Tokens.
    """

    email: EmailStr
    status: UserStatus = UserStatus.active
    is_verified: bool = False


class UserInDB(DateTimeModelMixin, UserBase, UUIDModelMixin):
    """The representation of a User stored in the database."""

    accounts: List[Account] = []


class SensitiveUserInDB(UserInDB):
    """An internal representation of a User that includes the user's Hashed password."""

    hashed_password: str


class UserCreate(CoreModel):
    """Model used to validate the creation of a new User."""

    email: EmailStr
    password: constr(min_length=7, max_length=100)


class UserUpdate(CoreModel):
    """Model used to validate the updating of a User."""

    email: Optional[EmailStr]
    status: Optional[UserStatus]
    is_verified: Optional[UserStatus]


class UserPasswordUpdate(CoreModel):
    """Mediate password changes for a User"""

    hashed_password: constr(min_length=7)


class UserInDBCreate(CoreModel):
    """Mediate Database record creation of a new user."""

    email: EmailStr
    hashed_password: constr(min_length=7)


class User(UserInDB):
    """
    A User represents the specific invocation of the Whetstone Engine.
    """

    access_token: Optional[AccessToken]
