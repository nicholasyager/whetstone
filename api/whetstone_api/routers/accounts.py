from typing import List, Union

from fastapi import APIRouter, Body, Depends, Path, HTTPException, Security
from starlette.status import HTTP_404_NOT_FOUND

from whetstone_api.dependencies.auth import (
    get_current_active_user,
    get_current_user_account_membership,
)
from whetstone_api.dependencies.database import get_repository
from whetstone_api.repositories.accounts import AccountRepository
from whetstone_api.schemas.accounts import Account, AccountUpdate
from whetstone_api.schemas.response import (
    Response,
    generate_response,
)
from whetstone_api.schemas.tokens import APIKeyAccess
from whetstone_api.schemas.users import UserInDB

router = APIRouter(prefix="/accounts", tags=["Accounts"])


@router.get("", response_model=Response[List[Account]])
async def list_accounts(
    repo: AccountRepository = Depends(get_repository(AccountRepository)),
    current_user: Union[UserInDB, APIKeyAccess] = Depends(get_current_active_user),
) -> Response[List[Account]]:
    """
    Use this List Accounts endpoint to enumerate the Accounts that your
    credentials have authorization to access.
    """

    if isinstance(current_user, APIKeyAccess):
        data = await repo.list()
    else:
        data = current_user.accounts

    return generate_response(data=data)


@router.get(
    "/{account_id}",
    response_model=Response[Account],
    dependencies=[
        Security(get_current_user_account_membership),
    ],
)
async def get_account(
    account_id: int,
    repo: AccountRepository = Depends(get_repository(AccountRepository)),
) -> Response[Account]:
    """
    Use the Get Account endpoint to access a specific Account based on the
    Account's ID.
    """
    account = await repo.get(account_id=account_id)
    return generate_response(data=account)


@router.put(
    "/{account_id}",
    response_model=Response[Account],
    dependencies=[
        Security(get_current_user_account_membership),
    ],
)
async def update_account(
    account_id: int = Path(..., ge=1, title="The ID of the Account to update."),
    account_update: AccountUpdate = Body(...),
    repo: AccountRepository = Depends(get_repository(AccountRepository)),
) -> Response[Account]:
    """
    Use the Update Account endpoint to update an Account's name.
    """
    updated_account = await repo.update(
        account_id=account_id,
        account_update=account_update,
    )
    if not updated_account:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Account found with that id.",
        )

    return generate_response(data=updated_account)
