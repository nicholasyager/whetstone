import hashlib
import hmac
import json
import logging
from celery import Celery
from fastapi import APIRouter, Depends, HTTPException, Request
from pydantic.types import UUID
from starlette.status import (
    HTTP_404_NOT_FOUND,
    HTTP_401_UNAUTHORIZED,
    HTTP_403_FORBIDDEN,
    HTTP_200_OK,
)

from whetstone_api.dependencies.celery import get_celery
from whetstone_api.dependencies.database import get_repository
from whetstone_api.dependencies.storage import Storage, get_storage
from whetstone_api.repositories.runs import RunRepository
from whetstone_api.repositories.webhooks import WebhookRepository
from whetstone_api.routers.runs import create_run
from whetstone_api.schemas.runs import RunCreate
from whetstone_api.schemas.webhooks import WebhookSystem
from whetstone_api.services import crypto_service

logger = logging.getLogger(__name__)


router = APIRouter(
    prefix="/webhooks",
    tags=["Webhooks"],
)


@router.post("/{webhook_id}", status_code=HTTP_200_OK)
async def receive_webhook(
    webhook_id: UUID,
    request: Request,
    repo: WebhookRepository = Depends(get_repository(WebhookRepository)),
    run_repo: RunRepository = Depends(get_repository(RunRepository)),
    storage: Storage = Depends(get_storage()),
    celery: Celery = Depends(get_celery()),
):
    """Receive and handle an inbound webhook."""

    # Check that the webhook exists
    body = await request.body()
    authorization = request.headers.get("authorization", None)

    if authorization is None:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="The request did not have a valid Authorization header.",
        )

    webhook = await repo.get_sensitive_webhook_by_id(webhook_id=webhook_id)

    if webhook is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail=f"No webhook found with the id `{webhook_id}`.",
        )

    # Confirm the authorization header
    app_secret = webhook.authorization.encode("utf-8")
    signature = hmac.new(app_secret, body, hashlib.sha256).hexdigest()

    if signature != authorization:
        raise HTTPException(
            status_code=HTTP_403_FORBIDDEN,
            detail="The submitted request did not have the appropriate Authorization to submit a webhook "
            "payload to this webhook endpoint. Please check your Authorization header against the secret "
            "submitted during registration.",
        )

    if webhook.system == WebhookSystem.dbt_cloud:
        payload = json.loads(body.decode("utf-8"))
        service_token = crypto_service.decrypt(webhook.credential)

        dbt_account_id = payload["accountId"]
        dbt_run_id = payload["data"]["runId"]

        if dbt_account_id == 0:
            logger.info("Received a test message! Return a success and do nothing.")
            return {"message": "Webhook received by Whetstone."}

        if payload["eventType"] != "job.run.completed":
            logger.info(
                "Received a message that is not a run completion! Return a success and do nothing."
            )
            return {
                "message": f"Webhook {payload['eventId']} received by Whetstone, but not being acted on."
            }

        import requests

        manifest = requests.get(
            f"https://cloud.getdbt.com/api/v2/accounts/"
            f"{dbt_account_id}/runs/{dbt_run_id}/artifacts/manifest.json",
            headers={
                "Authorization": f"Token {service_token}",
                "Content-Type": "application/json",
            },
        ).json()

        run: RunCreate = RunCreate(project_id=webhook.project_id, manifest=manifest)

        await create_run(
            account_id=webhook.account_id,
            run=run,
            repo=run_repo,
            storage=storage,
            celery=celery,
        )

    return
