import logging
from celery import Celery
from fastapi import APIRouter, Body, Depends, Path, HTTPException, Security
from starlette.status import HTTP_201_CREATED, HTTP_404_NOT_FOUND
from typing import List, Optional

from whetstone_api.dependencies.auth import get_current_user_account_membership
from whetstone_api.dependencies.celery import get_celery
from whetstone_api.dependencies.database import get_repository
from whetstone_api.dependencies.pagination import pagination
from whetstone_api.dependencies.storage import get_storage, Storage
from whetstone_api.repositories.run_statistics import RunStatisticRepository
from whetstone_api.repositories.runs import RunRepository
from whetstone_api.schemas.process_request import ProcessRequest
from whetstone_api.schemas.response import Response, generate_response
from whetstone_api.schemas.run_statistics import RunStatisticDict
from whetstone_api.schemas.runs import Run, RunCreate, RunUpdate, RunStatus

logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/accounts/{account_id}/runs",
    tags=["Runs"],
    dependencies=[
        Security(get_current_user_account_membership),
    ],
)


@router.get("", response_model=Response[List[Run]])
async def list_runs(
    account_id: int,
    project_id: Optional[int] = None,
    repo: RunRepository = Depends(get_repository(RunRepository)),
    pagination_config: dict = Depends(pagination),
) -> Response[List[Run]]:
    """
    Use this List Run endpoint to enumerate the Run associated with
    the provided Account that your credentials have authorization to access.
    """
    run_list = await repo.list_by_account_id(
        account_id=account_id, project_id=project_id, **pagination_config
    )

    total_count = await repo.count_by_account_id(
        account_id=account_id, project_id=project_id
    )

    return generate_response(data=run_list, total_count=total_count)


@router.post("", response_model=Response[Run], status_code=HTTP_201_CREATED)
async def create_run(
    account_id: int,
    run: RunCreate,
    repo: RunRepository = Depends(get_repository(RunRepository)),
    storage: Storage = Depends(get_storage()),
    celery: Celery = Depends(get_celery()),
) -> Response[Run]:
    """
    The Create Run endpoint is user to create a new Whetstone Run.
    """

    # Store artifacts somewhere
    manifest_key = storage.put_artifact(artifact=run.manifest)

    run_results_key = None
    if run.run_results:
        run_results_key = storage.put_artifact(artifact=run.run_results)

    run_record = await repo.create(
        account_id=account_id,
        new_run=run,
        manifest_location=manifest_key,
        run_results_location=run_results_key,
        metadata=run.manifest.get("metadata"),
    )

    # Trigger the `engine.process_run` task. Add a small delay to mitigate
    # against race conditions.
    try:
        request = ProcessRequest(
            run_id=str(run_record.id),
            account_id=run_record.account_id,
            manifest_location=run_record.manifest_location,
            run_results_location=run_record.run_results_location,
        )
        celery.send_task(
            name="engine.process_run", countdown=5, retries=2, args=[request.json()]
        )
    except Exception as e:
        logger.exception(e)

    # Return the run
    return generate_response(data=run_record)


@router.get("/{run_id}", response_model=Response[Run])
async def get_run(
    account_id: int,
    run_id: int,
    repo: RunRepository = Depends(get_repository(RunRepository)),
    statistic_repo: RunStatisticRepository = Depends(
        get_repository(RunStatisticRepository)
    ),
    storage: Storage = Depends(get_storage()),
) -> Response[Optional[Run]]:
    """
    Use the Get Run endpoint to access a specific Run based on the Run's ID.
    """

    run_from_db = await repo.get(account_id=account_id, run_id=run_id)

    if run_from_db is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Run found with that id.",
        )

    run = Run(**run_from_db.dict())

    # Load run statistics for the run if and only if the status is complete.
    if run.status == RunStatus.success:
        run_statistics = await statistic_repo.get_statistics_by_run(run_id=run.id)
        if len(run_statistics) > 0:
            run.statistics = RunStatisticDict.parse_obj(
                {
                    run_statistic.statistic: run_statistic
                    for run_statistic in run_statistics
                }
            )

    if run.manifest_location:
        run.manifest_url = storage.get_artifact_url(run.manifest_location)

    if run.run_results_location:
        run.run_results_url = storage.get_artifact_url(run.run_results_location)

    if run.graph_location:
        run.graph_url = storage.get_artifact_url(run.graph_location)

    return generate_response(data=run)


@router.put("/{run_id}", response_model=Response[Run])
async def update_run(
    account_id: int = Path(..., ge=1, title="The ID of the Account to update."),
    run_id: int = Path(..., ge=1, title="The ID of the Run to update."),
    run_update: RunUpdate = Body(...),
    repo: RunRepository = Depends(get_repository(RunRepository)),
) -> Response[Run]:
    """
    Use the Update Run endpoint to update a Run's status.
    """
    updated_run = await repo.update(
        account_id=account_id,
        run_id=run_id,
        run_update=run_update,
    )
    if not updated_run:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Run found with that id.",
        )

    return generate_response(data=updated_run)


@router.patch("/{run_id}", response_model=Response[Run])
async def partial_update_run(
    account_id: int = Path(..., ge=1, title="The ID of the Account to update."),
    run_id: int = Path(..., ge=1, title="The ID of the Run to update."),
    run_update: RunUpdate = Body(...),
    repo: RunRepository = Depends(get_repository(RunRepository)),
) -> Response[Run]:
    """
    Use the Update Run endpoint to update a Run's status.
    """
    updated_run = await repo.partial_update(
        account_id=account_id,
        run_id=run_id,
        run_update=run_update,
    )
    if not updated_run:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Run found with that id.",
        )

    return generate_response(data=updated_run)


@router.post(
    "/{run_id}/retry", response_model=Response[Run], status_code=HTTP_201_CREATED
)
async def retry_run(
    account_id: int = Path(..., ge=1, title="The ID of the Account to retry."),
    run_id: int = Path(..., ge=1, title="The ID of the Run to retry."),
    repo: RunRepository = Depends(get_repository(RunRepository)),
    celery: Celery = Depends(get_celery()),
) -> Response[Run]:
    """
    Use the Retry Run endpoint to retry an existing Run. This endpoint will return
    a new Run object that uses the same manifest and run results as the retried
    Run.
    """
    new_run = await repo.retry(account_id=account_id, run_id=run_id)
    if not new_run:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Run found with that id.",
        )

    # Trigger the `engine.process_run` task. Add a small delay to mitigate
    # against race conditions.
    try:
        request = ProcessRequest(
            run_id=str(new_run.id),
            account_id=new_run.account_id,
            manifest_location=new_run.manifest_location,
            run_results_location=new_run.run_results_location,
        )
        celery.send_task(
            name="engine.process_run", countdown=5, retries=2, args=[request.json()]
        )
    except Exception as e:
        logger.exception(e)

    return generate_response(data=new_run)


@router.post("", response_model=Response[Run], status_code=HTTP_201_CREATED)
async def create_run_statistic(
    account_id: int,
    run: RunCreate,
    repo: RunRepository = Depends(get_repository(RunRepository)),
    storage: Storage = Depends(get_storage()),
    celery: Celery = Depends(get_celery()),
) -> Response[Run]:
    """
    The Create Run endpoint is user to create a new Whetstone Run.
    """

    # Store artifacts somewhere
    manifest_key = storage.put_artifact(artifact=run.manifest)

    run_results_key = None
    if run.run_results:
        run_results_key = storage.put_artifact(artifact=run.run_results)

    run_record = await repo.create(
        account_id=account_id,
        new_run=run,
        manifest_location=manifest_key,
        run_results_location=run_results_key,
        metadata=run.manifest.get("metadata"),
    )

    # Trigger the `engine.process_run` task. Add a small delay to mitigate
    # against race conditions.
    try:
        request = ProcessRequest(
            run_id=str(run_record.id),
            account_id=run_record.account_id,
            manifest_location=run_record.manifest_location,
            run_results_location=run_record.run_results_location,
        )
        celery.send_task(
            name="engine.process_run", countdown=5, retries=2, args=[request.json()]
        )
    except Exception as e:
        logger.exception(e)

    # Return the run
    return generate_response(data=run_record)
