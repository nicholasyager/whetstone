import logging

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from pydantic import EmailStr
from starlette.status import HTTP_200_OK, HTTP_401_UNAUTHORIZED

from whetstone_api import config
from whetstone_api.dependencies.database import get_repository
from whetstone_api.repositories.users import UserRepository
from whetstone_api.schemas.tokens import AccessToken
from whetstone_api.services import auth_service

logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/auth",
    tags=["Authentication"],
)


@router.post("/token", response_model=AccessToken, status_code=HTTP_200_OK)
async def login_with_email_and_password(
    repo: UserRepository = Depends(get_repository(UserRepository)),
    form_data: OAuth2PasswordRequestForm = Depends(OAuth2PasswordRequestForm),
) -> AccessToken:
    """
    Mediate the authentication of a User using their email and password. Return
    an AccessToken.
    """

    user = await repo.authenticate_user(
        email=EmailStr(form_data.username), plaintext_password=form_data.password
    )

    if not user:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Authentication was unsuccessful.",
            headers={"WWW-Authenticate": "Bearer"},
        )

    access_token = AccessToken(
        access_token=auth_service.create_access_token_for_user(
            user=user,
            secret_key=str(config.SECRET_KEY),
            audience=config.JWT_AUDIENCE,
            algorithms=[config.JWT_ALGORITHM],
        ),
        token_type="bearer",
    )
    return access_token
