from fastapi import APIRouter, Body, Depends, Path, HTTPException, Security
from starlette.status import HTTP_201_CREATED, HTTP_404_NOT_FOUND
from typing import List, Optional

from whetstone_api.dependencies.auth import get_current_user_account_membership
from whetstone_api.dependencies.database import get_repository
from whetstone_api.dependencies.filtering import ResultGroupFilters, ResultFilters
from whetstone_api.dependencies.hotdag import CachedHotDAG, get_hotdag
from whetstone_api.dependencies.pagination import pagination
from whetstone_api.dependencies.storage import Storage, get_storage
from whetstone_api.repositories.result_groups import ResultGroupsRepository
from whetstone_api.repositories.results import ResultRepository
from whetstone_api.repositories.runs import RunRepository
from whetstone_api.schemas.response import (
    Response,
    generate_response,
)
from whetstone_api.schemas.result_groups import (
    ResultGroup,
    ResultGroupInDB,
    ResultGroupCreate,
    ResultGroupUpdate,
)

import logging

logger = logging.getLogger()

router = APIRouter(
    prefix="/accounts/{account_id}/runs/{run_id}/results",
    tags=["Results"],
    dependencies=[
        Security(get_current_user_account_membership),
    ],
)


@router.get("", response_model=Response[List[ResultGroup]])
async def list_results(
    account_id: int,
    run_id: int,
    include_stale: bool = False,
    repo: ResultGroupsRepository = Depends(get_repository(ResultGroupsRepository)),
    result_repo: ResultRepository = Depends(get_repository(ResultRepository)),
    pagination_config: dict = Depends(pagination),
    filters: ResultGroupFilters = Depends(ResultGroupFilters),
    storage: Storage = Depends(get_storage()),
    run_repo: RunRepository = Depends(get_repository(RunRepository)),
    hotdag: CachedHotDAG = Depends(get_hotdag()),
) -> Response[List[ResultGroup]]:
    """
    Use this List Results endpoint to enumerate the ResultGroups and Results associated with
    the provided Account and Run that your credentials have authorization to
    access.
    """

    # Check if a selector has been provided. If so, fetch the manifest and evaluate
    if filters.select:
        logger.debug("Selector detected. Fetching manifest for node selection.")
        run = await run_repo.get(account_id=account_id, run_id=run_id)
        if not run:
            raise HTTPException(
                status_code=HTTP_404_NOT_FOUND,
                detail="No ResultGroup found with that combination of account_id and run_id.",
            )

        filters.unique_ids = hotdag.get_selection(
            key=run.manifest_location, select=filters.select, exclude=filters.exclude
        )

    result_group_list: [ResultGroupInDB] = await repo.list_by_account_id(
        account_id=account_id,
        run_id=run_id,
        include_stale=include_stale,
        **pagination_config,
        filters=filters
    )

    total_count = await repo.count_by_account_id(
        account_id=account_id,
        run_id=run_id,
        include_stale=include_stale,
        filters=filters,
    )

    result_groups = [
        ResultGroup(
            **(result_group.dict()),
            results=await result_repo.list_by_result_group(
                result_group_id=result_group.id,
                filters=ResultFilters(
                    severity=filters.severity, rule_set=filters.rule_set
                ),
            )
        )
        for result_group in result_group_list
    ]

    return generate_response(data=result_groups, total_count=total_count)


@router.post(
    "", response_model=Response[List[ResultGroup]], status_code=HTTP_201_CREATED
)
async def create_result_group(
    account_id: int,
    run_id: int,
    result_groups: List[ResultGroupCreate],
    repo: ResultGroupsRepository = Depends(get_repository(ResultGroupsRepository)),
) -> Response[List[ResultGroup]]:
    """
    The Create Result Groups endpoint is user to create a new Whetstone ResultGroups.
    """

    new_result_group_tuples = await repo.create(
        account_id=account_id, run_id=run_id, new_result_groups=result_groups
    )

    return generate_response(
        data=[
            ResultGroup(**(result_group.dict()), results=results)
            for result_group, results in new_result_group_tuples
        ]
    )


@router.get("/{result_group_id}", response_model=Response[ResultGroup])
async def get_result_group(
    account_id: int,
    run_id: int,
    result_group_id: int,
    repo: ResultGroupsRepository = Depends(get_repository(ResultGroupsRepository)),
    result_repo: ResultRepository = Depends(get_repository(ResultRepository)),
) -> Response[Optional[ResultGroup]]:
    """
    Use the Get Result Group endpoint to access a specific ResultGroup based on the Result Group's ID.
    """
    result_group = await repo.get(
        account_id=account_id, run_id=run_id, result_group_id=result_group_id
    )

    if not result_group:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No ResultGroup found with that id.",
        )

    results = await result_repo.list_by_result_group(result_group_id=result_group.id)

    return generate_response(data=ResultGroup(**(result_group.dict()), results=results))


@router.put("/{result_group_id}", response_model=Response[ResultGroup])
async def update_result_group(
    account_id: int = Path(
        ..., ge=1, title="The ID of the Account that the Result is a part of."
    ),
    run_id: int = Path(
        ..., ge=1, title="The ID of the Run that the Result is a part of."
    ),
    result_group_id: int = Path(
        ..., ge=1, title="The ID of the ResultGroup to update."
    ),
    result_group_update: ResultGroupUpdate = Body(...),
    repo: ResultGroupsRepository = Depends(get_repository(ResultGroupsRepository)),
    result_repo: ResultRepository = Depends(get_repository(ResultRepository)),
) -> Response[ResultGroup]:
    """
    Use the Update ResultGroup endpoint to update a Result Group's content.
    """
    updated_result_group = await repo.update(
        account_id=account_id,
        run_id=run_id,
        result_group_id=result_group_id,
        result_group_update=result_group_update,
    )

    if not updated_result_group:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Result found with that id.",
        )

    results = await result_repo.list_by_result_group(
        result_group_id=updated_result_group.id
    )

    return generate_response(
        data=ResultGroup(**(updated_result_group.dict()), results=results)
    )


@router.post("/invalidate", response_model=Response[List[ResultGroup]])
async def invalidate_results(
    account_id: int = Path(
        ..., ge=1, title="The ID of the Account that the ResultGroup is a part of."
    ),
    run_id: int = Path(
        ..., ge=1, title="The ID of the Run that the ResultGroup is a part of."
    ),
    repo: ResultGroupsRepository = Depends(get_repository(ResultGroupsRepository)),
    result_repo: ResultRepository = Depends(get_repository(ResultRepository)),
) -> Response[List[ResultGroup]]:
    """
    Use the Invalidate Results endpoint to mark all Results and ResultGroups associated with a
    run as stale.
    """
    updated_result_groups = await repo.invalidate_results(
        account_id=account_id, run_id=run_id
    )

    return generate_response(
        data=[
            ResultGroup(
                **(result_group.dict()),
                results=await result_repo.list_by_result_group(
                    result_group_id=result_group.id
                )
            )
            for result_group in updated_result_groups
        ]
    )
