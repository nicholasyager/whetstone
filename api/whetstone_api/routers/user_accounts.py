from typing import List

from fastapi import APIRouter, Depends, HTTPException, Security
from starlette.status import HTTP_400_BAD_REQUEST

from whetstone_api.dependencies.auth import (
    get_current_user_account_membership,
)
from whetstone_api.dependencies.database import get_repository
from whetstone_api.repositories.user_accounts import UserAccountRepository
from whetstone_api.schemas.response import Response, generate_response
from whetstone_api.schemas.user_accounts import (
    UserAccount,
    UserAccountCreate,
    UserAccountUpdate,
)

router = APIRouter(
    prefix="/accounts/{account_id}/members",
    tags=["Accounts"],
    dependencies=[
        Security(get_current_user_account_membership),
    ],
)


@router.get("", response_model=Response[List[UserAccount]])
async def list_members(
    account_id: int,
    repo: UserAccountRepository = Depends(get_repository(UserAccountRepository)),
) -> Response[List[UserAccount]]:
    """
    Use this List Members endpoint to enumerate the Users associated
    with a particular account
    """

    members = await repo.get_user_accounts_for_account(account_id=account_id)
    return generate_response(data=members)


@router.post("", response_model=Response[UserAccount])
async def add_members(
    account_id: int,
    new_user_account: UserAccountCreate,
    repo: UserAccountRepository = Depends(get_repository(UserAccountRepository)),
) -> Response[UserAccount]:
    """
    Use this List Members endpoint to enumerate the Users associated
    with a particular account
    """

    if new_user_account.account_id != account_id:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=(
                "The account_id provided in the request body must "
                "match the provided account_id in the URL."
            ),
        )

    member = await repo.add_user_to_account(
        user_id=new_user_account.user_id, account_id=new_user_account.account_id
    )
    return generate_response(data=member)


@router.get(
    "/{user_account_id}",
    response_model=Response[UserAccount],
    dependencies=[
        Security(get_current_user_account_membership),
    ],
)
async def get_user_account(
    user_account_id: int,
    repo: UserAccountRepository = Depends(get_repository(UserAccountRepository)),
) -> Response[UserAccount]:
    """
    Use the Get Account endpoint to access a specific Account based on the
    Account's ID.
    """
    member = await repo.get(user_account_id=user_account_id)
    return generate_response(data=member)


@router.put(
    "/{user_account_id}",
    response_model=Response[UserAccount],
    dependencies=[
        Security(get_current_user_account_membership),
    ],
)
async def update_user_account(
    user_account_id: int,
    update_user_account_record: UserAccountUpdate,
    repo: UserAccountRepository = Depends(get_repository(UserAccountRepository)),
) -> Response[UserAccount]:
    """
    Use the Get Account endpoint to access a specific Account based on the
    Account's ID.
    """
    member = await repo.update_user_account(
        user_account_id=user_account_id, update_user_account=update_user_account_record
    )
    return generate_response(data=member)
