from pydantic.types import UUID
from typing import List

from fastapi import APIRouter, Body, Depends, Path, HTTPException, Security
from starlette.status import HTTP_201_CREATED, HTTP_404_NOT_FOUND

from whetstone_api.dependencies.auth import get_current_user_account_membership
from whetstone_api.dependencies.database import get_repository
from whetstone_api.repositories.projects import ProjectRepository
from whetstone_api.repositories.webhooks import WebhookRepository
from whetstone_api.schemas.projects import Project, ProjectCreate, ProjectUpdate
from whetstone_api.schemas.response import generate_response, Response
from whetstone_api.schemas.webhooks import (
    Webhook,
    WebhookCreate,
    WebhookUpdate,
    SensitiveWebhook,
)
from whetstone_api.services import CryptographyService, crypto_service

router = APIRouter(
    prefix="/accounts/{account_id}/projects",
    tags=["Projects"],
    dependencies=[
        Security(get_current_user_account_membership),
    ],
)


@router.get(
    "",
    response_model=Response[List[Project]],
)
async def list_projects(
    account_id: int,
    repo: ProjectRepository = Depends(get_repository(ProjectRepository)),
) -> Response[List[Project]]:
    """
    Use this List Projects endpoint to enumerate the Projects associated with
    the provided Account that your credentials have authorization to access.
    """

    projects = await repo.list_by_account_id(account_id=account_id)

    output = [
        Project(
            **(project.dict()),
            statistics=await repo.get_project_statistics(
                account_id=account_id, project_id=project.id
            )
        )
        for project in projects
    ]

    return generate_response(data=output)


@router.post("", response_model=Response[Project], status_code=HTTP_201_CREATED)
async def create_project(
    account_id: int,
    project: ProjectCreate,
    repo: ProjectRepository = Depends(get_repository(ProjectRepository)),
) -> Response[Project]:
    """
    The Create Project endpoint is user to create a new Whetstone Project
    with the desired name as part of the provided account.
    """
    return generate_response(
        data=await repo.create(account_id=account_id, new_project=project)
    )


@router.get("/{project_id}", response_model=Response[Project])
async def get_project(
    account_id: int,
    project_id: int,
    repo: ProjectRepository = Depends(get_repository(ProjectRepository)),
) -> Response[Project]:
    """
    Use the Get Account endpoint to access a specific Account based on the
    Account's ID.
    """
    project = await repo.get(account_id=account_id, project_id=project_id)

    if project is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Project found with that id.",
        )

    output = Project(
        **(project.dict()),
        statistics=await repo.get_project_statistics(
            account_id=account_id, project_id=project_id
        )
    )

    return generate_response(data=output)


@router.put("/{project_id}", response_model=Response[Project])
async def update_project(
    account_id: int = Path(..., ge=1, title="The ID of the Account to update."),
    project_id: int = Path(..., ge=1, title="The ID of the Project to update."),
    project_update: ProjectUpdate = Body(...),
    repo: ProjectRepository = Depends(get_repository(ProjectRepository)),
) -> Response[Project]:
    """
    Use the Update Project endpoint to update a Project's name or status.
    """
    updated_project = await repo.update(
        account_id=account_id,
        project_id=project_id,
        project_update=project_update,
    )
    if not updated_project:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Project found with that id.",
        )

    return generate_response(data=updated_project)


@router.get("/{project_id}/webhooks", response_model=Response[List[Webhook]])
async def list_webhooks(
    account_id: int,
    project_id: int,
    repo: WebhookRepository = Depends(get_repository(WebhookRepository)),
) -> Response[List[Webhook]]:
    """List all webhooks associated with a project."""

    webhooks = await repo.list_webhooks_for_project(
        account_id=account_id, project_id=project_id
    )
    return generate_response(data=webhooks, total_count=len(webhooks))


@router.post(
    "/{project_id}/webhooks",
    response_model=Response[Webhook],
    status_code=HTTP_201_CREATED,
)
async def register_webhook(
    account_id: int,
    project_id: int,
    new_webhook: WebhookCreate,
    repo: WebhookRepository = Depends(get_repository(WebhookRepository)),
) -> Response[Webhook]:
    """Register a webhook for a specific project. This requires a valid authorization string and system type."""

    webhook = await repo.register_webhook(
        account_id=account_id, project_id=project_id, new_webhook=new_webhook
    )

    if webhook is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No webhook found with that id.",
        )

    return generate_response(data=Webhook(**(webhook.dict())))


@router.get("/{project_id}/webhooks/{webhook_id}", response_model=Response[Webhook])
async def get_webhook(
    account_id: int,
    project_id: int,
    webhook_id: UUID,
    repo: WebhookRepository = Depends(get_repository(WebhookRepository)),
) -> Response[Webhook]:
    """Get a webhook by ID"""

    webhook = await repo.get_webhook(
        webhook_id=webhook_id, account_id=account_id, project_id=project_id
    )

    if webhook is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No webhook found with that id.",
        )

    return generate_response(data=Webhook(**(webhook.dict())))


@router.get(
    "/{project_id}/webhooks/{webhook_id}/secure",
    response_model=Response[SensitiveWebhook],
)
async def get_secure_webhook(
    account_id: int,
    project_id: int,
    webhook_id: UUID,
    repo: WebhookRepository = Depends(get_repository(WebhookRepository)),
) -> Response[SensitiveWebhook]:
    """Get a webhook and its service token by ID"""

    webhook = await repo.get_sensitive_webhook(
        webhook_id=webhook_id, account_id=account_id, project_id=project_id
    )

    if webhook is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No webhook found with that id.",
        )

    crypto: CryptographyService = crypto_service

    sensitive_webhook = SensitiveWebhook(
        **(webhook.dict()), service_token=crypto.decrypt(webhook.credential)
    )
    return generate_response(data=sensitive_webhook)


@router.put("/{project_id}/webhooks/{webhook_id}", response_model=Response[Webhook])
async def update_webhook(
    account_id: int,
    project_id: int,
    webhook_id: UUID,
    updated_webhook: WebhookUpdate,
    repo: WebhookRepository = Depends(get_repository(WebhookRepository)),
) -> Response[Webhook]:
    """Update an existing webhook with new data."""
    updated_webhook = await repo.update_webhook(
        account_id=account_id,
        project_id=project_id,
        webhook_id=webhook_id,
        webhook_update=updated_webhook,
    )
    if not updated_webhook:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Webhook found with that id.",
        )

    return generate_response(data=updated_webhook)
