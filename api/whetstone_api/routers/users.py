import logging

from fastapi import APIRouter, Depends
from starlette.status import HTTP_201_CREATED

from whetstone_api.dependencies.auth import get_current_active_user
from whetstone_api.dependencies.database import get_repository
from whetstone_api.repositories.accounts import AccountRepository
from whetstone_api.repositories.user_accounts import UserAccountRepository
from whetstone_api.repositories.users import UserRepository
from whetstone_api.schemas.accounts import AccountCreate, Account
from whetstone_api.schemas.response import Response, generate_response
from whetstone_api.schemas.users import UserCreate, User, UserInDB

logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/users",
    tags=["Users"],
)


@router.post("", response_model=Response[User], status_code=HTTP_201_CREATED)
async def register_new_user(
    new_user: UserCreate, repo: UserRepository = Depends(get_repository(UserRepository))
) -> Response[User]:
    """
    The User Registration endpoint is used to register a new Whetstone User.
    Users require a unique email address, and the email address must be
    verified before the User can authenticate.
    """

    created_user = await repo.register_new_user(new_user=new_user)
    return generate_response(data=created_user)


@router.get("/me", response_model=Response[User])
async def get_own_user(
    current_user: UserInDB = Depends(get_current_active_user),
) -> Response[User]:
    """Return the authenticated User."""
    return generate_response(data=current_user)


@router.post(
    "/me/accounts",
    response_model=Response[Account],
    status_code=HTTP_201_CREATED,
)
async def create_user_account(
    account: AccountCreate,
    repo: AccountRepository = Depends(get_repository(AccountRepository)),
    user_account_repo: UserAccountRepository = Depends(
        get_repository(UserAccountRepository)
    ),
    current_user: UserInDB = Depends(get_current_active_user),
) -> Response[Account]:
    """Create an account for this user, and add the user to the account"""

    # Create a new account.
    new_account = await repo.create(new_account=account)

    # Add the user to the new account.
    await user_account_repo.add_user_to_account(
        user_id=current_user.id, account_id=new_account.id
    )

    return generate_response(data=new_account)
