import logging
from fastapi import APIRouter, Depends, Security
from starlette.exceptions import HTTPException
from starlette.status import HTTP_201_CREATED, HTTP_404_NOT_FOUND
from typing import List

from whetstone_api.dependencies.auth import get_current_user_account_membership
from whetstone_api.dependencies.database import get_repository
from whetstone_api.repositories.demo.runs import DemoRunRepository
from whetstone_api.repositories.run_statistics import RunStatisticRepository
from whetstone_api.repositories.runs import RunRepository
from whetstone_api.schemas.response import Response, generate_response
from whetstone_api.schemas.run_statistics import RunStatisticCreate, RunStatistic

logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/statistics",
    tags=["Statistics"],
)


@router.post(
    "/runs",
    response_model=Response[List[RunStatistic]],
    status_code=HTTP_201_CREATED,
    dependencies=[Security(get_current_user_account_membership)],
)
async def create_run_statistics(
    run_statistics: List[RunStatisticCreate],
    account_id: int,
    repo: RunStatisticRepository = Depends(get_repository(RunStatisticRepository)),
    run_repo: RunRepository = Depends(get_repository(RunRepository)),
) -> Response[List[RunStatistic]]:
    """
    The Create RunStatistic endpoint is used to add statistics to an existing Whetstone run.
    """

    run = run_repo.get(run_id=run_statistics[0].run_id, account_id=account_id)

    if run is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Run found with that id.",
        )

    try:
        run_statistics = await repo.create(new_run_statistics=run_statistics)

    except Exception as e:
        logger.exception(e)
        raise e

    # Return the run
    return generate_response(data=run_statistics)


@router.post(
    "/demo_runs",
    response_model=Response[List[RunStatistic]],
    status_code=HTTP_201_CREATED,
)
async def create_demo_run_statistics(
    run_statistics: List[RunStatisticCreate],
    repo: RunStatisticRepository = Depends(get_repository(RunStatisticRepository)),
    run_repo: DemoRunRepository = Depends(get_repository(DemoRunRepository)),
) -> Response[List[RunStatistic]]:
    """
    The Create RunStatistic endpoint is used to add statistics to an existing Whetstone run.
    """

    run = run_repo.get(run_id=run_statistics[0].run_id)

    if run is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No DemoRun found with that id.",
        )

    try:
        run_statistics = await repo.create(new_run_statistics=run_statistics)

    except Exception as e:
        logger.exception(e)
        raise e

    # Return the run
    return generate_response(data=run_statistics)
