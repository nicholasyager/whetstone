from fastapi import APIRouter, Body, Depends, Path, HTTPException
from pydantic import UUID4
from starlette.status import HTTP_201_CREATED, HTTP_404_NOT_FOUND
from typing import List, Optional

from whetstone_api.dependencies.database import get_repository
from whetstone_api.dependencies.filtering import ResultGroupFilters, ResultFilters
from whetstone_api.dependencies.pagination import pagination
from whetstone_api.repositories.demo.result_groups import DemoResultGroupsRepository
from whetstone_api.repositories.demo.results import DemoResultRepository
from whetstone_api.schemas.demo.demo_result_groups import (
    DemoResultGroupInDB,
    DemoResultGroup,
    DemoResultGroupCreate,
    DemoResultGroupUpdate,
)
from whetstone_api.schemas.response import Response, generate_response

router = APIRouter(
    prefix="/demo/runs/{demo_run_id}/results",
    tags=["Demo"],
)


@router.get("", response_model=Response[List[DemoResultGroup]])
async def list_demo_results(
    demo_run_id: UUID4,
    include_stale: bool = False,
    repo: DemoResultGroupsRepository = Depends(
        get_repository(DemoResultGroupsRepository)
    ),
    result_repo: DemoResultRepository = Depends(get_repository(DemoResultRepository)),
    pagination_config: dict = Depends(pagination),
    filters: ResultGroupFilters = Depends(ResultGroupFilters),
) -> Response[List[DemoResultGroup]]:
    """
    Use this List DemoResult endpoint to enumerate the DemoResult associated with
    the provided DemoRun.
    """

    result_group_list: [DemoResultGroupInDB] = await repo.list_by_demo_run_id(
        demo_run_id=demo_run_id,
        include_stale=include_stale,
        **pagination_config,
        filters=filters
    )

    total_count = await repo.count_by_demo_run_id(
        demo_run_id=demo_run_id, include_stale=include_stale, filters=filters
    )

    result_groups = [
        DemoResultGroup(
            **(result_group.dict()),
            results=await result_repo.list_by_demo_result_group_id(
                demo_result_group_id=result_group.id,
                filters=ResultFilters(
                    severity=filters.severity, rule_set=filters.rule_set
                ),
            )
        )
        for result_group in result_group_list
    ]

    return generate_response(data=result_groups, total_count=total_count)


@router.post(
    "", response_model=Response[List[DemoResultGroup]], status_code=HTTP_201_CREATED
)
async def create_demo_result(
    demo_run_id: UUID4,
    result_groups: List[DemoResultGroupCreate],
    repo: DemoResultGroupsRepository = Depends(
        get_repository(DemoResultGroupsRepository)
    ),
) -> Response[List[DemoResultGroup]]:
    """
    The Create DemoResult endpoint is user to create a new Whetstone DemoResults.
    """

    new_result_group_tuples = await repo.create(
        demo_run_id=demo_run_id, new_result_groups=result_groups
    )

    return generate_response(
        data=[
            DemoResultGroup(**(result_group.dict()), results=results)
            for result_group, results in new_result_group_tuples
        ]
    )


@router.get(
    "/{demo_result_group_id}", response_model=Response[Optional[DemoResultGroup]]
)
async def get_demo_result(
    demo_result_group_id: UUID4,
    repo: DemoResultGroupsRepository = Depends(
        get_repository(DemoResultGroupsRepository)
    ),
    result_repo: DemoResultRepository = Depends(get_repository(DemoResultRepository)),
) -> Response[Optional[DemoResultGroup]]:
    """
    Use the Get DemoResult endpoint to access a specific Result based on the Result's ID.
    """

    result_group = await repo.get(demo_result_group_id=demo_result_group_id)

    if not result_group:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No ResultGroup found with that id.",
        )

    results = await result_repo.list_by_demo_result_group_id(
        demo_result_group_id=result_group.id, include_stale=False
    )

    return generate_response(
        data=DemoResultGroup(**(result_group.dict()), results=results)
    )


@router.put("/{demo_result_group_id}", response_model=Response[DemoResultGroup])
async def update_demo_result(
    demo_result_group_id: UUID4 = Path(..., title="The UUID of the Result to update."),
    result_group_update: DemoResultGroupUpdate = Body(...),
    repo: DemoResultGroupsRepository = Depends(
        get_repository(DemoResultGroupsRepository)
    ),
    result_repo: DemoResultRepository = Depends(get_repository(DemoResultRepository)),
) -> Response[DemoResultGroup]:
    """
    Use the Update Result endpoint to update a Result's summary, severity, or content.
    """

    updated_result_group = await repo.update(
        demo_result_group_id=demo_result_group_id,
        result_group_update=result_group_update,
    )

    if not updated_result_group:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Result found with that id.",
        )

    results = await result_repo.list_by_demo_result_group_id(
        demo_result_group_id=updated_result_group.id, include_stale=False
    )

    return generate_response(
        data=DemoResultGroup(**(updated_result_group.dict()), results=results)
    )


@router.post("/invalidate", response_model=Response[List[DemoResultGroup]])
async def invalidate_demo_results(
    demo_run_id: UUID4 = Path(
        ..., title="The ID of the Run that the Result is a part of."
    ),
    repo: DemoResultGroupsRepository = Depends(
        get_repository(DemoResultGroupsRepository)
    ),
    result_repo: DemoResultRepository = Depends(get_repository(DemoResultRepository)),
) -> Response[List[DemoResultGroup]]:
    """
    Use the Invalidate DemoResults endpoint to mark all results associated with a
    run as stale.
    """
    updated_result_groups = await repo.invalidate_results(demo_run_id=demo_run_id)

    return generate_response(
        data=[
            DemoResultGroup(
                **(result_group.dict()),
                results=await result_repo.list_by_demo_result_group_id(
                    demo_result_group_id=result_group.id
                )
            )
            for result_group in updated_result_groups
        ]
    )
