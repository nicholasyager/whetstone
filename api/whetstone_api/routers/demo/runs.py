import logging
from typing import List

from celery import Celery
from fastapi import APIRouter, Body, Depends, Path, HTTPException
from pydantic import UUID4
from starlette.status import HTTP_201_CREATED, HTTP_404_NOT_FOUND

from whetstone_api.dependencies.celery import get_celery
from whetstone_api.dependencies.database import get_repository
from whetstone_api.dependencies.pagination import pagination
from whetstone_api.dependencies.storage import get_storage, Storage
from whetstone_api.repositories.demo.runs import DemoRunRepository
from whetstone_api.repositories.run_statistics import RunStatisticRepository
from whetstone_api.schemas.demo.runs import DemoRun, DemoRunCreate, DemoRunUpdate
from whetstone_api.schemas.process_request import ProcessRequest
from whetstone_api.schemas.response import Response, generate_response
from whetstone_api.schemas.run_statistics import RunStatisticDict
from whetstone_api.schemas.runs import RunStatus

logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/demo/runs",
    tags=["Demo"],
)


@router.get("", response_model=Response[List[DemoRun]])
async def list_demo_runs(
    correlation_id: UUID4,
    repo: DemoRunRepository = Depends(get_repository(DemoRunRepository)),
    pagination_config: dict = Depends(pagination),
) -> Response[List[DemoRun]]:
    """
    Use this List DemoRun endpoint to enumerate the DemoRuns associated with
    the provided correlation_id.
    """
    runs = await repo.list_by_correlation_id(
        correlation_id=correlation_id, **pagination_config
    )
    total_count = await repo.count_by_correlation_id(correlation_id=correlation_id)
    return generate_response(data=runs, total_count=total_count)


@router.post("", response_model=Response[DemoRun], status_code=HTTP_201_CREATED)
async def create_demo_run(
    run: DemoRunCreate,
    repo: DemoRunRepository = Depends(get_repository(DemoRunRepository)),
    storage: Storage = Depends(get_storage()),
    celery: Celery = Depends(get_celery()),
) -> Response[DemoRun]:
    """
    The Create Run endpoint is user to create a new Whetstone Run.
    """

    # Store artifacts somewhere
    manifest_key = storage.put_artifact(artifact=run.manifest)

    run_results_key = None
    if run.run_results:
        run_results_key = storage.put_artifact(artifact=run.run_results)

    run_record = await repo.create(
        new_run=run,
        manifest_location=manifest_key,
        run_results_location=run_results_key,
        metadata=run.manifest.get("metadata"),
    )

    # Trigger the `engine.process_run_demo` task. Add a small delay to mitigate
    # against race conditions.
    try:
        request = ProcessRequest(
            run_id=str(run_record.id),
            correlation_id=run_record.correlation_id,
            manifest_location=run_record.manifest_location,
            run_results_location=run_record.run_results_location,
        )
        celery.send_task(
            name="engine.process_demo_run",
            countdown=5,
            retries=2,
            args=[request.json()],
        )
    except Exception as e:
        logger.exception(e)

    # Return the run
    return generate_response(data=run_record)


@router.get("/{run_id}", response_model=Response[DemoRun])
async def get_demo_run(
    run_id: UUID4,
    repo: DemoRunRepository = Depends(get_repository(DemoRunRepository)),
    statistic_repo: RunStatisticRepository = Depends(
        get_repository(RunStatisticRepository)
    ),
    storage: Storage = Depends(get_storage()),
) -> Response[DemoRun]:
    """
    Use the Get Run endpoint to access a specific DemoRun based on the Run's ID.
    """

    run_from_db = await repo.get(run_id=run_id)

    if run_from_db is None:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Demo Run found with that id.",
        )

    run = DemoRun(**run_from_db.dict())

    # Load run statistics for the run if and only if the status is complete.
    if run.status == RunStatus.success:
        run_statistics = await statistic_repo.get_statistics_by_run(run_id=run.id)
        if len(run_statistics) > 0:
            run.statistics = RunStatisticDict.parse_obj(
                {
                    run_statistic.statistic: run_statistic
                    for run_statistic in run_statistics
                }
            )

    if run.manifest_location:
        run.manifest_url = storage.get_artifact_url(run.manifest_location)

    if run.run_results_location:
        run.run_results_url = storage.get_artifact_url(run.run_results_location)

    if run.graph_location:
        run.graph_url = storage.get_artifact_url(run.graph_location)

    return generate_response(data=run)


@router.put("/{run_id}", response_model=Response[DemoRun])
async def update_demo_run(
    run_id: UUID4 = Path(..., title="The ID of the Run to update."),
    run_update: DemoRunUpdate = Body(...),
    repo: DemoRunRepository = Depends(get_repository(DemoRunRepository)),
) -> Response[DemoRun]:
    """
    Use the Update Run endpoint to update a Run's status.
    """
    updated_run = await repo.update(
        run_id=run_id,
        run_update=run_update,
    )
    if not updated_run:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Demo Run found with that id.",
        )

    return generate_response(data=updated_run)


@router.patch("/{run_id}", response_model=Response[DemoRun])
async def partial_update_demo_run(
    run_id: UUID4 = Path(..., title="The ID of the Run to update."),
    run_update: DemoRunUpdate = Body(...),
    repo: DemoRunRepository = Depends(get_repository(DemoRunRepository)),
) -> Response[DemoRun]:
    """
    Use the Update Run endpoint to update a Run's status.
    """
    updated_run = await repo.partial_update(
        run_id=run_id,
        run_update=run_update,
    )
    if not updated_run:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Run found with that id.",
        )

    return generate_response(data=updated_run)


@router.post(
    "/{run_id}/retry", response_model=Response[DemoRun], status_code=HTTP_201_CREATED
)
async def retry_demo_run(
    run_id: UUID4 = Path(..., title="The ID of the Run to retry."),
    repo: DemoRunRepository = Depends(get_repository(DemoRunRepository)),
    celery: Celery = Depends(get_celery()),
) -> Response[DemoRun]:
    """
    Use the Retry Run endpoint to retry an existing Run. This endpoint will return
    a new Run object that uses the same manifest and run results as the retried
    Run.
    """
    new_run = await repo.retry(run_id=run_id)
    if not new_run:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail="No Demo Run found with that id.",
        )

    # Trigger the `engine.process_demo_run` task. Add a small delay to mitigate
    # against race conditions.
    try:
        request = ProcessRequest(
            run_id=str(new_run.id),
            correlation_id=new_run.correlation_id,
            manifest_location=new_run.manifest_location,
            run_results_location=new_run.run_results_location,
        )
        celery.send_task(
            name="engine.process_demo_run",
            countdown=5,
            retries=2,
            args=[request.json()],
        )
    except Exception as e:
        logger.exception(e)

    return generate_response(data=new_run)
