import datetime
import uuid
from typing import List

import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from starlette.status import (
    HTTP_200_OK,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
    HTTP_201_CREATED,
)

from whetstone_api.schemas.demo.runs import DemoRunInDB
from whetstone_api.schemas.response import Response
from whetstone_api.schemas.runs import RunStatus


class TestDemoRunsRoutes:
    @pytest.mark.asyncio
    async def test_create_routes_exist(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.post(app.url_path_for("create_demo_run"), json={})
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_list_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.get(app.url_path_for("list_demo_runs"))
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.get(
            app.url_path_for("get_demo_run", run_id="asdjklhflasjhdflas")
        )
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_update_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.put(app.url_path_for("update_demo_run", run_id="1"))
        assert res.status_code != HTTP_404_NOT_FOUND


class TestGetDemoRun:
    @pytest.mark.asyncio
    async def test_get_run_by_id(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_demo_run: DemoRunInDB,
    ) -> None:
        res = await client.get(
            app.url_path_for("get_demo_run", run_id=str(test_demo_run.id))
        )
        assert res.status_code == HTTP_200_OK
        response: Response[DemoRunInDB] = Response[DemoRunInDB](**res.json())
        run: DemoRunInDB = response.data
        assert run.id == test_demo_run.id

    @pytest.mark.asyncio
    async def test_get_run_by_invalid_id_returns_404(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_demo_run: DemoRunInDB,
    ) -> None:
        res = await client.get(
            app.url_path_for("get_demo_run", run_id=str(uuid.uuid4()))
        )

        assert res.status_code == HTTP_404_NOT_FOUND


class TestUpdateDemoRuns:
    @pytest.mark.asyncio
    async def test_updates(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_demo_run: DemoRunInDB,
    ) -> None:
        dequeued_at = datetime.datetime(
            year=2022, month=1, day=1, tzinfo=datetime.timezone.utc
        )

        res = await client.put(
            app.url_path_for(
                "update_demo_run",
                run_id=str(test_demo_run.id),
            ),
            json={"status": "starting", "dequeued_at": dequeued_at.isoformat()},
        )
        assert res.status_code == HTTP_200_OK

        response: Response[DemoRunInDB] = Response[DemoRunInDB](**res.json())
        run: DemoRunInDB = response.data
        assert run.dequeued_at == dequeued_at
        assert run.status == RunStatus.starting

    @pytest.mark.asyncio
    async def test_multiple_updates(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_demo_run: DemoRunInDB,
    ) -> None:
        dequeued_at = datetime.datetime(
            year=2022, month=1, day=1, tzinfo=datetime.timezone.utc
        )
        started_at = dequeued_at + datetime.timedelta(seconds=10)

        res = await client.put(
            app.url_path_for(
                "update_demo_run",
                run_id=str(test_demo_run.id),
            ),
            json={"status": "starting", "dequeued_at": dequeued_at.isoformat()},
        )
        assert res.status_code == HTTP_200_OK

        res = await client.put(
            app.url_path_for(
                "update_demo_run",
                run_id=str(test_demo_run.id),
            ),
            json={
                "status": "running",
                "dequeued_at": None,
                "started_at": started_at.isoformat(),
            },
        )
        assert res.status_code == HTTP_200_OK

        response: Response[DemoRunInDB] = Response[DemoRunInDB](**res.json())
        run: DemoRunInDB = response.data
        assert run.dequeued_at is None
        assert run.started_at == started_at
        assert run.status == RunStatus.running

    @pytest.mark.asyncio
    async def test_partial_updates(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_demo_run: DemoRunInDB,
    ) -> None:
        dequeued_at = datetime.datetime(
            year=2022, month=1, day=1, tzinfo=datetime.timezone.utc
        )
        started_at = dequeued_at + datetime.timedelta(seconds=10)

        res = await client.patch(
            app.url_path_for(
                "partial_update_demo_run",
                run_id=str(test_demo_run.id),
            ),
            json={"status": "starting", "dequeued_at": dequeued_at.isoformat()},
        )
        assert res.status_code == HTTP_200_OK

        res = await client.patch(
            app.url_path_for(
                "partial_update_demo_run",
                run_id=str(test_demo_run.id),
            ),
            json={"status": "running", "started_at": started_at.isoformat()},
        )
        assert res.status_code == HTTP_200_OK

        response: Response[DemoRunInDB] = Response[DemoRunInDB](**res.json())
        run: DemoRunInDB = response.data
        assert run.dequeued_at == dequeued_at
        assert run.started_at == started_at
        assert run.status == RunStatus.running


class TestListDemoRuns:
    @pytest.mark.asyncio
    async def test_list_runs_invalid_id_returns_nothing(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_demo_run: DemoRunInDB,
    ) -> None:
        res = await client.get(
            app.url_path_for("list_demo_runs"),
            params={"correlation_id": str(uuid.uuid4())},
        )
        assert res.status_code == HTTP_200_OK
        response: Response[List[DemoRunInDB]] = Response[List[DemoRunInDB]](
            **res.json()
        )
        run_list = response.data
        assert len(run_list) == 0

    @pytest.mark.asyncio
    async def test_list_runs_by_correlation_id(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_demo_run: DemoRunInDB,
    ) -> None:
        res = await client.get(
            app.url_path_for("list_demo_runs"),
            params={"correlation_id": str(test_demo_run.correlation_id)},
        )
        assert res.status_code == HTTP_200_OK
        response: Response[List[DemoRunInDB]] = Response[List[DemoRunInDB]](
            **res.json()
        )
        run_list = response.data
        assert len(run_list) == 1


class TestCreateDemoRuns:
    @pytest.mark.asyncio
    async def test_invalid_input_raises_error(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
    ) -> None:
        res = await client.post(app.url_path_for("create_demo_run"), json={})
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_invalid_correlation_id_raises_error(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
    ) -> None:
        res = await client.post(
            app.url_path_for("create_demo_run"),
            json={
                "correlation_id": 9999,
                "manifest": {"foo": "bar"},
                "run_result": {"foo": "bar"},
            },
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_valid_input_succeeds(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
    ) -> None:
        res = await client.post(
            app.url_path_for("create_demo_run"),
            json={
                "correlation_id": str(uuid.uuid4()),
                "manifest": {"foo": "bar"},
                "run_result": {"foo": "bar"},
            },
        )

        assert res.status_code == HTTP_201_CREATED


class TestRetryRuns:
    @pytest.mark.asyncio
    async def test_retry_run_by_id(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_demo_run: DemoRunInDB,
    ) -> None:
        res = await client.post(
            app.url_path_for(
                "retry_demo_run",
                run_id=str(test_demo_run.id),
            )
        )
        assert res.status_code == HTTP_201_CREATED
        response: Response[DemoRunInDB] = Response[DemoRunInDB](**res.json())
        run: DemoRunInDB = response.data
        assert run.id != test_demo_run.id
        assert run.manifest_location == test_demo_run.manifest_location
        assert run.run_results_location == test_demo_run.run_results_location

    @pytest.mark.asyncio
    async def test_retry_run_by_invalid_id_returns_404(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_demo_run: DemoRunInDB,
    ) -> None:
        res = await client.post(
            app.url_path_for("retry_demo_run", run_id=str(uuid.uuid4()))
        )

        assert res.status_code == HTTP_404_NOT_FOUND
