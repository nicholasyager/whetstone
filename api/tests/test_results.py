from typing import List, Tuple

import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from starlette.status import (
    HTTP_200_OK,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
    HTTP_201_CREATED,
)

from whetstone_api.schemas.response import Response
from whetstone_api.schemas.result_groups import ResultGroupInDB
from whetstone_api.schemas.results import ResultInDB
from whetstone_api.schemas.runs import RunInDB


class TestResultsRoutes:
    @pytest.mark.asyncio
    async def test_create_routes_exist(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.post(
            app.url_path_for("create_result_group", account_id="1", run_id="1"), json={}
        )
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_list_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.get(
            app.url_path_for("list_results", account_id="1", run_id="1")
        )
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.get(
            app.url_path_for(
                "get_result_group", account_id="1", run_id="1", result_group_id="1"
            )
        )
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_update_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.put(
            app.url_path_for(
                "update_result_group", account_id="1", run_id="1", result_group_id="1"
            )
        )
        assert res.status_code != HTTP_404_NOT_FOUND


class TestGetResult:
    @pytest.mark.asyncio
    async def test_get_run_by_id(
        self,
        disable_auth,
        app: FastAPI,
        client: AsyncClient,
        test_result: Tuple[ResultGroupInDB, List[ResultInDB]],
    ) -> None:

        result_group, results = test_result

        res = await client.get(
            app.url_path_for(
                "get_result_group",
                account_id=str(result_group.account_id),
                run_id=str(result_group.run_id),
                result_group_id=str(result_group.id),
            )
        )
        assert res.status_code == HTTP_200_OK
        response: Response[ResultGroupInDB] = Response[ResultGroupInDB](**res.json())
        result_group: ResultGroupInDB = response.data
        assert result_group.id == result_group.id

    @pytest.mark.asyncio
    async def test_get_result_by_invalid_id_returns_null(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_result: Tuple[ResultGroupInDB, List[ResultInDB]],
    ) -> None:
        res = await client.get(
            app.url_path_for(
                "get_result_group",
                account_id=str(test_result[0].account_id),
                run_id=str(test_result[0].run_id),
                result_group_id="98234",
            )
        )

        assert res.status_code == HTTP_404_NOT_FOUND


class TestListResults:
    @pytest.mark.asyncio
    async def test_list_results_by_run_id(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_result: Tuple[ResultGroupInDB, List[ResultInDB]],
    ) -> None:

        result_group, _ = test_result

        res = await client.get(
            app.url_path_for(
                "list_results",
                account_id=str(test_result[0].account_id),
                run_id=str(test_result[0].run_id),
            )
        )
        assert res.status_code == HTTP_200_OK
        responses: Response[List[ResultGroupInDB]] = Response[List[ResultGroupInDB]](
            **res.json()
        )
        result: ResultGroupInDB = responses.data[0]
        assert result == result_group


class TestCreateResults:
    @pytest.mark.asyncio
    async def test_invalid_input_raises_error(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_run: RunInDB,
    ) -> None:
        res = await client.post(
            app.url_path_for(
                "create_result_group",
                account_id=str(test_run.account_id),
                run_id=str(test_run.id),
            ),
            json={},
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_invalid_severity_raises_error(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        res = await client.post(
            app.url_path_for(
                "create_result_group",
                account_id=str(test_run.account_id),
                run_id=str(test_run.id),
            ),
            json=[
                {
                    "entity": "testing",
                    "entity_id": "testing",
                    "entity_type": "model",
                    "content": {"query": "foo", "subgraph": {"key": "value"}},
                    "results": [
                        {
                            "summary": "Testing",
                            "severity": "pony",
                            "rule_set": "testing",
                            "model": "testing",
                        }
                    ],
                }
            ],
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_invalid_account_raises_error(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        res = await client.post(
            app.url_path_for(
                "create_result_group", account_id=str(123), run_id=str(test_run.id)
            ),
            json=[
                {
                    "entity": "testing",
                    "entity_id": "testing",
                    "entity_type": "model",
                    "content": {"query": "foo", "subgraph": {"key": "value"}},
                    "results": [
                        {
                            "summary": "Testing",
                            "severity": "low",
                            "rule_set": "testing",
                            "model": "testing",
                        }
                    ],
                }
            ],
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_invalid_run_raises_error(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        res = await client.post(
            app.url_path_for(
                "create_result_group",
                account_id=str(test_run.account_id),
                run_id=str(123),
            ),
            json=[
                {
                    "entity": "testing",
                    "entity_id": "testing",
                    "entity_type": "model",
                    "content": {"query": "foo", "subgraph": {"key": "value"}},
                    "results": [
                        {
                            "summary": "Testing",
                            "severity": "low",
                            "rule_set": "testing",
                            "model": "testing",
                        }
                    ],
                }
            ],
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_valid_input_succeeds(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_run: RunInDB,
    ) -> None:
        res = await client.post(
            app.url_path_for(
                "create_result_group",
                account_id=str(test_run.account_id),
                run_id=str(test_run.id),
            ),
            json=[
                {
                    "entity": "testing",
                    "entity_id": "testing",
                    "entity_type": "model",
                    "content": {"query": "foo", "subgraph": {"key": "value"}},
                    "results": [
                        {
                            "summary": "Testing",
                            "severity": "low",
                            "rule_set": "testing",
                            "model": "testing",
                        }
                    ],
                }
            ],
        )

        assert res.status_code == HTTP_201_CREATED


class TestResultInvalidation:
    @pytest.mark.asyncio
    async def test_list_excludes_stale_default(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_result: Tuple[ResultGroupInDB, List[ResultInDB]],
    ) -> None:
        res = await client.put(
            app.url_path_for(
                "update_result_group",
                account_id=str(test_result[0].account_id),
                run_id=str(test_result[0].run_id),
                result_group_id=str(test_result[0].id),
            ),
            json={
                "is_stale": True,
            },
        )

        assert res.status_code == HTTP_200_OK

        res = await client.get(
            app.url_path_for(
                "list_results",
                account_id=str(test_result[0].account_id),
                run_id=str(test_result[0].run_id),
            )
        )

        assert res.status_code == HTTP_200_OK
        response: Response[List[ResultGroupInDB]] = Response[List[ResultGroupInDB]](
            **res.json()
        )
        assert len(response.data) == 0

    @pytest.mark.asyncio
    async def test_list_stale_inclusion(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_result: Tuple[ResultGroupInDB, List[ResultInDB]],
    ) -> None:

        result_group, _ = test_result

        res = await client.put(
            app.url_path_for(
                "update_result_group",
                account_id=str(result_group.account_id),
                run_id=str(result_group.run_id),
                result_group_id=str(result_group.id),
            ),
            json={
                "is_stale": True,
            },
        )

        assert res.status_code == HTTP_200_OK

        res = await client.get(
            app.url_path_for(
                "list_results",
                account_id=str(result_group.account_id),
                run_id=str(result_group.run_id),
            ),
            params={"include_stale": True},
        )

        assert res.status_code == HTTP_200_OK
        responses: Response[List[ResultGroupInDB]] = Response[List[ResultGroupInDB]](
            **res.json()
        )
        result: ResultGroupInDB = responses.data[0]
        assert result.id == result_group.id

    @pytest.mark.asyncio
    async def test_bulk_invalidations(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_result: Tuple[ResultGroupInDB, List[ResultInDB]],
    ) -> None:

        result_group, _ = test_result

        res = await client.post(
            app.url_path_for(
                "invalidate_results",
                account_id=str(result_group.account_id),
                run_id=str(result_group.run_id),
            ),
            json={},
        )

        assert res.status_code == HTTP_200_OK

        res = await client.get(
            app.url_path_for(
                "list_results",
                account_id=str(result_group.account_id),
                run_id=str(result_group.run_id),
            )
        )

        assert res.status_code == HTTP_200_OK
        response: Response[List[ResultGroupInDB]] = Response[List[ResultGroupInDB]](
            **res.json()
        )
        assert len(response.data) == 0
