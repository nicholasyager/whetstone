import jwt
import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from starlette.status import HTTP_200_OK

from whetstone_api import config
from whetstone_api.schemas.users import UserInDB


class TestUserLogin:
    @pytest.mark.asyncio
    async def test_user_can_login_successfully_and_receives_valid_token(
        self,
        app: FastAPI,
        client: AsyncClient,
        test_user: UserInDB,
    ) -> None:

        client.headers["content-type"] = "application/x-www-form-urlencoded"
        login_data = {
            "username": test_user.email,
            "password": "starchild",  # insert user's plaintext password
        }

        res = await client.post(
            app.url_path_for("login_with_email_and_password"), data=login_data
        )
        assert res.status_code == HTTP_200_OK

        # check that token exists in response and has user encoded within it
        token = res.json().get("access_token")
        creds = jwt.decode(
            token,
            str(config.SECRET_KEY),
            audience=config.JWT_AUDIENCE,
            algorithms=[config.JWT_ALGORITHM],
        )
        assert "email" in creds
        assert creds["email"] == test_user.email
        assert "sub" in creds
        assert creds["sub"] == str(test_user.id)

        # check that token is proper type
        assert "token_type" in res.json()
        assert res.json().get("token_type") == "bearer"

    @pytest.mark.asyncio
    @pytest.mark.parametrize(
        "credential, wrong_value, status_code",
        (
            ("email", "wrong@email.com", 401),
            ("email", None, 422),
            ("email", "notemail", 401),
            ("password", "wrongpassword", 401),
            ("password", None, 422),
        ),
    )
    async def test_user_with_wrong_creds_doesnt_receive_token(
        self,
        app: FastAPI,
        client: AsyncClient,
        test_user: UserInDB,
        credential: str,
        wrong_value: str,
        status_code: int,
    ) -> None:

        client.headers["content-type"] = "application/x-www-form-urlencoded"
        user_data = test_user.dict()
        user_data["password"] = "starchild"  # insert user's plaintext password
        user_data[credential] = wrong_value

        login_data = {
            "username": user_data["email"],
            "password": user_data["password"],  # insert password from parameters
        }

        res = await client.post(
            app.url_path_for("login_with_email_and_password"), data=login_data
        )
        assert res.status_code == status_code
        assert "access_token" not in res.json()
