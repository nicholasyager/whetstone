from typing import Union, Type, Optional

import jwt
import pytest
from fastapi import FastAPI, HTTPException
from httpx import AsyncClient
from starlette.datastructures import Secret

from whetstone_api import config
from whetstone_api.schemas.users import UserInDB
from whetstone_api.services import auth_service


class TestAuthTokens:
    @pytest.mark.asyncio
    async def test_can_create_access_token_successfully(
        self, app: FastAPI, client: AsyncClient, test_user: UserInDB
    ) -> None:
        access_token = auth_service.create_access_token_for_user(
            user=test_user,
            secret_key=str(config.SECRET_KEY),
            audience=config.JWT_AUDIENCE,
            algorithms=[config.JWT_ALGORITHM],
        )
        creds = jwt.decode(
            access_token,
            str(config.SECRET_KEY),
            audience=config.JWT_AUDIENCE,
            algorithms=[config.JWT_ALGORITHM],
        )
        assert creds.get("email") is not None
        assert creds["email"] == test_user.email
        assert creds["aud"] == config.JWT_AUDIENCE

    @pytest.mark.asyncio
    async def test_token_missing_user_is_invalid(
        self, app: FastAPI, client: AsyncClient
    ) -> None:
        access_token = auth_service.create_access_token_for_user(
            user=None,
            secret_key=str(config.SECRET_KEY),
            audience=config.JWT_AUDIENCE,
            algorithms=[config.JWT_ALGORITHM],
            expires_in=config.ACCESS_TOKEN_EXPIRE_MINUTES,
        )
        with pytest.raises(jwt.PyJWTError):
            jwt.decode(
                access_token,
                str(config.SECRET_KEY),
                audience=config.JWT_AUDIENCE,
                algorithms=[config.JWT_ALGORITHM],
            )

    @pytest.mark.asyncio
    @pytest.mark.parametrize(
        "secret_key, jwt_audience, exception",
        (
            ("wrong-secret", config.JWT_AUDIENCE, jwt.InvalidSignatureError),
            (None, config.JWT_AUDIENCE, jwt.InvalidSignatureError),
            (config.SECRET_KEY, "othersite:auth", jwt.InvalidAudienceError),
        ),
    )
    async def test_invalid_token_content_raises_error(
        self,
        app: FastAPI,
        client: AsyncClient,
        test_user: UserInDB,
        secret_key: Union[str, Secret],
        jwt_audience: str,
        exception: Type[BaseException],
    ) -> None:

        with pytest.raises(exception):
            access_token = auth_service.create_access_token_for_user(
                user=test_user,
                secret_key=str(secret_key),
                audience=jwt_audience,
                algorithms=[config.JWT_ALGORITHM],
                expires_in=config.ACCESS_TOKEN_EXPIRE_MINUTES,
            )
            jwt.decode(
                access_token,
                str(config.SECRET_KEY),
                audience=config.JWT_AUDIENCE,
                algorithms=[config.JWT_ALGORITHM],
            )

    @pytest.mark.asyncio
    async def test_can_retrieve_email_from_token(
        self, app: FastAPI, client: AsyncClient, test_user: UserInDB
    ) -> None:
        token = auth_service.create_access_token_for_user(
            user=test_user,
            secret_key=str(config.SECRET_KEY),
            audience=config.JWT_AUDIENCE,
            algorithms=[config.JWT_ALGORITHM],
        )
        email = auth_service.get_email_from_token(
            token=token, secret_key=str(config.SECRET_KEY)
        )
        assert email == test_user.email

    @pytest.mark.asyncio
    @pytest.mark.parametrize(
        "secret, wrong_token",
        (
            (config.SECRET_KEY, "asdf"),  # use wrong token
            (config.SECRET_KEY, ""),  # use wrong token
            (config.SECRET_KEY, None),  # use wrong token
            ("ABC123", "use correct token"),  # use wrong secret
        ),
    )
    async def test_error_when_token_or_secret_is_wrong(
        self,
        app: FastAPI,
        client: AsyncClient,
        test_user: UserInDB,
        secret: Union[Secret, str],
        wrong_token: Optional[str],
    ) -> None:
        token = auth_service.create_access_token_for_user(
            user=test_user,
            secret_key=str(secret),
            audience=config.JWT_AUDIENCE,
            algorithms=[config.JWT_ALGORITHM],
        )
        if wrong_token == "use correct token":
            wrong_token = token
        with pytest.raises(HTTPException):
            auth_service.get_email_from_token(
                token=wrong_token, secret_key=str(config.SECRET_KEY)
            )
