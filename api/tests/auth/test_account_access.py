import pytest
from databases import Database
from fastapi import FastAPI, HTTPException
from httpx import AsyncClient

from whetstone_api.dependencies.auth import get_current_user_account_membership
from whetstone_api.repositories.users import UserRepository
from whetstone_api.schemas.accounts import AccountInDB
from whetstone_api.schemas.users import UserInDB


class TestAccountMembership:
    @pytest.mark.asyncio
    async def test_current_user_account_membership(
        self,
        app: FastAPI,
        client: AsyncClient,
        db: Database,
        test_user: UserInDB,
        test_account: AccountInDB,
    ) -> None:
        user_repo = UserRepository(db)
        updated_user = await user_repo.add_user_to_account(
            user_id=test_user.id, account_id=test_account.id
        )

        result = get_current_user_account_membership(
            current_user=updated_user, account_id=test_account.id
        )

        assert updated_user.id == test_user.id
        assert result is True

    @pytest.mark.asyncio
    async def test_user_account_membership_is_invalid(
        self,
        app: FastAPI,
        client: AsyncClient,
        test_user: UserInDB,
        test_account: AccountInDB,
    ) -> None:

        with pytest.raises(HTTPException):
            get_current_user_account_membership(
                current_user=test_user, account_id=test_account.id
            )
