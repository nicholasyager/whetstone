import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from starlette.status import (
    HTTP_200_OK,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
    HTTP_201_CREATED,
)

from whetstone_api.schemas.accounts import AccountInDB
from whetstone_api.schemas.projects import ProjectInDB
from whetstone_api.schemas.response import Response


class TestProjectsRoutes:
    @pytest.mark.asyncio
    async def test_create_routes_exist(
        self, app: FastAPI, disable_auth, client: AsyncClient
    ) -> None:
        res = await client.post(
            app.url_path_for("create_project", account_id="1"), json={}
        )
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_list_route_exists(
        self, app: FastAPI, disable_auth, client: AsyncClient
    ) -> None:
        res = await client.get(app.url_path_for("list_projects", account_id="1"))
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_route_exists(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_project: ProjectInDB
    ) -> None:
        res = await client.get(
            app.url_path_for(
                "get_project",
                account_id=str(test_project.account_id),
                project_id=str(test_project.id),
            )
        )
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_update_route_exists(
        self, app: FastAPI, disable_auth, client: AsyncClient
    ) -> None:
        res = await client.put(
            app.url_path_for("update_project", account_id="1", project_id="1")
        )
        assert res.status_code != HTTP_404_NOT_FOUND


class TestGetProject:
    @pytest.mark.asyncio
    async def test_get_project_by_id(
        self, app: FastAPI, client: AsyncClient, disable_auth, test_project: ProjectInDB
    ) -> None:
        res = await client.get(
            app.url_path_for(
                "get_project",
                account_id=str(test_project.account_id),
                project_id=str(test_project.id),
            )
        )
        assert res.status_code == HTTP_200_OK
        response: Response[ProjectInDB] = Response[ProjectInDB](**res.json())
        project: ProjectInDB = response.data
        assert project == test_project

    @pytest.mark.asyncio
    async def test_get_project_by_invalid_id_returns_404(
        self, app: FastAPI, client: AsyncClient, disable_auth, test_project: ProjectInDB
    ) -> None:
        res = await client.get(
            app.url_path_for(
                "get_project",
                account_id=str(test_project.account_id),
                project_id="9544",
            )
        )

        assert res.status_code == HTTP_404_NOT_FOUND


class TestCreateProjects:
    @pytest.mark.asyncio
    async def test_invalid_input_raises_error(
        self,
        app: FastAPI,
        client: AsyncClient,
        disable_auth,
        test_account: AccountInDB,
    ) -> None:
        res = await client.post(
            app.url_path_for("create_project", account_id=str(test_account.id)), json={}
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_invalid_status_raises_error(
        self,
        app: FastAPI,
        client: AsyncClient,
        disable_auth,
        test_account: AccountInDB,
    ) -> None:
        res = await client.post(
            app.url_path_for("create_project", account_id=str(test_account.id)),
            json={"name": "Test Account", "status": "pending"},
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_valid_input_succeeds(
        self,
        app: FastAPI,
        client: AsyncClient,
        disable_auth,
        test_account: AccountInDB,
    ) -> None:
        res = await client.post(
            app.url_path_for("create_project", account_id=str(test_account.id)),
            json={"name": "Test Project"},
        )
        assert res.status_code == HTTP_201_CREATED
