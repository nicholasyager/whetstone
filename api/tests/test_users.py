import pytest
from databases import Database
from fastapi import FastAPI
from httpx import AsyncClient
from starlette.status import HTTP_201_CREATED

from whetstone_api.repositories.users import UserRepository
from whetstone_api.schemas.response import Response
from whetstone_api.schemas.users import User


class TestUserRegistration:
    @pytest.mark.asyncio
    async def test_users_can_register_successfully(
        self,
        app: FastAPI,
        client: AsyncClient,
        db: Database,
    ) -> None:
        user_repo = UserRepository(db)
        new_user = {"email": "shakira@shakira.io", "password": "chantaje"}

        # make sure user doesn't exist yet
        user_in_db = await user_repo.get_by_email(email=new_user["email"])
        assert user_in_db is None

        # send post request to create user and ensure it is successful
        res = await client.post(app.url_path_for("register_new_user"), json=new_user)
        assert res.status_code == HTTP_201_CREATED

        # ensure that the user now exists in the db
        user_in_db = await user_repo.get_by_email(email=new_user["email"])
        assert user_in_db is not None
        assert user_in_db.email == new_user["email"]

        # check that the user returned in the response is equal to the user in the database
        json_response = res.json()
        json_response["data"]["hashed_password"] = "whatever"
        response = Response[User](**json_response)
        created_user = response.data

        assert created_user == User(**user_in_db.dict())

    @pytest.mark.asyncio
    @pytest.mark.parametrize(
        "attr, value, status_code",
        (
            ("email", "shakira@shakira.io", 400),
            ("email", "invalid_email@one@two.io", 422),
            ("password", "short", 422),
        ),
    )
    async def test_user_registration_fails_when_credentials_are_taken(
        self,
        app: FastAPI,
        client: AsyncClient,
        db: Database,
        attr: str,
        value: str,
        status_code: int,
    ) -> None:
        new_user = {"email": "nottaken@email.io", "password": "freepassword"}
        new_user[attr] = value
        res = await client.post(app.url_path_for("register_new_user"), json=new_user)
        assert res.status_code == status_code
