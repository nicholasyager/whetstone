import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from starlette.status import (
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
    HTTP_201_CREATED,
    HTTP_200_OK,
    HTTP_403_FORBIDDEN,
    HTTP_401_UNAUTHORIZED,
)

from whetstone_api.schemas.users import UserInDB


class TestAccountsRoutes:
    @pytest.mark.asyncio
    async def test_create_route_exists(
        self, app: FastAPI, superuser_client: AsyncClient
    ) -> None:
        res = await superuser_client.post(app.url_path_for("create_account"), json={})
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_list_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.get(app.url_path_for("list_accounts"))
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.get(app.url_path_for("get_account", account_id="1"))
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_update_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.put(app.url_path_for("update_account", account_id="1"))
        assert res.status_code != HTTP_404_NOT_FOUND


class TestGetAccounts:
    @pytest.mark.asyncio
    async def test_get_account_success(
        self, app: FastAPI, authorized_client: AsyncClient, test_user: UserInDB
    ):
        """Verify that a user can fetch the account that they are a part of."""
        res = await authorized_client.get(
            app.url_path_for("get_account", account_id=str(test_user.accounts[0].id))
        )
        assert res.status_code == HTTP_200_OK

    @pytest.mark.asyncio
    async def test_get_account_unauthorized_failure(
        self, app: FastAPI, authorized_client: AsyncClient
    ):
        """Verify that a user can fetch the account that they are a part of."""
        res = await authorized_client.get(
            app.url_path_for("get_account", account_id=str(9999))
        )
        assert res.status_code == HTTP_403_FORBIDDEN


class TestCreateAccounts:
    @pytest.mark.asyncio
    async def test_invalid_input_raises_error(
        self, app: FastAPI, superuser_client: AsyncClient
    ) -> None:
        res = await superuser_client.post(app.url_path_for("create_account"), json={})
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_invalid_plan_raises_error(
        self, app: FastAPI, superuser_client: AsyncClient
    ) -> None:
        res = await superuser_client.post(
            app.url_path_for("create_account"),
            json={"name": "Test Account", "plan": "free"},
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_valid_input_succeeds(
        self, app: FastAPI, superuser_client: AsyncClient
    ) -> None:
        res = await superuser_client.post(
            app.url_path_for("create_account"),
            json={"name": "Test Account", "plan": "developer"},
        )
        assert res.status_code == HTTP_201_CREATED

    @pytest.mark.asyncio
    async def test_unauthenticated_usage_fails(
        self, app: FastAPI, authorized_client: AsyncClient
    ) -> None:
        res = await authorized_client.post(
            app.url_path_for("create_account"),
            json={"name": "Test Account", "plan": "developer"},
        )
        assert res.status_code == HTTP_401_UNAUTHORIZED
