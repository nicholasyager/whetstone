from typing import Tuple, List

import uuid
import warnings
import os
import pytest_asyncio
from asgi_lifespan import LifespanManager
from fastapi import FastAPI
from httpx import AsyncClient
from databases import Database
import alembic
from alembic.config import Config

# Apply migrations at beginning and end of testing session
from whetstone_api import config
from whetstone_api.dependencies.auth import get_current_user_account_membership
from whetstone_api.repositories.accounts import AccountRepository
from whetstone_api.repositories.demo.runs import DemoRunRepository
from whetstone_api.repositories.projects import ProjectRepository
from whetstone_api.repositories.result_groups import ResultGroupsRepository
from whetstone_api.repositories.runs import RunRepository
from whetstone_api.repositories.users import UserRepository

from whetstone_api.schemas.accounts import (
    AccountInDB,
    AccountCreate,
    AccountStatus,
    AccountPlan,
    Account,
)
from whetstone_api.schemas.demo.runs import DemoRunCreate, DemoRunInDB
from whetstone_api.schemas.projects import ProjectInDB, ProjectCreate, ProjectStatus
from whetstone_api.schemas.result_groups import ResultGroupCreate, ResultGroupInDB
from whetstone_api.schemas.runs import RunCreate, RunInDB
from whetstone_api.schemas.results import ResultCreate, ResultInDB, ResultSeverity
from whetstone_api.schemas.users import UserCreate, UserInDB
from whetstone_api.services import auth_service


@pytest_asyncio.fixture(scope="session")
def apply_migrations():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    os.environ["TESTING"] = "1"
    config = Config("alembic.ini")
    alembic.command.upgrade(config, "head")
    yield
    alembic.command.downgrade(config, "base")


# Create a new application for testing
@pytest_asyncio.fixture
def app(apply_migrations: None) -> FastAPI:
    from whetstone_api.main import get_application

    return get_application()


# Grab a reference to our database when needed
@pytest_asyncio.fixture
async def db(app: FastAPI) -> Database:
    return app.state.db


# Make requests in our tests
@pytest_asyncio.fixture
async def client(app: FastAPI) -> AsyncClient:
    async with LifespanManager(app):
        async with AsyncClient(
            app=app,
            base_url="http://testserver",
            headers={"Content-Type": "application/json"},
        ) as client:
            yield client


# Model Fixtures
@pytest_asyncio.fixture
async def test_account(db: Database) -> AccountInDB:
    account_repo = AccountRepository(db)
    new_account = AccountCreate(
        name="Test Account", plan=AccountPlan.developer, status=AccountStatus.active
    )

    return await account_repo.create(new_account=new_account)


@pytest_asyncio.fixture
async def test_project(db: Database, test_account: Account) -> ProjectInDB:
    project_repo = ProjectRepository(db)
    new_project = ProjectCreate(name="Test Project", status=ProjectStatus.active)

    return await project_repo.create(
        account_id=test_account.id, new_project=new_project
    )


@pytest_asyncio.fixture
async def test_run(
    db: Database, test_account: Account, test_project: ProjectInDB
) -> RunInDB:
    repo = RunRepository(db)
    new_run = RunCreate(
        project_id=test_project.id, manifest={"foo": "bar"}, run_result={"baz": "bat"}
    )

    return await repo.create(
        account_id=test_account.id,
        new_run=new_run,
        manifest_location="testing",
        run_results_location="testing",
    )


@pytest_asyncio.fixture
async def test_demo_run(db: Database) -> DemoRunInDB:
    repo = DemoRunRepository(db)
    new_run = DemoRunCreate(
        correlation_id=uuid.uuid4(), manifest={"foo": "bar"}, run_result={"baz": "bat"}
    )

    return await repo.create(
        new_run=new_run,
        manifest_location="testing",
        run_results_location="testing",
    )


@pytest_asyncio.fixture
async def test_result(
    db: Database, test_account: Account, test_run: RunInDB
) -> Tuple[ResultGroupInDB, List[ResultInDB]]:
    result_group_repo = ResultGroupsRepository(db)

    new_result = ResultCreate(
        summary="summary",
        severity=ResultSeverity.low,
        rule_set="TestingSet",
        content={"name": "It's a name!"},
        model="test.model",
    )

    new_result_group = ResultGroupCreate(
        entity="test.model",
        entity_type="model",
        entity_id="test.model",
        content={"query": "FooBar", "subgraph": {"key": "value"}},
        results=[new_result],
    )

    return (
        await result_group_repo.create(
            account_id=test_account.id,
            run_id=test_run.id,
            new_result_groups=[new_result_group],
        )
    )[0]


@pytest_asyncio.fixture
async def test_user(db: Database, test_account: AccountInDB) -> UserInDB:
    new_user = UserCreate(
        email="david.bowie@rockers.io",
        password="starchild",
    )
    user_repo = UserRepository(db)
    existing_user = await user_repo.get_by_email(email=new_user.email)

    if existing_user:
        return existing_user

    user = await user_repo.register_new_user(new_user=new_user)

    return await user_repo.add_user_to_account(
        user_id=user.id, account_id=test_account.id
    )


@pytest_asyncio.fixture
def authorized_client(client: AsyncClient, test_user: UserInDB) -> AsyncClient:
    access_token = auth_service.create_access_token_for_user(
        user=test_user,
        secret_key=str(config.SECRET_KEY),
        audience=config.JWT_AUDIENCE,
        algorithms=[config.JWT_ALGORITHM],
    )
    client.headers = {
        **client.headers,
        "Authorization": f"{config.JWT_TOKEN_PREFIX} {access_token}",
    }
    return client


@pytest_asyncio.fixture
def superuser_client(client: AsyncClient) -> AsyncClient:
    client.headers = {
        **client.headers,
        "X-API-KEY": str(config.API_KEY),
    }
    return client


async def override_get_current_user_account_membership():
    """Disable account membership security function for testing"""
    pass


@pytest_asyncio.fixture
def disable_auth(app: FastAPI):
    """Disable account membership security dependency for testing"""
    app.dependency_overrides[
        get_current_user_account_membership
    ] = override_get_current_user_account_membership
