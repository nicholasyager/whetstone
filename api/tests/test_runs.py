import datetime
from typing import List

import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from starlette.status import (
    HTTP_200_OK,
    HTTP_404_NOT_FOUND,
    HTTP_422_UNPROCESSABLE_ENTITY,
    HTTP_201_CREATED,
)

from whetstone_api.schemas.accounts import AccountInDB
from whetstone_api.schemas.projects import ProjectInDB
from whetstone_api.schemas.response import Response
from whetstone_api.schemas.runs import RunInDB, RunStatus, Run


class TestRunsRoutes:
    @pytest.mark.asyncio
    async def test_create_routes_exist(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.post(app.url_path_for("create_run", account_id="1"), json={})
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_list_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.get(app.url_path_for("list_runs", account_id="1"))
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.get(app.url_path_for("get_run", account_id="1", run_id="1"))
        assert res.status_code != HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_update_route_exists(self, app: FastAPI, client: AsyncClient) -> None:
        res = await client.put(
            app.url_path_for("update_run", account_id="1", run_id="1")
        )
        assert res.status_code != HTTP_404_NOT_FOUND


class TestGetRun:
    @pytest.mark.asyncio
    async def test_get_run_by_id(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        res = await client.get(
            app.url_path_for(
                "get_run", account_id=str(test_run.account_id), run_id=str(test_run.id)
            )
        )
        assert res.status_code == HTTP_200_OK
        response: Response[Run] = Response[Run](**res.json())
        run: Run = response.data
        assert run.id == test_run.id

    @pytest.mark.asyncio
    async def test_get_run_by_invalid_id_returns_404(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        res = await client.get(
            app.url_path_for(
                "get_run", account_id=str(test_run.account_id), run_id="9544"
            )
        )

        assert res.status_code == HTTP_404_NOT_FOUND


class TestUpdateRuns:
    @pytest.mark.asyncio
    async def test_updates(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        dequeued_at = datetime.datetime(
            year=2022, month=1, day=1, tzinfo=datetime.timezone.utc
        )

        res = await client.put(
            app.url_path_for(
                "update_run",
                account_id=str(test_run.account_id),
                run_id=str(test_run.id),
            ),
            json={"status": "starting", "dequeued_at": dequeued_at.isoformat()},
        )
        assert res.status_code == HTTP_200_OK

        response: Response[Run] = Response[Run](**res.json())
        run: Run = response.data
        assert run.dequeued_at == dequeued_at
        assert run.status == RunStatus.starting

    @pytest.mark.asyncio
    async def test_multiple_updates(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        dequeued_at = datetime.datetime(
            year=2022, month=1, day=1, tzinfo=datetime.timezone.utc
        )
        started_at = dequeued_at + datetime.timedelta(seconds=10)

        res = await client.put(
            app.url_path_for(
                "update_run",
                account_id=str(test_run.account_id),
                run_id=str(test_run.id),
            ),
            json={"status": "starting", "dequeued_at": dequeued_at.isoformat()},
        )
        assert res.status_code == HTTP_200_OK

        res = await client.put(
            app.url_path_for(
                "update_run",
                account_id=str(test_run.account_id),
                run_id=str(test_run.id),
            ),
            json={
                "status": "running",
                "dequeued_at": None,
                "started_at": started_at.isoformat(),
            },
        )
        assert res.status_code == HTTP_200_OK

        response: Response[Run] = Response[Run](**res.json())
        run: Run = response.data
        assert run.dequeued_at is None
        assert run.started_at == started_at
        assert run.status == RunStatus.running

    @pytest.mark.asyncio
    async def test_partial_updates(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        dequeued_at = datetime.datetime(
            year=2022, month=1, day=1, tzinfo=datetime.timezone.utc
        )
        started_at = dequeued_at + datetime.timedelta(seconds=10)

        res = await client.patch(
            app.url_path_for(
                "partial_update_run",
                account_id=str(test_run.account_id),
                run_id=str(test_run.id),
            ),
            json={"status": "starting", "dequeued_at": dequeued_at.isoformat()},
        )
        assert res.status_code == HTTP_200_OK

        res = await client.patch(
            app.url_path_for(
                "partial_update_run",
                account_id=str(test_run.account_id),
                run_id=str(test_run.id),
            ),
            json={"status": "running", "started_at": started_at.isoformat()},
        )
        assert res.status_code == HTTP_200_OK

        response: Response[Run] = Response[Run](**res.json())
        run: Run = response.data
        assert run.dequeued_at == dequeued_at
        assert run.started_at == started_at
        assert run.status == RunStatus.running


class TestListRuns:
    @pytest.mark.asyncio
    async def test_list_runs_by_account_id(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        res = await client.get(
            app.url_path_for("list_runs", account_id=str(test_run.account_id))
        )
        assert res.status_code == HTTP_200_OK
        response = Response[List[Run]](**res.json())
        runs: List[Run] = response.data
        assert len(runs) > 0
        assert test_run.id in set([run.id for run in runs])

    @pytest.mark.asyncio
    async def test_list_runs_by_project_id(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        res = await client.get(
            app.url_path_for("list_runs", account_id=str(test_run.account_id)),
            params={"project_id": 0},
        )
        assert res.status_code == HTTP_200_OK
        response = Response[List[Run]](**res.json())
        run_list = response.data
        assert len(run_list) == 0


class TestCreateRuns:
    @pytest.mark.asyncio
    async def test_invalid_input_raises_error(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_account: AccountInDB,
    ) -> None:
        res = await client.post(
            app.url_path_for("create_run", account_id=str(test_account.id)), json={}
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_invalid_project_raises_error(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_account: AccountInDB
    ) -> None:
        res = await client.post(
            app.url_path_for("create_run", account_id=str(test_account.id)),
            json={
                "project_id": 9999,
                "manifest": {"foo": "bar"},
                "run_result": {"foo": "bar"},
            },
        )
        assert res.status_code == HTTP_422_UNPROCESSABLE_ENTITY

    @pytest.mark.asyncio
    async def test_valid_input_succeeds(
        self,
        app: FastAPI,
        disable_auth,
        client: AsyncClient,
        test_account: AccountInDB,
        test_project: ProjectInDB,
    ) -> None:
        res = await client.post(
            app.url_path_for("create_run", account_id=str(test_account.id)),
            json={
                "project_id": test_project.id,
                "manifest": {"foo": "bar"},
                "run_result": {"foo": "bar"},
            },
        )

        assert res.status_code == HTTP_201_CREATED


class TestRetryRuns:
    @pytest.mark.asyncio
    async def test_retry_run_by_id(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        res = await client.post(
            app.url_path_for(
                "retry_run",
                account_id=str(test_run.account_id),
                run_id=str(test_run.id),
            )
        )
        assert res.status_code == HTTP_201_CREATED
        response: Response[Run] = Response[Run](**res.json())
        run: Run = response.data
        assert run.id != test_run.id
        assert run.manifest_location == test_run.manifest_location
        assert run.run_results_location == test_run.run_results_location

    @pytest.mark.asyncio
    async def test_retry_run_by_invalid_id_returns_404(
        self, app: FastAPI, disable_auth, client: AsyncClient, test_run: RunInDB
    ) -> None:
        res = await client.post(
            app.url_path_for(
                "retry_run", account_id=str(test_run.account_id), run_id="9544"
            )
        )

        assert res.status_code == HTTP_404_NOT_FOUND
