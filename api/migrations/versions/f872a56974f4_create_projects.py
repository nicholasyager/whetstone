"""create_projects
Revision ID: f872a56974f4
Revises: 67eb5802b4de
Create Date: 2022-03-22 03:28:07.674811
"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic

revision = "f872a56974f4"
down_revision = "67eb5802b4de"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "projects",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.Text, nullable=False),
        sa.Column(
            "account_id", sa.Integer, sa.ForeignKey("accounts.id"), nullable=False
        ),
        sa.Column("status", sa.Text, nullable=False, server_default="active"),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
    )

    # Auto-update account updated_at fields
    op.execute(
        """
        CREATE TRIGGER update_projects_modtime
            BEFORE UPDATE
            ON projects
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )


def downgrade() -> None:
    op.drop_table("projects")
