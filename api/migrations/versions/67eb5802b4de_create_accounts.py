"""create_accounts
Revision ID: 67eb5802b4de
Revises:
Create Date: 2022-03-21 18:49:58.344655
"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic
revision = "67eb5802b4de"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    # Make the DB auto-update updated_at fields.
    op.execute(
        """
        CREATE OR REPLACE FUNCTION update_updated_at_column()
            RETURNS TRIGGER AS
        $$
        BEGIN
            NEW.updated_at = now();
            RETURN NEW;
        END;
        $$ language 'plpgsql';
        """
    )

    op.create_table(
        "accounts",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.Text, nullable=False),
        sa.Column("status", sa.Text, nullable=False, server_default="pending"),
        sa.Column("plan", sa.Text, nullable=False),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
    )

    # Auto-update account updated_at fields
    op.execute(
        """
        CREATE TRIGGER update_accounts_modtime
            BEFORE UPDATE
            ON accounts
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )


def downgrade() -> None:
    op.drop_table("accounts")
    op.execute("DROP FUNCTION update_updated_at_column")
