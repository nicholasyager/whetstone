"""runs_get_metadata
Revision ID: 20ac7bc28639
Revises: 3c1d42c9c558
Create Date: 2022-11-23 21:55:03.339530
"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic
revision = "20ac7bc28639"
down_revision = "3c1d42c9c558"
branch_labels = None
depends_on = None


def upgrade() -> None:

    op.add_column(
        "runs",
        sa.Column("metadata", sa.JSON, nullable=True),
    )

    op.add_column(
        "demo_runs",
        sa.Column("metadata", sa.JSON, nullable=True),
    )


def downgrade() -> None:
    op.drop_column("demo_runs", "metadata")
    op.drop_column("runs", "metadata")
