"""add_run_statistics
Revision ID: c168daf34440
Revises: 91e633f2465b
Create Date: 2023-02-26 16:01:31.590816
"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic
revision = "c168daf34440"
down_revision = "91e633f2465b"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "run_statistics",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("run_id", sa.Text, nullable=False),
        sa.Column("label", sa.Text, nullable=False),
        sa.Column("statistic", sa.Text, nullable=False),
        sa.Column("value", sa.Float, nullable=False),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
    )

    op.execute(
        """
        CREATE TRIGGER update_run_statistics_modtime
            BEFORE UPDATE
            ON run_statistics
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )


def downgrade() -> None:
    op.drop_table("run_statistics")
