"""add_severity_to_result_groups
Revision ID: 91e633f2465b
Revises: 561d84ff52e6
Create Date: 2023-02-21 08:44:42.619374
"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic
revision = "91e633f2465b"
down_revision = "561d84ff52e6"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "result_groups",
        sa.Column("severity_score", sa.Integer, nullable=False, server_default="0"),
    )
    op.add_column(
        "demo_result_groups",
        sa.Column("severity_score", sa.Integer, nullable=False, server_default="0"),
    )
    op.execute(
        """
        with

        run_results as (
            select
                *,
                case
                    when severity = 'low' then 1
                    when severity = 'medium' then 10
                    when severity = 'high' then 100
                end as severity_score
            from results
        ),

        sort_order as (
            select
                result_group_id,
                sum(severity_score) as total_severity
            from run_results
            group by 1
        )


        update result_groups
        set
            severity_score = sort_order.total_severity
        from sort_order
        where result_groups.id = sort_order.result_group_id;
        """
    )

    op.execute(
        """
        with

        run_results as (
            select
                *,
                case
                    when severity = 'low' then 1
                    when severity = 'medium' then 10
                    when severity = 'high' then 100
                end as severity_score
            from demo_results
        ),

        sort_order as (
            select
                demo_result_group_id,
                sum(severity_score) as total_severity
            from run_results
            group by 1
        )


        update demo_result_groups
        set
            severity_score = sort_order.total_severity
        from sort_order
        where demo_result_groups.id = sort_order.demo_result_group_id;
        """
    )


def downgrade() -> None:
    op.drop_column("result_groups", "severity_score")
    op.drop_column("demo_result_groups", "severity_score")
