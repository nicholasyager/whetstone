"""create_users
Revision ID: 7d52b077dbc2
Revises: a2adc720f647
Create Date: 2022-03-29 01:06:56.666614
"""

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic
revision = "7d52b077dbc2"
down_revision = "a2adc720f647"
branch_labels = None
depends_on = None


def upgrade() -> None:

    op.execute(
        """
        CREATE EXTENSION IF NOT EXISTS pgcrypto;
        """
    )

    op.create_table(
        "users",
        sa.Column(
            "id", UUID, primary_key=True, server_default=sa.func.gen_random_uuid()
        ),
        sa.Column("email", sa.Text, nullable=False, unique=True, index=True),
        sa.Column("hashed_password", sa.Text, nullable=True),
        sa.Column("status", sa.Text, nullable=False, server_default="active"),
        sa.Column("is_verified", sa.Boolean, nullable=False, server_default="false"),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
    )

    op.execute(
        """
        CREATE TRIGGER update_users_modtime
            BEFORE UPDATE
            ON users
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )

    op.create_table(
        "user_accounts",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("user_id", UUID, sa.ForeignKey("users.id"), nullable=False),
        sa.Column(
            "account_id", sa.Integer, sa.ForeignKey("accounts.id"), nullable=False
        ),
        sa.Column("is_active", sa.Boolean, nullable=False, server_default="true"),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
    )

    op.execute(
        """
        CREATE TRIGGER update_user_accounts_modtime
            BEFORE UPDATE
            ON user_accounts
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )


def downgrade() -> None:
    op.drop_table("user_accounts")
    op.drop_table("users")
