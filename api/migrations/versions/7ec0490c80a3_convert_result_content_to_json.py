"""convert_result_content_to_json
Revision ID: 7ec0490c80a3
Revises: bf08cd6ec957
Create Date: 2022-04-24 00:50:07.846497
"""

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB

# revision identifiers, used by Alembic
revision = "7ec0490c80a3"
down_revision = "bf08cd6ec957"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.drop_column("results", "content")
    op.add_column("results", sa.Column("content", JSONB, nullable=True))


def downgrade() -> None:
    op.alter_column("results", "content", type_=sa.text)
