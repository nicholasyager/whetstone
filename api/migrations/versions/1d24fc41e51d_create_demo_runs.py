"""create_demo_runs
Revision ID: 1d24fc41e51d
Revises: 7ec0490c80a3
Create Date: 2022-05-22 23:35:23.926346
"""

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic
revision = "1d24fc41e51d"
down_revision = "7ec0490c80a3"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "demo_runs",
        sa.Column("id", UUID, primary_key=True),
        sa.Column("correlation_id", UUID, nullable=False),
        sa.Column("status", sa.Text, nullable=False, server_default="queued"),
        sa.Column("manifest_location", sa.Text, nullable=False),
        sa.Column("run_results_location", sa.Text, nullable=True),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "dequeued_at",
            sa.TIMESTAMP(timezone=True),
            nullable=True,
            index=False,
        ),
        sa.Column(
            "started_at",
            sa.TIMESTAMP(timezone=True),
            nullable=True,
            index=False,
        ),
        sa.Column(
            "finished_at",
            sa.TIMESTAMP(timezone=True),
            nullable=True,
            index=False,
        ),
    )

    # Auto-update account updated_at fields
    op.execute(
        """
        CREATE TRIGGER update_demo_runs_modtime
            BEFORE UPDATE
            ON demo_runs
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )


def downgrade() -> None:
    op.drop_table("demo_runs")
