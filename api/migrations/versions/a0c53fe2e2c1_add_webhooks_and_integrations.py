"""add_webhooks_and_integrations
Revision ID: a0c53fe2e2c1
Revises: 84ab4d0b81b1
Create Date: 2023-04-03 20:11:40.233849
"""
from sqlalchemy.dialects.postgresql import JSONB, UUID

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic
revision = "a0c53fe2e2c1"
down_revision = "84ab4d0b81b1"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "webhooks",
        sa.Column("id", UUID, primary_key=True),
        sa.Column(
            "account_id", sa.Integer, sa.ForeignKey("accounts.id"), nullable=False
        ),
        sa.Column(
            "project_id", sa.Integer, sa.ForeignKey("projects.id"), nullable=False
        ),
        sa.Column("system", sa.Text, nullable=False),
        sa.Column("authorization", sa.Text, nullable=False),
        sa.Column("credential", JSONB, nullable=False),
        sa.Column("is_active", sa.Boolean, nullable=False, server_default="false"),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
    )

    op.execute(
        """
        CREATE TRIGGER update_webhooks_modtime
            BEFORE UPDATE
            ON webhooks
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )


def downgrade() -> None:
    op.drop_table("webhooks")
