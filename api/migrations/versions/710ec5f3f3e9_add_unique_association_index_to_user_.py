"""add_unique_association_index_to_user_accounts
Revision ID: 710ec5f3f3e9
Revises: 7d52b077dbc2
Create Date: 2022-03-30 00:10:51.328347
"""

from alembic import op

# revision identifiers, used by Alembic
revision = "710ec5f3f3e9"
down_revision = "7d52b077dbc2"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_unique_constraint(
        constraint_name="unique_associations",
        table_name="user_accounts",
        columns=["user_id", "account_id"],
    )
    op.create_index(
        index_name="users_idx", table_name="user_accounts", columns=["user_id"]
    )


def downgrade() -> None:
    op.drop_constraint(
        constraint_name="unique_associations", table_name="user_accounts"
    )

    op.drop_index(index_name="users_idx", table_name="user_accounts")
