"""Populate ResultGroups and remove unnecessary fields
Revision ID: 561d84ff52e6
Revises: ab56d3f301fb
Create Date: 2023-02-20 22:54:52.359776
"""

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic
revision = "561d84ff52e6"
down_revision = "ab56d3f301fb"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute(
        """
    with

    identified_result_groups as (

        select
            distinct on (run_id, account_id, model)
            run_id,
            account_id,
            model as entity,
            model as entity_id,
            'model' as entity_type,
            jsonb_build_object(
                   'query', ("content" ->> 'query'),
                   'subgraph', "content" -> 'subgraph'
            ) as content,
            created_at,
            updated_at
        from results
        order by run_id, account_id, model, updated_at desc

    ),

    result_counts as (
        select
            run_id,
            account_id,
            model,
            count(*)
        from results
        group by 1, 2, 3
    )

    insert into result_groups
    (run_id, account_id, entity, entity_id, entity_type, result_count, content, created_at, updated_at)
    select
        identified_result_groups.run_id,
        identified_result_groups.account_id,
        identified_result_groups.entity,
        identified_result_groups.entity_id,
        identified_result_groups.entity_type,
        result_counts.count,
        identified_result_groups.content,
        identified_result_groups.created_at,
        identified_result_groups.updated_at

    from identified_result_groups
    left join result_counts on
        identified_result_groups.run_id = result_counts.run_id
        and identified_result_groups.account_id = result_counts.account_id
        and identified_result_groups.entity = result_counts.model;
    """
    )

    op.execute(
        """
       with

       identified_result_groups as (

           select
               distinct on (demo_run_id, correlation_id, model)
               demo_run_id,
               correlation_id,
               model as entity,
               model as entity_id,
               'model' as entity_type,
               jsonb_build_object(
                   'query', ("content" ->> 'query'),
                   'subgraph', "content" -> 'subgraph'
               ) as content,
               created_at,
               updated_at
           from demo_results
           order by demo_run_id, correlation_id, model, updated_at desc

       ),

       result_counts as (
           select
               demo_run_id,
               correlation_id,
               model,
               count(*)
           from demo_results
           group by 1, 2, 3
       )

       insert into demo_result_groups
       (id, demo_run_id, correlation_id, entity, entity_id, entity_type, result_count, content, created_at, updated_at)
       select
           gen_random_uuid() as id,
           identified_result_groups.demo_run_id,
           identified_result_groups.correlation_id,
           identified_result_groups.entity,
           identified_result_groups.entity_id,
           identified_result_groups.entity_type,
           result_counts.count,
           identified_result_groups.content,
           identified_result_groups.created_at,
           identified_result_groups.updated_at

       from identified_result_groups
       left join result_counts on
           identified_result_groups.demo_run_id = result_counts.demo_run_id
           and identified_result_groups.correlation_id = result_counts.correlation_id
           and identified_result_groups.entity = result_counts.model;
       """
    )

    op.add_column(
        "results",
        sa.Column(
            "result_group_id",
            sa.Integer,
            sa.ForeignKey("result_groups.id"),
            nullable=True,
            index=True,
        ),
    )

    op.add_column(
        "demo_results",
        sa.Column(
            "demo_result_group_id",
            UUID,
            sa.ForeignKey("demo_result_groups.id"),
            nullable=True,
            index=True,
        ),
    )

    op.execute(
        """
        UPDATE results
        SET
            result_group_id = result_groups.id
        FROM result_groups
        WHERE
            results.run_id = result_groups.run_id
            and results.account_id = result_groups.account_id
            and results.model = result_groups.entity;
        """
    )

    op.execute(
        """
        UPDATE demo_results
        SET demo_result_group_id = demo_result_groups.id
        FROM demo_result_groups
        WHERE
            demo_results.demo_run_id = demo_result_groups.demo_run_id
            and demo_results.correlation_id = demo_result_groups.correlation_id
            and demo_results.model = demo_result_groups.entity;
        """
    )


def downgrade() -> None:
    op.drop_column("results", "result_group_id")
    op.drop_column("demo_results", "demo_result_group_id")

    op.execute("truncate result_groups;")
    op.execute("truncate demo_result_groups;")
