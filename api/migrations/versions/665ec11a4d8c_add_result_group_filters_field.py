"""add_result_group_filters_field
Revision ID: 665ec11a4d8c
Revises: c168daf34440
Create Date: 2023-03-14 08:38:19.175356
"""

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB

# revision identifiers, used by Alembic
revision = "665ec11a4d8c"
down_revision = "c168daf34440"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "result_groups",
        sa.Column("filters", JSONB, nullable=False, server_default="[]"),
    )

    op.execute(
        """
        with

        distinct_results as (
            select distinct on (result_group_id, severity, rule_set)
                result_group_id,
                severity ,
                rule_set,
                json_build_object(
                    'severity', severity,
                    'rule_set', rule_set
                ) as filters
            from results
            order by 1, 2, 3
        ),

        filter_documents as (
            select
                result_group_id,
                json_agg(filters) as filters
            from distinct_results
            group by 1
        )

        update result_groups
        set
            filters = filter_documents.filters
        from filter_documents
        where filter_documents.result_group_id = result_groups.id
        """
    )

    op.add_column(
        "demo_result_groups",
        sa.Column("filters", JSONB, nullable=False, server_default="[]"),
    )

    op.execute(
        """
        with

        distinct_results as (
            select distinct on (demo_result_group_id, severity, rule_set)
                demo_result_group_id,
                severity ,
                rule_set,
                json_build_object(
                    'severity', severity,
                    'rule_set', rule_set
                ) as filters
            from demo_results
            order by 1, 2, 3
        ),

        filter_documents as (
            select
                demo_result_group_id,
                json_agg(filters) as filters
            from distinct_results
            group by 1
        )

        update demo_result_groups
        set
            filters = filter_documents.filters
        from filter_documents
        where filter_documents.demo_result_group_id = demo_result_groups.id
        """
    )


def downgrade() -> None:
    op.drop_column("result_groups", "filters")
    op.drop_column("demo_result_groups", "filters")
