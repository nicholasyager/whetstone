"""create_demo_results
Revision ID: 9cd6cee69894
Revises: 1d24fc41e51d
Create Date: 2022-05-24 13:11:52.843736
"""

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic
revision = "9cd6cee69894"
down_revision = "1d24fc41e51d"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "demo_results",
        sa.Column("id", UUID, primary_key=True),
        sa.Column("demo_run_id", UUID, sa.ForeignKey("demo_runs.id"), nullable=False),
        sa.Column("correlation_id", UUID, nullable=False),
        sa.Column("summary", sa.Text, nullable=False),
        sa.Column("severity", sa.Text, nullable=False),
        sa.Column("model", sa.Text, nullable=False),
        sa.Column("rule_set", sa.Text, nullable=False),
        sa.Column("content", sa.Text, nullable=True),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
    )
    op.execute(
        """
        CREATE TRIGGER update_demo_results_modtime
            BEFORE UPDATE
            ON demo_results
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )


def downgrade() -> None:
    op.drop_table("demo_results")
