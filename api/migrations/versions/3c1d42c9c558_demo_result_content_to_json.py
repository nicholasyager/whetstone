"""demo_result_content_to_json
Revision ID: 3c1d42c9c558
Revises: c7bf315dbef9
Create Date: 2022-06-21 23:40:33.857511
"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic
from sqlalchemy.dialects.postgresql import JSONB

revision = "3c1d42c9c558"
down_revision = "c7bf315dbef9"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.drop_column("demo_results", "content")
    op.add_column("demo_results", sa.Column("content", JSONB, nullable=True))


def downgrade() -> None:
    op.alter_column("demo_results", "content", type_=sa.text)
