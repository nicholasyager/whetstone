"""create_runs_and_results
Revision ID: 198924a5e617
Revises: f872a56974f4
Create Date: 2022-03-25 13:17:17.279956
"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic
revision = "198924a5e617"
down_revision = "f872a56974f4"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "runs",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column(
            "account_id", sa.Integer, sa.ForeignKey("accounts.id"), nullable=False
        ),
        sa.Column(
            "project_id", sa.Integer, sa.ForeignKey("projects.id"), nullable=False
        ),
        sa.Column("status", sa.Text, nullable=False, server_default="queued"),
        sa.Column("manifest_location", sa.Text, nullable=False),
        sa.Column("run_results_location", sa.Text, nullable=True),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "dequeued_at",
            sa.TIMESTAMP(timezone=True),
            nullable=True,
            index=False,
        ),
        sa.Column(
            "started_at",
            sa.TIMESTAMP(timezone=True),
            nullable=True,
            index=False,
        ),
        sa.Column(
            "finished_at",
            sa.TIMESTAMP(timezone=True),
            nullable=True,
            index=False,
        ),
    )

    # Auto-update account updated_at fields
    op.execute(
        """
        CREATE TRIGGER update_runs_modtime
            BEFORE UPDATE
            ON runs
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )

    op.create_table(
        "results",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("run_id", sa.Integer, sa.ForeignKey("runs.id"), nullable=False),
        sa.Column("summary", sa.Text, nullable=False),
        sa.Column("severity", sa.Text, nullable=False),
        sa.Column("model", sa.Text, nullable=False),
        sa.Column("rule_set", sa.Text, nullable=False),
        sa.Column("content", sa.Text, nullable=True),
        sa.Column(
            "created_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
        sa.Column(
            "updated_at",
            sa.TIMESTAMP(timezone=True),
            server_default=sa.func.now(),
            nullable=False,
            index=False,
        ),
    )
    op.execute(
        """
        CREATE TRIGGER update_results_modtime
            BEFORE UPDATE
            ON results
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )


def downgrade() -> None:
    op.drop_table("results")
    op.drop_table("runs")
