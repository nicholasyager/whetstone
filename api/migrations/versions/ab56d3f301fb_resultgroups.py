"""ResultGroups
Revision ID: ab56d3f301fb
Revises: 20ac7bc28639
Create Date: 2023-02-20 22:09:29.374546
"""
from sqlalchemy.dialects.postgresql import JSONB, UUID

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic
revision = "ab56d3f301fb"
down_revision = "20ac7bc28639"
branch_labels = None
depends_on = None

shared_columns = [
    sa.Column("entity", sa.Text, nullable=False),
    sa.Column("entity_type", sa.Text, nullable=False),
    sa.Column("entity_id", sa.Text, nullable=False),
    sa.Column("result_count", sa.Integer, nullable=False, server_default="0"),
    sa.Column("content", JSONB, nullable=True),
    sa.Column("is_stale", sa.Boolean, nullable=False, server_default="false"),
    sa.Column(
        "created_at",
        sa.TIMESTAMP(timezone=True),
        server_default=sa.func.now(),
        nullable=False,
        index=False,
    ),
    sa.Column(
        "updated_at",
        sa.TIMESTAMP(timezone=True),
        server_default=sa.func.now(),
        nullable=False,
        index=False,
    ),
]


def upgrade() -> None:
    op.create_table(
        "result_groups",
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("run_id", sa.Integer, sa.ForeignKey("runs.id"), nullable=False),
        sa.Column(
            "account_id", sa.Integer, sa.ForeignKey("accounts.id"), nullable=False
        ),
        *shared_columns
    )

    op.execute(
        """
        CREATE TRIGGER update_result_groups_modtime
            BEFORE UPDATE
            ON result_groups
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )

    op.create_table(
        "demo_result_groups",
        sa.Column("id", UUID, primary_key=True),
        sa.Column("demo_run_id", UUID, sa.ForeignKey("demo_runs.id"), nullable=False),
        sa.Column("correlation_id", UUID, nullable=False),
        *shared_columns
    )

    op.execute(
        """
        CREATE TRIGGER update_demo_result_groups_modtime
            BEFORE UPDATE
            ON demo_result_groups
            FOR EACH ROW
        EXECUTE PROCEDURE update_updated_at_column();
        """
    )


def downgrade() -> None:

    op.drop_table("result_groups")
    op.drop_table("demo_result_groups")
