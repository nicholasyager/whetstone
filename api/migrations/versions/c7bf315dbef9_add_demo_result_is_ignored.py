"""add_demo_result_is_ignored
Revision ID: c7bf315dbef9
Revises: cfc6dba868e5
Create Date: 2022-06-21 23:36:46.870104
"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic
revision = "c7bf315dbef9"
down_revision = "cfc6dba868e5"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "demo_results",
        sa.Column("is_ignored", sa.Boolean, nullable=False, server_default="false"),
    )


def downgrade() -> None:
    op.drop_column("demo_results", "is_ignored")
