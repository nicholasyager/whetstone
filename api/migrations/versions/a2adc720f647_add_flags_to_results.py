"""add_flags_to_results
Revision ID: a2adc720f647
Revises: c48795dd1e1f
Create Date: 2022-03-28 18:01:49.248975
"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic
revision = "a2adc720f647"
down_revision = "c48795dd1e1f"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "results",
        sa.Column("is_stale", sa.Boolean, nullable=False, server_default="false"),
    )
    op.add_column(
        "results",
        sa.Column("is_ignored", sa.Boolean, nullable=False, server_default="false"),
    )


def downgrade() -> None:
    op.drop_column("results", "is_stale")
    op.drop_column("results", "is_ignored")
