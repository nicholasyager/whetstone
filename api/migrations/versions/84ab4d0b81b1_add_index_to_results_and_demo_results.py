"""add_index_to_results_and_demo_results
Revision ID: 84ab4d0b81b1
Revises: 665ec11a4d8c
Create Date: 2023-03-21 20:11:53.533993
"""

from alembic import op

# revision identifiers, used by Alembic
revision = "84ab4d0b81b1"
down_revision = "665ec11a4d8c"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_index(
        index_name="result_group_entity_id_index",
        table_name="result_groups",
        columns=["entity_id"],
        postgresql_ops={"data": "text_pattern_ops"},
    )

    op.create_index(
        index_name="result_group_lookup_index",
        table_name="result_groups",
        columns=["account_id", "run_id"],
    )

    op.create_index(
        index_name="demo_result_group_entity_id_index",
        table_name="demo_result_groups",
        columns=["entity_id"],
        postgresql_ops={"data": "text_pattern_ops"},
    )

    op.create_index(
        index_name="demo_result_group_lookup_index",
        table_name="demo_result_groups",
        columns=["correlation_id"],
    )


def downgrade() -> None:
    op.drop_index("entity_id_index")
    op.drop_index("lookup_index")
    op.drop_index("entity_id_index")
    op.drop_index("demo_lookup_index")
