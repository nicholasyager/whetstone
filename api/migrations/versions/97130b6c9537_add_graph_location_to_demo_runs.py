"""add_graph_location_to_demo_runs
Revision ID: 97130b6c9537
Revises: 9cd6cee69894
Create Date: 2022-05-25 13:21:28.172039
"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic
revision = "97130b6c9537"
down_revision = "9cd6cee69894"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "demo_runs",
        sa.Column("graph_location", sa.Text, nullable=True),
    )


def downgrade() -> None:
    op.drop_column("demo_runs", "graph_location")
