"""add_graph_location_to_run
Revision ID: bf08cd6ec957
Revises: 710ec5f3f3e9
Create Date: 2022-04-03 17:29:42.409477
"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic
revision = "bf08cd6ec957"
down_revision = "710ec5f3f3e9"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "runs",
        sa.Column("graph_location", sa.Text, nullable=True),
    )


def downgrade() -> None:
    op.drop_column("runs", "graph_location")
