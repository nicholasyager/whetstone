"""add_demo_result_is_stale
Revision ID: cfc6dba868e5
Revises: 97130b6c9537
Create Date: 2022-06-21 22:40:58.193211
"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic
revision = "cfc6dba868e5"
down_revision = "97130b6c9537"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "demo_results",
        sa.Column("is_stale", sa.Boolean, nullable=False, server_default="false"),
    )


def downgrade() -> None:
    op.drop_column("demo_results", "is_stale")
