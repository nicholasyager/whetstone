"""add_account_id_to_results
Revision ID: c48795dd1e1f
Revises: 198924a5e617
Create Date: 2022-03-27 00:03:13.548287
"""

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic
revision = "c48795dd1e1f"
down_revision = "198924a5e617"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "results",
        sa.Column(
            "account_id", sa.Integer, sa.ForeignKey("accounts.id"), nullable=False
        ),
    )


def downgrade() -> None:
    op.drop_column("results", "account_id")
