#!/usr/bin/env python
import codecs
import os
import pathlib
from setuptools import setup, find_packages

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / "README.md").read_text(encoding="utf-8")


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), "r") as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith("__version__"):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


setup(
    name="whetstone_api",
    version=get_version("whetstone_api/__version__.py"),
    description="API Server for Whetstone",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Nicholas Yager",
    author_email="yager@nicholasyager.com",
    url="https://gitlab.com/nicholasyager/whetstone",
    packages=find_packages(),
    python_requires=">=3.6, <4",
)
