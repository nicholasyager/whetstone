export interface APIResponse<Type> {
  metadata: {
    pagination?: {
      total_count: number;
      count: number;
    };
  };
  data: Type;
}
