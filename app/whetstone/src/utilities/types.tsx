export interface ManifestFile {
  metadata: object;
  nodes: object;
  sources: object;
  macros: object;
  docs: object;
  exposures: object;
  metrics: object;
  selectors: object;
  disabled: object;
  parent_map: object;
  child_map: object;
}
