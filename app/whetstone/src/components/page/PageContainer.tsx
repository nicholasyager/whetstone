import styled from "styled-components";

export const PageContainer = styled.div`
  max-width: 70em;
  margin: 2em auto;
`;
