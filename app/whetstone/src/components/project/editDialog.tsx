import { useState } from "react";
import { useMutation, useQueryClient } from "react-query";

import { Project, updateProject } from "../../utilities/api/projects";
import { Button } from "../button/Button";
import { ButtonNav, Dialog } from "../dialog/dialog";

interface DeleteDialogProps {
  project: Project;
  open: boolean;
  setOpen: (open: boolean) => void;
}

export const EditDialog: React.FC<DeleteDialogProps> = ({
  project,
  open,
  setOpen,
}) => {
  const queryClient = useQueryClient();

  const [projectName, setProjectName] = useState(project.name);

  const mutation = useMutation(updateProject, {
    onSuccess: (data) => {
      // Invalidate and refetch
      queryClient.setQueryData(
        [
          "projects",
          {
            accountId: project.account_id.toString(),
            projectId: project.id.toString(),
          },
        ],
        data
      );
    },
  });

  const handleSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();

    mutation.mutate({
      accountId: project.account_id,
      projectId: project.id,
      projectUpdate: {
        name: projectName,
        status: "active",
      },
    });

    setOpen(false);
  };

  return (
    <Dialog open={open} setOpen={setOpen}>
      <h2>Edit Project</h2>
      <p>You can edit your project's settings below.</p>
      <form style={{ margin: "2em 1em 0 1em" }} onSubmit={handleSubmit}>
        <label htmlFor="projectName">
          Name:{" "}
          <input
            id="projectName"
            type="text"
            placeholder="Project Name"
            value={projectName}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setProjectName(e.target.value)
            }
            onSubmitCapture={() => {}}
          />
        </label>

        <ButtonNav>
          <Button
            label="Cancel"
            primary={false}
            onClick={() => {
              setOpen(false);
            }}
            type="button"
          />
          <Button label="Submit" primary={true} type="submit" />
        </ButtonNav>
      </form>
    </Dialog>
  );
};
