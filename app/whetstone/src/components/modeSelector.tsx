import React, { ReactNode } from 'react';
import styled from 'styled-components';

interface SelectorProps {
  children?: ReactNode;
}

interface ItemProps {
  id?: string;
  label: string;
  name: string;
  onChange: any;
  checked: boolean;
}

const SelectorContainer = styled.form`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 0 auto;
`;

const SelectorItem = styled.div`
  text-align: center;
  margin: 0 1em;
`;

export const ModeSelector: React.FC<SelectorProps> = (props) => {
  return <SelectorContainer>{props.children}</SelectorContainer>;
};

export const ModeSelectorItem: React.FC<ItemProps> = (props) => {
  return (
    <SelectorItem>
      <input
        type="radio"
        name={props.name}
        id={props.id}
        onChange={props.onChange}
        checked={props.checked}
      />
      <label htmlFor={props.id}>{props.label}</label>
    </SelectorItem>
  );
};
