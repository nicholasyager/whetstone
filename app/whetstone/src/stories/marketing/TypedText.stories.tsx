import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { TypedText } from "../../components/marketing/typedText";

export default {
  title: "Marketing/TypedText",
  component: TypedText,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: "fullscreen",
  },
} as ComponentMeta<typeof TypedText>;

const Template: ComponentStory<typeof TypedText> = (args) => (
  <TypedText {...args}></TypedText>
);

export const WordList = Template.bind({});
WordList.args = {
  wordList: ["foo", "bar", "baz"],
};
