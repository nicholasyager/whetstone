import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { LandingSection } from "../../components/marketing/landing";

export default {
  title: "Marketing/LandingSection",
  component: LandingSection,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: "fullscreen",
  },
} as ComponentMeta<typeof LandingSection>;

const Template: ComponentStory<typeof LandingSection> = (args) => (
  <LandingSection {...args}>
    <h2>Foo bar baz</h2>
    <img src="https://dummyimage.com/300" alt="dummy" />
  </LandingSection>
);

export const FlexDirectionRow = Template.bind({});
FlexDirectionRow.args = {
  flexDirection: "row",
};

export const FlexDirectionColumn = Template.bind({});
FlexDirectionRow.args = {
  flexDirection: "column",
};

export const Thin = Template.bind({});
Thin.args = {
  flexDirection: "row",
  thickness: "thin",
};

export const Normal = Template.bind({});
Normal.args = {
  flexDirection: "row",
  thickness: "normal",
};

export const Thick = Template.bind({});
Thick.args = {
  flexDirection: "row",
  thickness: "thick",
};
