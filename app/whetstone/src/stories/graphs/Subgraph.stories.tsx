import { Meta, StoryObj } from "@storybook/react";
import { Subgraph } from "../../components/graph/subgraph";
import Graph from "graphology";

const meta = {
  title: "Graphs/Subgraph",
  component: Subgraph,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    // layout: "fullscreen",
  },
} as Meta<typeof Subgraph>;

export default meta;
type Story = StoryObj<typeof meta>;

const exampleGraph = {
  directed: true,
  multigraph: true,
  graph: {},
  nodes: [
    {
      id: "example",
      position: [0, 0],
    },
    {
      id: "example2",
      position: [100, 0],
    },
  ],
  links: [
    {
      source: "example",
      target: "example2",
    },
  ],
};

export const Primary: Story = {
  args: {
    id: "example",
    graph: exampleGraph,
  },
};
