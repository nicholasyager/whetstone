import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { RegistrationForm } from "../components/auth/registration";

export default {
  title: "Authentication/RegistrationForm",
  component: RegistrationForm,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: "fullscreen",
  },
} as ComponentMeta<typeof RegistrationForm>;

const Template: ComponentStory<typeof RegistrationForm> = (args) => (
  <RegistrationForm {...args} />
);

export const LoggedIn = Template.bind({});
