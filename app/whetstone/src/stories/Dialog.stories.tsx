import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Dialog } from "../components/dialog/dialog";
import { Input } from "../components/forms/forms";

export default {
  title: "Forms/Dialog",
  component: Dialog,
  parameters: {
    // More on Story layout: https://storybook.js.org/docs/react/configure/story-layout
    layout: "fullscreen",
  },
} as ComponentMeta<typeof Dialog>;

const Template: ComponentStory<typeof Dialog> = (args) => (
  <Dialog {...args}>
    <h1>Example</h1>
    <p>Example content!</p>
    <Input id={"Name"} label={"Name"} />
  </Dialog>
);

export const Default = Template.bind({});
Default.args = {};
