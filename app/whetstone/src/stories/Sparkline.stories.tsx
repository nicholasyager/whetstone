import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import {
  SparklineContainer,
  Sparkline,
  SparklineLabel,
  SparklinePoints,
} from "../components/sparkline/sparkline";
import { colors } from "../utilities/style";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Figures/Sparkline",
  component: Sparkline,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    width: { control: "number" },
    height: { control: "number" },
    color: { control: "color" },
    fill: { control: "color" },
  },
} as ComponentMeta<typeof Sparkline>;

let margin = { top: 10, right: 10, bottom: 10, left: 10 };

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof Sparkline> = (args) => (
  <SparklineContainer
    width={280}
    height={40}
    margin={margin}
    data={args.data || []}
  >
    <Sparkline {...args} />
    <SparklineLabel type="number" />
    <SparklinePoints />
  </SparklineContainer>
);

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  data: [
    { x: 1, y: 1 },
    { x: 2, y: 24 },
    { x: 3, y: undefined },
    { x: 4, y: 78 },
  ],
  color: colors.primary,
  fill: colors.lightprimary,
};
