## 0.6.6 (2023-07-18)

### Fix

- Correct flake8 issues
- Update deps and add (some) support for dbt-core 1.5.x

## 0.6.5 (2023-04-09)

### Fix

- **app**: Correct login button evaluation on `ENTER` key

## 0.6.4 (2023-04-09)

### Fix

- **app**: Correct pasword input "show" button margin

## 0.6.3 (2023-04-05)

## 0.6.2 (2023-04-04)

### Feat

- **app**: Add loading indicators for project edit forms
- **app**: Password inputs have a show/hide button

## 0.6.1 (2023-04-04)

### Fix

- **app**: Correct lockfile to limit sigma version to 2.1.2

## 0.6.0 (2023-04-04)

### Feat

- **app**: Wire up edit page to create and update Webhooks
- **api**: Add webhook handler endpoint
- **api**: Add ability to register webhooks
- **app**: Add input for dbt Cloud service token
- **app**: Add Edit page for projects

### Fix

- **api**: Support test messages and block non-job-success messages

## 0.5.0 (2023-04-02)

### Feat

- **app**: Set search box to use node selectors instead of entity_id
- **api**: Add select and exclude qsps to support dbt-basic node selection

## 0.4.5 (2023-04-01)

### Fix

- **app**: Correct new errors in style typing

### Perf

- **engine**: Migrated from pydot to pygraphviz for performance

## 0.4.4 (2023-03-26)

### Fix

- **engine**: Migrate from ssh dep to https dep for whetstone-api

## 0.4.3 (2023-03-24)

### Fix

- **engine**: Engine handles unexpected node types during content generation

## 0.4.2 (2023-03-23)

### Fix

- **engine**: Handle run_results referencing models that do not exist in the manifest
- **engine**: Fix parsing of metrics. Allow None models and set correct optionality for manifest v8

## 0.4.1 (2023-03-22)

### Fix

- **app**: Remove input label for analysis entity search

## 0.4.0 (2023-03-21)

### Feat

- **app**: Add an entity_id search input to the results page
- **api**: Add entity_id filtering to DemoResultGroups
- **api**: Add pattern search for `entity_id` via api for ResultGroups
- **engine**: Add rule for exposure parent materialization

### Fix

- **api**: Fix handling missing entity_id filter NoneType exception
- **engine**: Add missing content generator for exposures

### Refactor

- **engine**: Move chained views and exposure parent materializations to Execution ruleset

### Perf

- **api**: Add indices to database to support pattern search for entity_ids

## 0.2.8 (2023-03-20)

### Feat

- Added source fanout rule
- Updated README.md. Added full logo

### Fix

- Only project sources are included in the DAG, not package sources
- Change <p> to <div> to better support GitLab centering

## 0.2.7 (2023-03-19)

### Feat

- Add edit and delete buttons for Projects
- Add actual API docs to the landing!

### Fix

- Updates to unit tests
- flake8 cleanups
- Engine only returns graph and result for project nodes.
- Eslint suggestions

## 0.2.6 (2023-03-18)

### Feat

- Make 'yer own dang sparklines
- Add sparkline for results, too. Why not.
- Add statistics history to Projects output for List and Get
- Add sparklines to Project list
- Add basic styling to Project list
- Add open graph image for social media stuffs
- Add error message when auth fails during login
- Add ability for Engine to generate filters
- Add filters list to ResultGroupCreate schemas
- Wire-up filtering for DemoAnalyses
- Add basic multicheckbox selector for filters. Wire up to API
- Added filters for Severity and RuleSet to DemoResults
- Add basic filtering for ResultGroups and Results
- Add `filters` jsonb column to ResultGroup and DemoResultGroup for indexable filters

### Fix

- eslint corrections!
- Add URL root to open graph images content
- eslint fixes
- Test failures due to inner join instead of left join
- Remove footer from everything but landing page

## 0.2.5 (2023-03-13)

### Feat

- Add a footer to the bottom of the page
- Minify image used for the engine

### Fix

- Cleaned up eslint warnings

## 0.2.4 (2023-03-12)

### Feat

- Basic subgraph saliency

### Fix

- Correct tests for RG constructor

## 0.2.3 (2023-03-11)

### Fix

- Back botton from project list goes back to landing
- Back button works on demo list page
- Updated correlation functions to use localStorage instead of sessionStorage

## 0.2.1 (2023-03-11)

### Feat

- Add invocation_id to RunList and run summary components
- Updated build process to have better tags for Docker images

### Fix

- Tweak conditionals for image tagging if ci tag is missing

## 0.2.0 (2023-03-10)

### Feat

- Add HiRes logo png
- Add chained view rule to DAGStructureRuleSet. Fixed cli arguments
- Add paginator and API client updates for demo runs listing
- Add pagination to demo run list endpoint
- Add Pagination support to webapp
- Add pagination support to Run list endpoint
- Add Latest Analysis section to DemoRuns page
- Add accomodations to RunMetadata and RunDetails using <abbr />
- Add API optimizations for DemoResults
- Performance enhancements on engine subgraph generation
- Update Result and ResultGroup to bulk-insert
- Enhanced rendering of ResultCards and RunDetails
- Add graph complexity score to DAGStructure RuleSet
- Update `make build` to run a full rebuild
- Basic in-app run statistic visualization
- Add label to RunStatistic for UI generation
- Ad RunStatistic router and repository
- Add migration for run statistics
- Add RunStatistic schema
- Enable autoscaling for whetstone-engine celery worker
- Simplified worker to be agnostic to Demos
- Added refreshes to AnalysisPage
- Add automatic retries to DemoAnalysis page for run status
- Add logging and remove extra content from Results!
- Add tests, wire up workers, update API for ResultGroups
- Update Demo Result endpoint to have a homologous result group implementation
- Added DemoResultGroup schemas
- Implement remaining standard Result endpoints to use ResultGroups
- Add repository for ResultGroup, migrate to populate severity and count
- Add background color to resultSection SigmaContainer component
- Create registration form and page
- Create a typing animation for the landing page
- Add SSH agent for engine build process
- Add release job
- Add GitLab job for building/deploying docker containers
- Added loading indicator for ResultsPage
- Syntax highlighting and details dropdown per model
- Hack to add query to outside of a ResultCard
- Added gzip compression for a little extra oomph
- Set subgraphs to be hidden. Add "Subgraph" toggle for each result
- Removed upper Paginator from ResultList
- Small visual tweaks to runMetadata block
- Add names to breadcrumbs on Project page
- Added account and project names to breadcrumbs in ResultPage
- Added basic pagination to Results lists
- Added total_count to Results endpoint
- Add pagination to results list
- Update app to support new API wrapper
- Add Response wrapper to DemoResults endpoints
- Wrap DemoRun with Response
- Wrapped Users with Response. Updated tests
- Added Response wrapper for user accounts
- Migrated runs to Response output
- Add loading container for graphs
- Add lazy Graph loading
- Add reintroduction of upstream concepts
- Add endpoint and repo for UserAccounts
- Add rule for models with no references
- Abstract graph serialization
- Add `root_model_rule` to Whetstone engine
- Major overhaul to support demo results and runs

### Fix

- Convert source check from list to Set. dedupes
- Corrected total page count in paginators
- Add missing order_by. Remove extra project_id
- Minor performance enhancements
- WebApp more robust for Statistical oddities
- Engine support run stats for DemoRuns
- Run router handles missing statistics by returning None
- Fix non-operative DAGStructure rule set
- Correct query for getting RunStatistics
- Small improvements to RunStatistics
- eslint cleanup!
- Various mobile-friendly improvements
- RunList support mobile
- Styles for dialogs are consistent w/ ability to close without submit
- Correct shared dependencies
- Engine tests for result group construction
- Correct defefcts in demo result creation
- Style improvements to landing on large displays
- Update WebApp for new Result structure, now that the content is different
- Add Union for ResultGroupContent type
- Small fixes based on testing
- Correct remaining eslint results
- Cleaned up extra logging from result group repo
- Updated tests for results
- Add early filter for demo results to prevent exposing results
- ResultSection oddness component updates in render
- Add key to Paginator link listings
- Remove some vistigial warnings
- Remove graph timing logging
- Removed unnecessary logging
- Add background color to sigma component
- Correcting breadcrumbs in demo pages
- Minor corrections to run ids. Added links and whatnot
- Style improvements and consistency changes
- Tweaked comparitors to eslint spec
- envvar types must be strings or hashes in GitLab
- add missing envvar to test environment
- Made minio SSL configurable in an envvar
- Simplified environemtn variables for app
- Second attempt on the API url
- Add WHETSTONE_API environment variable for production builds
- Add changes rule to engine and api to reduce deployments
- Flake8 fix for engine
- Add dependency installation to test job for engine
- Tweaking docker registry URL
- Stage correction attempt 2
- Correct stage names
- Tweaked names and added dependencies
- Corrected rule/if for API container build
- Change /execute/fetch_one/ for adding UserAccounts
- Wired up pagination for DemoResults components/API calls
- Correct DemoResults query returned fields
- Add pagination to DemoResults query and list endpoint
- Add json serde for DemoRun creation metadata field
- Add check for defaultAccountId in navigation of Dashboard page
- Correct grammatical error in Documentation description
- Graph flicker on reengagement w/ screen
- Replace dropshadow with tanslucent background in ResultCard
- Corrected bug in pagination code for result listing API call
- Paginate results by model, not result
- Change low-severity Card color to gray
- Add logic to include first and last page to Paginator
- Resolved testing errors w/ Response wrappers
- Fixed test_runs tests for new Response format.
- Remove extra semicolon
- Resolve eslint errors
- Tweak makefiles to support engine containerization
- Add GraphCache provider to handle graph caching on repeat uploads
- Add IDs to logging
- Added timing logging to identify bottlenecks
- Remove unnecessary imports and old commented-out code
- Corrected refresh annoyances
- Correct engine use of result invalidation
- Corrected missing React dependencies in LoadGraph component

## 0.1.0 (2022-04-05)
